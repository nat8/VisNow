VisNow
==================================

VisNow is a generic visualization framework in Java technology, developed by Interdisciplinary Centre for Mathematical and Computational Modelling 
at University of Warsaw. See VisNow web page (http://visnow.icm.edu.pl/) for details.
It is a modular data flow driven platform enabling users to create schemes for data visualization, visual analysis, data processing and simple simulations. 
Motivated by 'Read and Watch' idea, VisNow shows the data as soon and fast as possible giving further opportunity for processing and more in-depth visualization. 
In a few steps it can create professional images and movies as well as discover unknown information hidden in datasets.

VisNow, not trying to be yet another visualization tool, is targeted at overcoming the drawbacks of other similar software platforms. It is subject to the following keynotes:
* Clear and legible desktop
* Network creation support
* Multifunctional modules
* Module-Object-Interface connection

## Acknowledgements
The following work was implemented at the Interdisciplinary Centre for Mathematical 
and Computational Modelling (ICM), University of Warsaw, Poland, as a part of the 
OCEAN (Open Centre for Data and its Analysis) project, co-funded by the National 
Centre for Research and Development within Innovative Economy Programme (POIG). 

![OCEAN EU footer](stopka_ENG_100.png)