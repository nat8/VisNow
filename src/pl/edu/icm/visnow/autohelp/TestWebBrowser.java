package pl.edu.icm.visnow.autohelp;

/**
 * fast, testing prototype of web browser to test java html rendering engine
 */
import javax.swing.*;
import java.io.*;
import java.awt.*;
import java.awt.event.*;

public class TestWebBrowser
{

    public static void main(String[] args)
    {

        // get the first URL
        String initialPage = "http://www.icm.edu.pl/";
        if (args.length > 0) {
            initialPage = args[0];
        }

        // set up the editor pane
        final JEditorPane jep = new JEditorPane();
        jep.setEditable(false);

        try {
            jep.setPage(initialPage);
        } catch (IOException ex) {

        }

        // set up the window
        JScrollPane scrollPane = new JScrollPane(jep);
        JFrame f = new JFrame();
        JPanel p = new JPanel();
        p.setLayout(new BorderLayout());
        final JTextField t = new JTextField();
        t.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent e)
            {
                try {
                    System.out.println("Loading : " + t.getText());
                    jep.setPage(t.getText());
                    System.out.println("Loading : " + t.getText());
                } catch (Exception ex) {
                }
                ;
            }
        });

        p.add(t, BorderLayout.NORTH);
        p.add(scrollPane, BorderLayout.CENTER);

        f.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        f.setContentPane(p);
        f.setSize(512, 342);
        EventQueue.invokeLater(new FrameShower(f));

    }

    // Helps avoid a really obscure deadlock condition.
    // See http://java.sun.com/developer/JDCTechTips/2003/tt1208.html#1
    private static class FrameShower implements Runnable
    {

        private final Frame frame;

        FrameShower(Frame frame)
        {
            this.frame = frame;
        }

        public void run()
        {
            frame.setVisible(true);
        }

    }

}
