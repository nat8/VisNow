package pl.edu.icm.visnow.autohelp;

import java.io.File;
import static pl.edu.icm.visnow.autohelp.AutoHelpGenerator.AUTOHELP_MODULES_DIR;
import static pl.edu.icm.visnow.autohelp.AutoHelpGenerator.AUTOHELP_ROOT;

public class FullHelpGenerator
{

    public static void main(String[] args)
    {
        try {
            String workDir = System.getProperty("user.dir");
            String srcDir = workDir + File.separator + "src";
            String modulesDir = srcDir + File.separator + AUTOHELP_ROOT + File.separator + AUTOHELP_MODULES_DIR;
            removeRecursively(modulesDir, true);
            ModuleImageGenerator.main(args);
            AutoHelpGenerator.main(args);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private static void removeRecursively(String path, boolean contentOnly)
    {
        if (path == null) {
            return;
        }

        File f = new File(path);
        if (!f.exists()) {
            return;
        }

        if (f.isFile()) {
            f.delete();
            return;
        }

        if (f.isDirectory()) {
            String[] ls = f.list();
            for (int i = 0; i < ls.length; i++) {
                removeRecursively(f.getAbsolutePath() + File.separator + ls[i], false);
            }
        }

        if (!contentOnly) {
            f.delete();
        }
    }

}
