package pl.edu.icm.visnow.gui.utils;

import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.Action;
import javax.swing.JComponent;
import javax.swing.JTable;

/**
 *
 * @author babor
 */


public class Tableutilities {

    public static void addContentTooltipPopup(final JTable table, final int column) {
        table.addMouseMotionListener(new MouseAdapter() {
            
            @Override
            public void mouseMoved(MouseEvent e) {
                JComponent component = (JComponent) e.getSource();
                if(component == null)
                    return;
                
                int c = table.columnAtPoint(e.getPoint());
                if (c == column) {
                    int r = table.rowAtPoint(e.getPoint());
                    if(r == -1)
                        return;
                    Object obj = table.getModel().getValueAt(r, c);
                    if(obj == null)
                        return;
                    component.setToolTipText(obj.toString());
                    Action toolTipAction = component.getActionMap().get("postTip");
                    if (toolTipAction != null) {
                        ActionEvent postTip = new ActionEvent(component, ActionEvent.ACTION_PERFORMED, "");
                        toolTipAction.actionPerformed(postTip);
                    }
                } else {
                    component.setToolTipText(null);
                }
            }
        });

    }
    
    
    
}
