package pl.edu.icm.visnow.geometries.viewer3d.MoviesManager;

/**
 * @author Norbert Kapiński (norkap@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 * 
 * 
 */


public interface CameraWindowCloseListener {
    public void onCameraWindowClose();
}
