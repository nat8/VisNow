package pl.edu.icm.visnow.system.swing;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridBagLayoutInfo;
import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 * Panel that "cheats" backing GridBagLayout to prevent it from using minimumSize of components when there is not enough space for render them using preferredSize.
 * Cheating means passing preferredSize as minimumSize.
 * 
 * @author szpak
 */
public class FixedGridBagLayoutPanel extends JPanel
{
    private GridBagLayout fixedGridBagLayoutManager;

    public FixedGridBagLayoutPanel()
    {
        fixedGridBagLayoutManager = new GridBagLayout()
        {

            @Override
            protected GridBagLayoutInfo getLayoutInfo(Container parent, int sizeflag)
            {
                return super.getLayoutInfo(parent, PREFERREDSIZE);
            }

            public Dimension minimumLayoutSize(Container parent)
            {
                GridBagLayoutInfo info = super.getLayoutInfo(parent, MINSIZE);
                return getMinSize(parent, info);
            }
        };

        setLayout(fixedGridBagLayoutManager);
    }

    /**
     * This method should not be called as this JPanel is dedicated for use with customized GridBagLayout. Though, it is called in initComponents (in NB GUI Builder).
     * This method should have empty body but NB GUI Builder doesn't work properly then.
     * @param mgr
     * @deprecated
     */
    @Deprecated
    @Override
    public void setLayout(LayoutManager mgr)
    {
        super.setLayout(mgr); //To change body of generated methods, choose Tools | Templates.
        super.setLayout(fixedGridBagLayoutManager);
    }

}
