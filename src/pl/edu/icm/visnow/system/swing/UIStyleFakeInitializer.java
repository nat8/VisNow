package pl.edu.icm.visnow.system.swing;

import javax.swing.JComponent;

/**
 * Fake component for applying UIStyles in GUI builder. (sic!)
 * This is for Netbeans GUI builder doesn't run Component constructor in design mode but just shows generated form using .form file.
 * In the same time it initializes all children (all used subcomponents) with default constructor. :(
 * <p>
 * @author szpak
 */
public class UIStyleFakeInitializer extends JComponent
{

    private boolean postInitSucceed = false;

    public UIStyleFakeInitializer()
    {
        UIStyle.initStyle();
        this.setVisible(false);
    }
}
