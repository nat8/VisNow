package pl.edu.icm.visnow.system.swing;

import java.awt.Dimension;
import java.awt.LayoutManager;
import javax.swing.JPanel;

/**
 * Panel which preferred width is constant. Preferred height is taken from preferred height of super component.
 * This panel is suitable for use in JScrollPane.
 * <p>
 * @author szpak
 */
public class CustomSizePanel extends JPanel
{
    //horizontal scroll should be visible up to this width
    private int customWidth = 240;
    private int customHeight = -1;

    private boolean keepSmallerWidth = false;
    private boolean keepSmallerHeight = false;

    private int minWidthIfKeepSmaller = 0;
    private int minHeightIfKeepSmaller = 0;

    private boolean overrideMinSize = false;

    public CustomSizePanel(LayoutManager layout, boolean isDoubleBuffered)
    {
        super(layout, isDoubleBuffered);
    }

    public CustomSizePanel(LayoutManager layout)
    {
        super(layout);
    }

    public CustomSizePanel(boolean isDoubleBuffered)
    {
        super(isDoubleBuffered);
    }

    public CustomSizePanel()
    {

    }

    /**
     * @return preferred size based on preferred height of super component and custom width.
     */
    @Override
    public Dimension getPreferredSize()
    {
        Dimension d = super.getPreferredSize();
        if (customWidth != -1)
            if (keepSmallerWidth)
                d.width = Math.max(minWidthIfKeepSmaller, Math.min(d.width, customWidth));
            else
                d.width = customWidth;

        if (customHeight != -1)
            if (keepSmallerHeight)
                d.height = Math.max(minHeightIfKeepSmaller, Math.min(d.height, customHeight));
            else
                d.height = customHeight;

        return d;
    }

    @Override
    public Dimension getMinimumSize()
    {
        if (overrideMinSize) return getPreferredSize();
        else return super.getMinimumSize(); //To change body of generated methods, choose Tools | Templates.
    }

    public int getCustomWidth()
    {
        return customWidth;
    }

    public void setCustomWidth(int width)
    {
        this.customWidth = width;
    }

    public boolean isKeepSmallerWidth()
    {
        return keepSmallerWidth;
    }

    public void setKeepSmallerWidth(boolean keepSmallerWidth)
    {
        this.keepSmallerWidth = keepSmallerWidth;
    }

    public int getCustomHeight()
    {
        return customHeight;
    }

    public void setCustomHeight(int customHeight)
    {
        this.customHeight = customHeight;
    }

    public boolean isKeepSmallerHeight()
    {
        return keepSmallerHeight;
    }

    public void setKeepSmallerHeight(boolean keepSmallerHeight)
    {
        this.keepSmallerHeight = keepSmallerHeight;
    }

    public int getMinWidthIfKeepSmaller()
    {
        return minWidthIfKeepSmaller;
    }

    public void setMinWidthIfKeepSmaller(int minWidthIfKeepSmaller)
    {
        this.minWidthIfKeepSmaller = minWidthIfKeepSmaller;
    }

    public int getMinHeightIfKeepSmaller()
    {
        return minHeightIfKeepSmaller;
    }

    public void setMinHeightIfKeepSmaller(int minHeightIfKeepSmaller)
    {
        this.minHeightIfKeepSmaller = minHeightIfKeepSmaller;
    }

    public boolean isOverrideMinSize()
    {
        return overrideMinSize;
    }

    public void setOverrideMinSize(boolean overrideMinSize)
    {
        this.overrideMinSize = overrideMinSize;
    }
}
