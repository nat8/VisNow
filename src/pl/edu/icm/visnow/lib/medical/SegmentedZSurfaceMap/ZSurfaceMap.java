//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.medical.SegmentedZSurfaceMap;


import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.visnow.engine.core.ModuleCore;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class ZSurfaceMap extends ModuleCore
{

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private static final Logger LOGGER = Logger.getLogger(ZSurfaceMap.class);
    protected RegularField inField = null;
    protected RegularField outField = null;
    protected GUI computeUI = null;
    protected boolean fromUI = false;
    protected boolean ignoreUI = false;
    protected Params params;
    protected DataArray da;
    protected int[] dims;
    protected int sliceSize = 1;
    protected int maxbin = -1;
    protected int[][] bins;
    protected int nStructs = 1;
    protected int nThreads = VisNow.availableProcessors();

    public ZSurfaceMap()
    {
        parameters = params = new Params();
        params.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent evt)
            {
                fromUI = true;
                if (ignoreUI) {
                    ignoreUI = false;
                    return;
                }
                if (params.write()) {
                    params.setWrite(false);
                    if (outField != null)
                    {
                        FileWriter outFile;
                        try {
                            outFile = new FileWriter(params.getOutFilePath(), false);
                            float[] la = outField.getComponent(0).getRawFloatArray().getData();
                            float dx = outField.getAffine()[0][0];
                            for (int i = 0; i < la.length; i++) 
                                outFile.write(String.format("%8.3f, %8.3f%n", i * dx, la[i]));
                            outFile.close();
                        } catch (IOException e) {
                            System.out.println("write failed");
                        }
                    }
                } else
                   startAction();
            }
        });
        computeUI = new GUI();
        computeUI.setParams(params);
        setPanel(computeUI);
    }

    private void updateBins()
    {
        if (da.getUserData() == null || da.getUserData().length <= 3 || !"MAP".equalsIgnoreCase(da.getUserData()[0]))
            return;
        nStructs = da.getUserData().length - 3;
        bins = new int[nStructs][dims[2]];
        sliceSize = dims[0] * dims[1];
        for (int i = 0; i < dims[2]; i++) {
            int k = i * sliceSize;
            switch (da.getType()) {
                case FIELD_DATA_BYTE:
                    byte[] bd = (byte[])da.getRawArray().getData();
                    for (int j = 0; j < sliceSize; j++, k++) {
                        int v = (0xff & bd[k]) - 2;
                        if (v >= 0 && v < nStructs)
                             bins[v][dims[2] - i - 1] += 1;
                    }
                    break;
            }
        }
    }

    private void updateOut()
    {
        float[][] outData = new float[nStructs][dims[2]];
        for (int j = 0; j < nStructs; j++) 
            for (int i = 0; i < dims[2]; i++)
                outData[j][i] =  bins[j][dims[2] - i - 1];
        if (params.getType() == Params.SQRT)
            for (int j = 0; j < nStructs; j++) 
                for (int i = 0; i < dims[2]; i++)
                     outData[j][i] =  (float) sqrt(outData[j][i]);
        outField = new RegularField(new int[]{dims[2]});
        outField.getAffine()[0][0] = inField.getAffine()[2][2];
        for (int i = 0; i < nStructs; i++) {
            String[] s = da.getUserData()[3 + i].split(":");
            outField.addComponent(DataArray.create(outData[i], 1, s[1]));
        }
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null)
            return;
        RegularField field = ((VNRegularField) getInputFirstValue("inField")).getField();
        if (field == null)
            return;
        inField = field;
        if (da == null)
            da = inField.getComponent("segmented_data");
        if (da == null)
            return;
        dims = inField.getDims();
        updateBins();
        updateOut();
        setOutputValue("outField", new VNRegularField(outField));
    }

}
