package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D;

import java.awt.Component;
import java.awt.Frame;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart.ExtendedChartPanel;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart.ChartData;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Vector;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jfree.data.UnknownKeyException;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.Input;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.Inputs;
import pl.edu.icm.visnow.engine.core.Link;
import pl.edu.icm.visnow.engine.core.LinkFace;
import pl.edu.icm.visnow.engine.core.ModuleCore;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart.ExtendedChart;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart.RemoveChartActionListener;
import static pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.FieldViewer1DShared.*;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.utils.ExtendedMenuItem;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 *
 * @author norkap
 */
public class FieldViewer1D extends ModuleCore implements RemoveChartActionListener
{

    protected ArrayList<ExtendedChart> charts;
    protected Viewer1DFrame frame = new Viewer1DFrame();
    private GUI ui = null;
    private boolean dataSetChanged = false;
    private HashMap<String, String> currentConnectedComponents;

    public FieldViewer1D()
    {
        this.charts = new ArrayList<>();
        this.currentConnectedComponents = new HashMap<>();
        
        parameters.addParameterChangelistener(new ParameterChangeListener() {

            @Override
            public void parameterChanged(String name) {
                if(name.equals(DATA_SET.getName())){
                    dataSetChanged = true;
                    startAction();
                }else{
                    dataSetChanged = false;
                }
            }
        });

        frame.addChartActionListener(new AddChartListener());

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                ui = new GUI();

                ExtendedChartPanel chartPanel = new ExtendedChartPanel();
                chartPanel.addRemoveChartListener(getInstance());
                ChartData data = new ChartData();

                ExtendedChart chart = new ExtendedChart(data, chartPanel);
                charts.add(chart);

                frame.addChartToDisplay(chartPanel);
                frame.pack();
                frame.setTitle("VisNow Viewer1D");
                frame.setVisible(true);

                ui.addChangeListener(new ChangeListener()
                {
                    @Override
                    public void stateChanged(ChangeEvent evt)
                    {
                        frame.setVisible(true);
                        frame.setExtendedState(Frame.NORMAL);
                    }
                });
                setPanel(ui);
            }

        });

    }
    
    private FieldViewer1D getInstance(){
        return this;
    }

    public static InputEgg[] inputEggs = null;

    @Override
    public void onActive()
    {
        Parameters p = parameters.getReadOnlyClone();
        
        if(dataSetChanged){

            XYSeriesCollection seriesCollection = p.get(DATA_SET);
            
            for (ExtendedChart chart : charts) {
                chart.setDataSet(seriesCollection);
                chart.update();
            }
            dataSetChanged = false;
        }else{
            Inputs moduleInputs = getInputs();
            Iterator it = moduleInputs.iterator();
            
            if(it.hasNext()){
                
                currentConnectedComponents = new HashMap<>();
                XYSeriesCollection seriesCollection = new XYSeriesCollection();
                Vector<Link> links = ((Input)it.next()).getLinks();
                for (Link link : links) {
                    String moduleName = link.getName().getOutputModule();
                    VNRegularField field = (VNRegularField)link.getOutput().getData().getValue();
                    
                    if(field != null){

                    RegularField inFld;
                    DataArray da; 

                    inFld = ((VNRegularField)field).getField();

                        for (int i = 0; i < inFld.getNComponents(); i++) {
                            da = inFld.getComponent(i);
                            if(da.isNumeric() && da.getVectorLength() == 1){
                                String name = da.getName();

                                try{
                                    int j = 2;
                                    String temp_name = name;
                                    while(true){
                                        XYSeries series = seriesCollection.getSeries(name);
                                        name = temp_name + j;
                                        j++;
                                    }
                                }catch(UnknownKeyException ex){
                                    seriesCollection.addSeries(Core.createSeries(da, inFld, name));
                                    currentConnectedComponents.put(da.getName() +"&"+ moduleName, name);
                                }                  
                            }
                        }
                    }
                }
                parameters.set(DATA_SET, seriesCollection);
            }
        }
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(DATA_SET, new XYSeriesCollection())
        };
    }
    
    private void setDataSet(XYSeriesCollection seriesCollection){
        parameters.set(DATA_SET, seriesCollection);
    }
    
    @Override
    public void onInputAttach(LinkFace link) {
        
        VNRegularField field = (VNRegularField)link.getOutput().getData().getValue();
        String moduleName = link.getOutput().getModuleBox().getName();
        
        if(field == null)
            return;

        RegularField inFld;
        DataArray da;
        boolean daAdded = false;
        XYSeriesCollection seriesCollection = parameters.get(DATA_SET);

        inFld = ((VNRegularField)field).getField();
           
            for (int i = 0; i < inFld.getNComponents(); i++) {
                da = inFld.getComponent(i);
                if(da.isNumeric() && da.getVectorLength() == 1){
                    String name = da.getName();
                    
                    try{
                        int j = 2;
                        String temp_name = name;
                        while(true){
                            XYSeries series = seriesCollection.getSeries(name);
                            name = temp_name + j;
                            j++;
                        }
                    }catch(UnknownKeyException ex){
                        seriesCollection.addSeries(Core.createSeries(da, inFld, name));
                        currentConnectedComponents.put(da.getName() +"&"+ moduleName, name);
                        daAdded = true;
                    }                  
                }
            }
        if(daAdded){
            setDataSet(seriesCollection);
        }
    }

    @Override
    public void onInputDetach(LinkFace link) {
        
        String moduleName = link.getOutput().getModuleBox().getName();
        HashMap<String, String> newCurrentConnectedComponents = new HashMap<>();

        for (Map.Entry entry : currentConnectedComponents.entrySet()) {
            if(((String)entry.getKey()).split("&")[1].equals(moduleName)){
                XYSeries seriesToBeRemoved = parameters.get(DATA_SET).getSeries((Comparable)entry.getValue());
                parameters.get(DATA_SET).removeSeries(seriesToBeRemoved);
            }else{
                newCurrentConnectedComponents.put((String)entry.getKey(), (String)entry.getValue());
            }
        }

        if(newCurrentConnectedComponents.size()!=currentConnectedComponents.size()){
            currentConnectedComponents = newCurrentConnectedComponents;
            parameters.fireParameterChanged(DATA_SET.getName());
        }
    }
    

    @Override
    public void removeChart(Component comp)
    {
        int idx = frame.getComponentIndex(comp);
        charts.remove(idx);
        frame.removeChartFromDisplay((ExtendedChartPanel) comp);
        showLastComponentManagerContainer();
    }

    private void addChart()
    {
        ExtendedChartPanel chartPanel = new ExtendedChartPanel();
        chartPanel.addRemoveChartListener(this);
        ChartData data = new ChartData();
        ExtendedChart chart = new ExtendedChart(data, chartPanel);
        
        Parameters p = parameters.getReadOnlyClone();
        chart.setDataSet(p.get(DATA_SET));
        chart.update();
        
        charts.add(chart);
        frame.addChartToDisplay(chartPanel);
        showLastComponentManagerContainer();
    }
    
    private void showLastComponentManagerContainer(){
        boolean show;
        for (int i = 0; i < charts.size(); i++) {
            show = (i == (charts.size() - 1));
            charts.get(i).getChartPanel().showComponentsList(show);
        }
    }

    class RemoveChartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            if (e.getSource() instanceof ExtendedMenuItem) {
                ExtendedMenuItem mItem = (ExtendedMenuItem) e.getSource();
                removeChart(SwingUtilities.getAncestorOfClass(ExtendedChartPanel.class, mItem.getAncestor()));
            }
        }

    }

    class AddChartListener implements ActionListener
    {

        @Override
        public void actionPerformed(ActionEvent e)
        {
            addChart();
        }

    }
    
    @Override
    public void onInitFinished()
    {
        updateFrameName();
    }

    @Override
    public void onNameChanged()
    {
        updateFrameName();
    }

    private void updateFrameName()
    {
        if (frame != null)
            frame.setTitle("VisNow Viewer1D - " + this.getApplication().getTitle() + " - " + this.getName());
    }

    @Override
    public void onDelete() {
        frame.dispose();
    }
    
    

}
