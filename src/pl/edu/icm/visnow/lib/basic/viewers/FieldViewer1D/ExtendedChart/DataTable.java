package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart;

import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.utils.ColorRenderer;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.utils.ColorEditor;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.event.CellEditorListener;
import javax.swing.table.AbstractTableModel;
import pl.edu.icm.visnow.gui.utils.Tableutilities;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.utils.ExtendedMenuItem;

/**
 *
 * @author norkap
 */
public class DataTable extends JPanel
{

    private boolean DEBUG = false;
    private final JTable table;
    private final TableModel tableModel;
    private JPopupMenu popup;
    private ExtendedMenuItem removeSeriesPopupItem;
    private boolean remove = false;

    public DataTable()
    {
        super(new GridLayout(1, 0));

        tableModel = new DataTable.TableModel();

        table = new JTable(tableModel);
        Tableutilities.addContentTooltipPopup(table, 0);
        table.setPreferredScrollableViewportSize(new Dimension(450, 300));
        table.setFillsViewportHeight(true);

        //Create the scroll pane and add the table to it.
        JScrollPane scrollPane = new JScrollPane(table);

        //Set up renderer and editor for the Favorite Color column.
        table.setDefaultRenderer(Color.class,
                                 new ColorRenderer(true));
        table.setDefaultEditor(Color.class,
                               new ColorEditor());

        table.getTableHeader().setReorderingAllowed(false);
        //Add the scroll pane to this panel.
        add(scrollPane);
        createPopupMenu();
        
    }

    public String[] getRemovedSeriesNames()
    {
        int nSelectedRows = table.getSelectedRowCount();
        boolean isEmptyRow = false;
        if (nSelectedRows > 0) {
            String[] seriesToRemoveNames = new String[nSelectedRows];
            for (int i = 0; i < nSelectedRows; i++) {
                seriesToRemoveNames[i] = table.getValueAt(table.getSelectedRows()[i], 0).toString();
                if (seriesToRemoveNames[i].equals("")) {
                    isEmptyRow = true;
                    break;
                }
            }
            if (!isEmptyRow) {
                return seriesToRemoveNames;
            } else
                return null;
        }
        return null;
    }

    public void addRemoveSeriesActionListener(ActionListener remSeriesAL)
    {
        removeSeriesPopupItem.addActionListener(remSeriesAL);
    }

    public void addSeriesColorChangedListener(CellEditorListener seriesColorChangedCEL)
    {
        table.getDefaultEditor(Color.class).addCellEditorListener(seriesColorChangedCEL);
    }

    private void createPopupMenu()
    {
        popup = new JPopupMenu();
        removeSeriesPopupItem = new ExtendedMenuItem("remove series");
        popup.add(removeSeriesPopupItem);
        MouseListener popupListener = new PopupListener(popup);
        table.addMouseListener(popupListener);
    }

    public Color getSeriesColor(ColorEditor colorEditor)
    {
        return (Color) colorEditor.getCellEditorValue();
    }

    public int getSelectedRow()
    {
        return table.getSelectedRow();
    }

    public String getSelectedSeriesName()
    {
        return (String) table.getValueAt(table.getSelectedRow(), 0);
    }

    public JTable getTable()
    {
        return this.table;
    }

    public void updateDataTable(Object[][] data)
    {
        if (data.length == 0) {
            tableModel.data = new Object[][]{
                {"", "", "", "", "", ""},};
        } else {
            tableModel.data = new Object[data.length][data[0].length];
            for (int i = 0; i < data.length; i++) {
                tableModel.data[i] = Arrays.copyOf(data[i], data[i].length);
            }
        }

        table.setModel(tableModel);
    }

    class TableModel extends AbstractTableModel
    {

        private String[] columnNames = {"component",
                                        "color",
                                        "min",
                                        "avg",
                                        "max",
                                        "<html>\u03c3</html>"
        };

        private Object[][] data = {
            {"", "", "", "", "", ""},};

        @Override
        public int getColumnCount()
        {
            return columnNames.length;
        }

        @Override
        public int getRowCount()
        {
            return data.length;
        }

        @Override
        public String getColumnName(int col)
        {
            return columnNames[col];
        }

        @Override
        public Object getValueAt(int row, int col)
        {
            return data[row][col];
        }

        public void setColumnNames(String[] columnNames)
        {
            this.columnNames = columnNames;
        }

        public void setData(Object[][] data)
        {
            this.data = data;
        }

        /*
         * JTable uses this method to determine the default renderer/
         * editor for each cell.  If we didn't implement this method,
         * then the last column would contain text ("true"/"false"),
         * rather than a check box.
         */
        @Override
        public Class getColumnClass(int c)
        {
            return getValueAt(0, c).getClass();
        }

        @Override
        public boolean isCellEditable(int row, int col)
        {
            //Note that the data/cell address is constant,
            //no matter where the cell appears onscreen.
            if (col == 1) {
                return true;
            }

            return false;
        }

        @Override
        public void setValueAt(Object value, int row, int col)
        {
            if (DEBUG) {
                System.out.println("Setting value at " + row + "," + col +
                    " to " + value +
                    " (an instance of " +
                    value.getClass() + ")");
            }

            data[row][col] = value;
            fireTableCellUpdated(row, col);

            if (DEBUG) {
                System.out.println("New value of data:");
                printDebugData();
            }
        }

        private void printDebugData()
        {
            int numRows = getRowCount();
            int numCols = getColumnCount();

            for (int i = 0; i < numRows; i++) {
                System.out.print("    row " + i + ":");
                for (int j = 0; j < numCols; j++) {
                    System.out.print("  " + data[i][j]);
                }
                System.out.println();
            }
            System.out.println("--------------------------");
        }
    }

    private class PopupListener extends MouseAdapter
    {

        private final JPopupMenu popup;

        PopupListener(JPopupMenu popupMenu)
        {
            popup = popupMenu;
        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            maybeShowPopup(e);
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {
            if (table.getSelectedRow() != -1) {
                maybeShowPopup(e);
            }
        }

        private void maybeShowPopup(MouseEvent e)
        {
            if (e.isPopupTrigger()) {
                int x = e.getX();
                int y = e.getY();
                popup.show(e.getComponent(), x, y);
            }
        }
    }

}
