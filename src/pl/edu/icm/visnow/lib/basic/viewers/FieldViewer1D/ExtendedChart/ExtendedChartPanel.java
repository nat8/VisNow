package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.Icon;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.TransferHandler;
import static javax.swing.TransferHandler.COPY;
import javax.swing.event.CellEditorListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.plot.XYPlot;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.Core;
import pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.utils.ColorEditor;
import pl.edu.icm.visnow.system.swing.UIIconLoader;
import pl.edu.icm.visnow.system.swing.UIManagerKey;

/**
 *
 * @author norkap
 */
public class ExtendedChartPanel extends javax.swing.JPanel
{
    private static final org.apache.log4j.Logger LOGGER = org.apache.log4j.Logger.getLogger(ExtendedChartPanel.class);
    private ChartPanel chartPanel = null;
    private final DataTable seriesTableContainer;
    private SeriesToDisplayChangedListener seriesToDisplayChangedListener;
    private RemoveChartActionListener removeChartActionListener;

    /**
     * Creates new form Viewer1DFrame
     */
    public ExtendedChartPanel()
    {
        initComponents();
        
        componentsList.setTransferHandler(new TransferHandler(){
                @Override
                public int getSourceActions(JComponent c) {
                    return COPY;
                }
                
                @Override
                protected Transferable createTransferable(JComponent c) {
                    JList list = (JList)c;
                    List values = list.getSelectedValuesList();

                    StringBuffer buff = new StringBuffer();

                    for (int i = 0; i < values.size(); i++) {
                        Object val = values.get(i);
                        buff.append(val == null ? "" : val.toString());
                        if (i != values.size() - 1) {
                            buff.append("\n");
                        }
                    }
                    return new StringSelection(buff.toString());
                }
            });
        componentsList.setDragEnabled(true);

        seriesTableContainer = new DataTable();
        GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 0);
        
        seriesTableContainer.setTransferHandler(new TransferHandler(){
            @Override
            public boolean canImport(TransferHandler.TransferSupport info){
                // only import Strings                
                return info.isDataFlavorSupported(DataFlavor.stringFlavor);
            }
            
            @Override
            public boolean importData(TransferHandler.TransferSupport info) {
                 if (!info.isDrop()) {
                    return false;
                }
                
                // Check for String flavor
                if (!info.isDataFlavorSupported(DataFlavor.stringFlavor)) {
                    LOGGER.error("Chart doesn't accept a drop of this type.");
                    return false;
                }
                
                
                try {
                    Transferable t = info.getTransferable();
                    String data = (String) t.getTransferData(DataFlavor.stringFlavor);
                    
                    String[] compNames = data.split("\n");
                    for (String compName : compNames) {
                        compName = compName.trim();
                    }
                    if(seriesToDisplayChangedListener!=null){
                        seriesChangedAction(compNames);
                    }
                    return true;
                } catch (UnsupportedFlavorException ex) {
                    Logger.getLogger(DataTable.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(DataTable.class.getName()).log(Level.SEVERE, null, ex);
                }
                return false;
            }
        });
        

        componentsManagerContainer.add(seriesTableContainer, gridBagConstraints);
        componentsManagerContainer.revalidate();
        componentsManagerContainer.repaint();
        
        Icon icon = UIIconLoader.getIcon(UIManagerKey.InternalFrame_paletteCloseIcon);
        closeChartButton.setIcon(icon);
    }

    public void setSeriesColor(int id, Color c)
    {
        chartPanel.getChart().getXYPlot().getRenderer().setSeriesPaint(id, c);
    }

    public void setChartPanel(ChartPanel chartPanel)
    {
        this.chartPanel = chartPanel;
        chartPanel.addMouseListener(new MyMouseListener());
        customizeChartPopup();
    }
    
    private void customizeChartPopup(){
        JPopupMenu popup = chartPanel.getPopupMenu();
        JMenuItem showLegend = new JMenuItem("Show Legend");
        showLegend.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                toggleLegend();
                if(((JMenuItem)e.getSource()).getText().equals("Show Legend")){
                    ((JMenuItem)e.getSource()).setText("Hide Legend");
                }else{
                    ((JMenuItem)e.getSource()).setText("Show Legend");
                }
            }
        });
        popup.add(showLegend);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        layeredPanel = new javax.swing.JLayeredPane();
        closeChartButton = new javax.swing.JButton();
        chartContainer = new javax.swing.JPanel();
        componentsManagerContainer = new javax.swing.JPanel();
        componentsListContainer = new javax.swing.JScrollPane();
        componentsList = new javax.swing.JList();
        componentsListLabel = new javax.swing.JLabel();
        componentsListTooltip = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(800, 200));
        setPreferredSize(new java.awt.Dimension(0, 0));
        setLayout(new java.awt.GridBagLayout());

        layeredPanel.setLayout(new java.awt.GridBagLayout());

        closeChartButton.setBackground(new java.awt.Color(153, 153, 153));
        closeChartButton.setBorderPainted(false);
        closeChartButton.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        closeChartButton.setFocusPainted(false);
        closeChartButton.setMaximumSize(new java.awt.Dimension(18, 22));
        closeChartButton.setMinimumSize(new java.awt.Dimension(18, 22));
        closeChartButton.setOpaque(false);
        closeChartButton.setPreferredSize(new java.awt.Dimension(18, 22));
        closeChartButton.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        closeChartButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeChartButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.FIRST_LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 5);
        layeredPanel.add(closeChartButton, gridBagConstraints);

        chartContainer.setBackground(new java.awt.Color(255, 255, 255));
        chartContainer.setMinimumSize(new java.awt.Dimension(400, 300));
        chartContainer.setPreferredSize(new java.awt.Dimension(400, 300));
        chartContainer.setLayout(new java.awt.GridBagLayout());
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 5);
        layeredPanel.add(chartContainer, gridBagConstraints);

        componentsManagerContainer.setMaximumSize(new java.awt.Dimension(250, 2147483647));
        componentsManagerContainer.setMinimumSize(new java.awt.Dimension(350, 300));
        componentsManagerContainer.setPreferredSize(new java.awt.Dimension(350, 300));
        componentsManagerContainer.setLayout(new java.awt.GridBagLayout());

        componentsListContainer.setViewportView(componentsList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 0.3;
        componentsManagerContainer.add(componentsListContainer, gridBagConstraints);

        componentsListLabel.setText("Components List: ");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        componentsManagerContainer.add(componentsListLabel, gridBagConstraints);

        componentsListTooltip.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        componentsListTooltip.setForeground(new java.awt.Color(120, 120, 120));
        componentsListTooltip.setText("[Drag and drop items on the table]");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        componentsManagerContainer.add(componentsListTooltip, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.VERTICAL;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.insets = new java.awt.Insets(15, 5, 5, 0);
        layeredPanel.add(componentsManagerContainer, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        add(layeredPanel, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void closeChartButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeChartButtonActionPerformed
        if(removeChartActionListener != null){
            removeChartActionListener.removeChart(this);
        }
    }//GEN-LAST:event_closeChartButtonActionPerformed

    private void toggleLegend(){
        if(chartPanel!=null){
            chartPanel.getChart().getLegend().setVisible(!chartPanel.getChart().getLegend().isVisible());
        }
    }
    
    public void addSeriesListener(SeriesToDisplayChangedListener seriesChangedListener)
    {
        seriesToDisplayChangedListener = seriesChangedListener;
    }
    
    private void seriesChangedAction(String[] names){
        seriesToDisplayChangedListener.addSeriesToDisplay(names);
    }

    public void addRemoveSeriesListener(ActionListener removeSeriesAL)
    {
        seriesTableContainer.addRemoveSeriesActionListener(removeSeriesAL);
    }
    
    public void addKeyPressedOnSeriesTableListener(KeyListener keyListener)
    {
        seriesTableContainer.getTable().addKeyListener(keyListener);
    }

    public void addSeriesColorChangedListener(CellEditorListener seriesColorChangedCEL)
    {
        seriesTableContainer.addSeriesColorChangedListener(seriesColorChangedCEL);
    }

    public void addRemoveChartListener(RemoveChartActionListener removePlotAL)
    {
        removeChartActionListener = removePlotAL;
    }

    public String getSelectedComponentName()
    {
        return (String) componentsList.getSelectedValue();
    }

    public String[] getRemovedSeriesNames()
    {
        return seriesTableContainer.getRemovedSeriesNames();
    }

    private Object getPlotColor(int idx)
    {
        if (chartPanel != null) {
            Color c = (Color) chartPanel.getChart().getXYPlot().getRenderer().getSeriesPaint(idx);
            return c == null ? new Color(255, 255, 255) : c;
        }
        return new Color(255, 255, 255);
    }

    public Color getSeriesColor(ColorEditor colorEditor)
    {
        return seriesTableContainer.getSeriesColor(colorEditor);
    }

    public int getSelectedRow()
    {
        return seriesTableContainer.getSelectedRow();
    }

    public String getSelectedSeriesName()
    {
        return seriesTableContainer.getSelectedSeriesName();
    }
    
    public void showComponentsList(boolean show)
    {
        componentsListContainer.setVisible(show);
        componentsListLabel.setVisible(show);
        componentsListTooltip.setVisible(show);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel chartContainer;
    private javax.swing.JButton closeChartButton;
    private javax.swing.JList componentsList;
    private javax.swing.JScrollPane componentsListContainer;
    private javax.swing.JLabel componentsListLabel;
    private javax.swing.JLabel componentsListTooltip;
    private javax.swing.JPanel componentsManagerContainer;
    private javax.swing.JLayeredPane layeredPanel;
    // End of variables declaration//GEN-END:variables
    public void showChart()
    {
        if (chartPanel != null) {

            GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new java.awt.Insets(0, 0, 0, 0);

            chartContainer.removeAll();
            chartContainer.invalidate();
            chartContainer.add(chartPanel, gridBagConstraints);
            chartContainer.validate();
        }
    }

    public void restoreChartAutoBands()
    {
        chartPanel.restoreAutoBounds();
    }

    public void initGUIComponents(String[] list, float[][] stats, String[] xLabels)
    {
        DefaultListModel listModel = new DefaultListModel();
        if (list.length > 0 && stats.length > 0) {
            for (int i = 0; i < list.length; i++) {
                listModel.addElement(list[i]);
            }
            componentsList.setModel(listModel);
            chartPanel.getChart().getXYPlot().getRenderer().setSeriesPaint(0, Core.PLOT_COLORS[0]);
        }else{
            componentsList.setModel(listModel);
        }
    }

    public void update(String[] list, float[][] stats, String[] xLabels)
    {

        Object[][] data = new Object[list.length][6];
        String xAxisLabel = "";
        
        for (int i = 0; i < xLabels.length; i++) {
            xAxisLabel += xLabels[i];
            xAxisLabel += (i<xLabels.length - 1) ?  ", " : "; "; 
        }

        for (int i = 0; i < list.length; i++) {
            data[i] = new Object[]{list[i], getPlotColor(i), stats[i][0], stats[i][1], stats[i][2], stats[i][3]};
        }

        seriesTableContainer.updateDataTable(data);
        seriesTableContainer.repaint();
        
        chartPanel.getChart().getXYPlot().getDomainAxis().setLabel(xAxisLabel);
    }

    public Color addNewColorToPlot(int idx)
    {
        Color c = Core.PLOT_COLORS[idx % Core.PLOT_COLORS.length];
        chartPanel.getChart().getXYPlot().getRenderer().setSeriesPaint(idx, c);
        return c;
    }

    public void paintPlot(Map<String, Color> seriesColorMap)
    {
        XYPlot plot = chartPanel.getChart().getXYPlot();
        int seriesCount = plot.getSeriesCount();
        String key;
        for (int i = 0; i < seriesCount; i++) {
            key = (String) plot.getDataset().getSeriesKey(i);
            plot.getRenderer().setSeriesPaint(i, seriesColorMap.get(key));
        }
    }

    private class MyMouseListener implements MouseListener
    {

        @Override
        public void mouseClicked(MouseEvent e)
        {

        }

        @Override
        public void mousePressed(MouseEvent e)
        {
            if (e.getButton() == 2) {
                restoreChartAutoBands();
            }
        }

        @Override
        public void mouseReleased(MouseEvent e)
        {

        }

        @Override
        public void mouseEntered(MouseEvent e)
        {

        }

        @Override
        public void mouseExited(MouseEvent e)
        {

        }

    }

}
