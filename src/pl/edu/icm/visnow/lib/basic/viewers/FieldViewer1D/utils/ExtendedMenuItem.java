package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.utils;

import java.awt.Component;
import javax.swing.JMenuItem;

/**
 *
 * @author norkap
 */
public class ExtendedMenuItem extends JMenuItem
{

    private Component ancestor;

    public ExtendedMenuItem(String name)
    {
        super.setText(name);
    }

    public Component getAncestor()
    {
        return ancestor;
    }

    public void setAncestor(Component ancestor)
    {
        this.ancestor = ancestor;
    }

}
