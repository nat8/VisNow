package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart;

import java.awt.Component;

/**
 *
 * @author Norbert_2
 */


public interface RemoveChartActionListener {
    public abstract void removeChart(Component comp);
}
