package pl.edu.icm.visnow.lib.basic.viewers.FieldViewer1D.ExtendedChart;

/**
 *
 * @author Norbert_2
 */


public interface SeriesToDisplayChangedListener {
    public abstract void addSeriesToDisplay(String[] names);
}
