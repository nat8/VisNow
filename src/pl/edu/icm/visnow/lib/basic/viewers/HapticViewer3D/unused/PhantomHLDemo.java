/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.edu.icm.visnow.lib.basic.viewers.HapticViewer3D.unused;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import openhaptics.OH;
import openhaptics.hdapi.Device;
import openhaptics.hdapi.HDException;
import openhaptics.hlapi.Context;
import openhaptics.hlapi.HLException;
import openhaptics.hlapi.effects.Viscous;

/**
 *
 * @author Krzysztof Madejski <krzysztof@madejscy.pl> ICM, University of Warsaw
 */
public class PhantomHLDemo
{

    static {
        OH.init();
    }

    public static void main(String[] args)
    {
        try {
            //            int hHD = Device.initDevice(Device.DEFAULT_PHANTOM);
            int hHD = Device.initDevice("DEFAULT_PHANTOM");
            Context context = Context.createContext(hHD);

            /*
             CustomEffect ce = new CustomEffect(context, new CustomEffectCallback() {

             public void computeForce(Vector3d force, Cache cache) {
             force.set(2,0,0);
             }

             public void startForceProc(Cache cache) {
             }

             public void stopForceProc(Cache cache) {
             }
             });
             *
             */
            Viscous ce = new Viscous(context, 1.0, 1.0);

            ce.start();

            System.out.println("Press any thing to stop force");
            try {
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

            ce.stop();

            System.out.println("Press any thing to exit");
            try {
                System.in.read();
            } catch (IOException ex) {
                Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
            }

            context.delete();
            Device.disableDevice(hHD);
        } catch (HLException ex) {
            Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (HDException ex) {
            Logger.getLogger(PhantomHLDemo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private PhantomHLDemo()
    {
    }
}
