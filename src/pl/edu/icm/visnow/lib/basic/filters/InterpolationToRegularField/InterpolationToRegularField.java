//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.InterpolationToRegularField;

import java.awt.Color;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.geometries.events.ColorEvent;
import pl.edu.icm.visnow.geometries.events.ColorListener;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.pick.Pick3DListener;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.NO_RUN;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.RUN_DYNAMICALLY;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.RUN_ONCE;
import pl.edu.icm.visnow.lib.basic.filters.ConvexHull.ConvexHullCore;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.interpolation.RegularInterpolation;
import static pl.edu.icm.visnow.lib.basic.filters.InterpolationToRegularField.InterpolationToRegularFieldShared.*;
import pl.edu.icm.visnow.lib.utils.interpolation.OptimizedBox;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class InterpolationToRegularField extends OutFieldVisualizationModule
{

    private int runQueue = 0;

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    protected GUI computeUI = null;
    protected boolean ignoreUI = false;

    protected Field inField = null;
    protected int trueDim;

    protected FloatLargeArray inCoords = null;
    
    protected InteractiveGlyph glyph = new InteractiveGlyph(BOX);
    protected InteractiveGlyphParams glyphParams = glyph.getParams();
    protected InteractiveGlyphGUI glyphGUI = glyph.getComputeUI();
    
    protected float currentVolume = 0;
    protected boolean autoubdateBox = true;
    
    
    private float[][] outAffine;
    
    public InterpolationToRegularField()
    {
        backGroundColorListener = new ColorListener()
        {
            @Override
            public void colorChoosen(ColorEvent e) {
                Color bgr = e.getSelectedColor();
                float[] bgrF = new float[4];
                bgr.getColorComponents(bgrF);
                if (glyph != null)
                    glyph.setColors(bgrF[0] + bgrF[1] + bgrF[2] > 1.5);
            }
        };
        glyphParams.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (glyphParams.getAdjusting() < 0) 
                    currentVolume = glyphParams.getVolume();
                computeUI.armRunButton();
            }
        });
        
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name == null || name.isEmpty())
                    return;
                switch (name){
                case RUNNING_MESSAGE_STRING:
                    if (parameters.get(RUNNING_MESSAGE) == RUN_ONCE) {
                        runQueue++;
                        startAction();
                    } else if (parameters.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                        startAction();
                    break;
                case AUTO_STRING:
                    if (parameters.get(AUTO)) {
                        outAffine = OptimizedBox.optimizeInitBox(inCoords, trueDim, glyphParams);
                        glyph.getGlyph().setShowReper(false);
                        autoubdateBox = glyphParams.isAutoUpdateBox();
                        glyphParams.setAutoUpdateBox(true);
                    }
                    else {
                        glyph.getGlyph().setShowReper(true);
                        glyphParams.setAutoUpdateBox(autoubdateBox);
                    }
                    computeUI.armRunButton();
                    break;
                default:
                    computeUI.armRunButton();
                    break;
                }
            }
        });

        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                computeUI.addGlyphUI(glyphGUI);
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });

        outObj.addNode(glyph);
    }

    @Override
    public Pick3DListener getPick3DListener() {
        return glyph.getPick3DListener();
    }
    
    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(GLOBAL_RESOLUTION, true),
            new Parameter<>(RESOLUTION, 256),
            new Parameter<>(RESOLUTION0, 100),
            new Parameter<>(RESOLUTION1, 100),
            new Parameter<>(RESOLUTION2, 100),
            new Parameter<>(RESET_BOX, false),
            new Parameter<>(IS_OUTPUT, false),
            new Parameter<>(AUTO, true),
            new Parameter<>(META_TRUE_NSPACE, 3),
            new Parameter<>(META_DIAMETER, 1f),
            new Parameter<>(RUNNING_MESSAGE, NO_RUN)
        };
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        parameters.set(META_TRUE_NSPACE, inField.getTrueNSpace());
        parameters.set(META_DIAMETER, inField.getDiameter());
        if (resetParameters) {
            parameters.set(AUTO, true);
            parameters.set(GLOBAL_RESOLUTION, true);
            parameters.set(RESOLUTION, 256);
            parameters.set(RESOLUTION0, 100);
            parameters.set(RESOLUTION1, 100);
            parameters.set(RESOLUTION2, 100);
        }
        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully, setRunButtonPending);
    }


    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null) {
            outField = null;
            prepareOutputGeometry();
            show();
            setOutputValue("outField", null);
            return;
        }
        Parameters p = parameters.getReadOnlyClone();
        Field newField = ((VNField) getInputFirstValue("inField")).getField();
        boolean isNewField = !isFromVNA() && newField != inField;
        if (isNewField) {
            inField = newField;
            switch (inField.getTrueNSpace()) {
            case 2:
                trueDim = 2;
                glyph.setType(RECTANGLE);
                break;
            case 3:
                trueDim = 3;
                glyph.setType(BOX);
                break;
            default:
                trueDim = 0;
                return;
            }
            inCoords = new FloatLargeArray(ConvexHullCore.convexHullCoords(inField));
            synchronized (parameters) {
                validateParamsAndSetSmart(isNewField);
                p = parameters.getReadOnlyClone();
            }
            glyph.setField(inField);
            notifyGUIs(p, isFromVNA() || isNewField, isFromVNA() || isNewField);
            computeUI.resetAuto();
            outAffine = OptimizedBox.optimizeInitBox(inCoords, trueDim, glyphParams);
            glyphParams.setAutoUpdateBox(true);
            glyph.getGlyph().setShowReper(false);
            computeUI.armRunButton();
            outObj.clearAllGeometry();
            outObj.addNode(glyph);
            setOutputValue("outField", null);
        }
        if (p != null && (runQueue > 0 || p.get(RUNNING_MESSAGE) == RUN_DYNAMICALLY)) {
            runQueue = Math.max(runQueue - 1, 0);
            if (trueDim < 2)
                return;
            if (outAffine == null || !parameters.get(AUTO)) 
                outAffine = RegularInterpolation.outAffineFromboxVerts(glyph.getBoxVerts(trueDim), trueDim);
            
            if (parameters.get(GLOBAL_RESOLUTION))
                outField = outRegularField = 
                    RegularInterpolation.interpolate(inField, 
                                                     outAffine, 
                                                     parameters.get(RESOLUTION));
            else
                outField = outRegularField = 
                    RegularInterpolation.interpolate(inField, 
                                                     outAffine, 
                                                     new int[] {parameters.get(RESOLUTION0), 
                                                                parameters.get(RESOLUTION1), 
                                                                parameters.get(RESOLUTION2)});
            outAffine = null;
            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
            outObj.addNode(glyph);
            computeUI.disarmRunButton();
        }
    }
}
