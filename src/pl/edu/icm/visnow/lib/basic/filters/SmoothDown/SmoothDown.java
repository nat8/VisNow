///<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.SmoothDown;

import java.util.Arrays;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.gui.widgets.RunButton.RunState.*;
import static pl.edu.icm.visnow.lib.basic.filters.SmoothDown.SmoothDownShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.visnow.lib.utils.field.FieldSmoothDown;
import pl.edu.icm.visnow.system.main.VisNow;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class SmoothDown extends OutFieldVisualizationModule
{

    private SmoothDownGUI computeUI = new SmoothDownGUI();
    protected RegularField inField = null;
    protected boolean fromGUI = false;
    private int runQueue = 0;
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private final int nThreads = VisNow.availableProcessors();

    public SmoothDown()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                if (name.equals(SMOOTHDOWN_RUNNING_MESSAGE.getName()) && parameters.get(SMOOTHDOWN_RUNNING_MESSAGE) == RUN_ONCE) {
                    runQueue++;
                    startAction();
                } else if (parameters.get(SMOOTHDOWN_RUNNING_MESSAGE) == RUN_DYNAMICALLY)
                    startIfNotInQueue();

            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new SmoothDownGUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(SMOOTHDOWN_SIGMA, 1.f),
            new Parameter<>(SMOOTHDOWN_DOWNSIZE, new int[]{3, 3, 3}),
            new Parameter<>(SMOOTHDOWN_RUNNING_MESSAGE, NO_RUN)
        };
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(parameters, resetFully, setRunButtonPending);
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);
        int[] dims = inField.getDims();

        int[] downsizes = parameters.get(SMOOTHDOWN_DOWNSIZE);

        if (!resetParameters) { //validate
            for (int i = 0; i < inField.getDimNum(); i++) {
                int maxDownsize = dims[i] / 2;
                if (downsizes[i] > maxDownsize)
                    if (downsizes[i] < maxDownsize + 2) //"smart" :(
                        downsizes[i] = maxDownsize;
                    else
                        downsizes[i] = Math.min(3, maxDownsize); //default (3) :(
            }
        } else { //set smart
            int scale = dims.length == 3 ? 100 : dims.length == 2 ? 1000 : 1000_000;
            for (int i = 0; i < dims.length; i++) downsizes[i] = (dims[i] + scale - 1) / scale;
        }
        parameters.set(SMOOTHDOWN_DOWNSIZE, downsizes);
        parameters.setParameterActive(true);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            RegularField newField = ((VNRegularField) getInputFirstValue("inField")).getField();
            boolean isDifferentField = !isFromVNA() && (inField == null || !Arrays.equals(inField.getDims(), newField.getDims()));
            boolean isNewField = !isFromVNA() && newField != inField;
            inField = newField;

            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
                p = parameters.getReadOnlyClone();
            }

            notifyGUIs(p, isFromVNA() || isDifferentField, isFromVNA() || isNewField);

            if (runQueue > 0 || p.get(SMOOTHDOWN_RUNNING_MESSAGE) == RUN_DYNAMICALLY) {
                runQueue = Math.max(runQueue - 1, 0);
                outRegularField = FieldSmoothDown.smoothDown(inField, p.get(SMOOTHDOWN_DOWNSIZE), p.get(SMOOTHDOWN_SIGMA), nThreads);
                if (outRegularField == null) {
                    setOutputValue("outField", null);
                } else {
                    setOutputValue("outField", new VNRegularField(outRegularField));
                }
                outField = outRegularField;
                prepareOutputGeometry();
                show();
            }
        }
    }
}
