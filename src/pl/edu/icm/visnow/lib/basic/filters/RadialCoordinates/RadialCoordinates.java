//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.RadialCoordinates;

import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.geometries.parameters.RegularField3DParams;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.filters.RadialCoordinates.RadialCoordinatesShared.*;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNRegularField;

/**
 *
 ** @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class RadialCoordinates extends OutFieldVisualizationModule
{
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    private GUI computeUI = null;
    protected RegularField3DParams regularField3DmapParams = new RegularField3DParams();
    protected RegularField inField = null;
    protected RegularField lastInField = null;
    protected float[] coords = null;
    protected int[] dims = null;
    protected boolean transformVectors = true;
    protected float[] jacobian0 = null;
    protected float[] jacobian1 = null;
    protected float[] jacobian2 = null;
    static Logger logger = Logger.getLogger(RadialCoordinates.class);

    public RadialCoordinates()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startIfNotInQueue();
            }

        });
        SwingInstancer.swingRunAndWait(
                new Runnable()
                {
                    public void run()
                    {
                        computeUI = new GUI();
                        computeUI.setParameters(parameters);
                        ui.addComputeGUI(computeUI);
                        setPanel(ui);
                    }
                });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(MAP_TYPE, MAP_SPHERICAL),
            new Parameter<>(RADIUS_AXIS, 0),
            new Parameter<>(RADIUS_MIN, 0.0f),
            new Parameter<>(RADIUS_MAX, 1.0f),
            new Parameter<>(PHI_AXIS, 1),
            new Parameter<>(PHI_MIN, 0.0f),
            new Parameter<>(PHI_MAX, (float) PI / 2),
            new Parameter<>(PSI_AXIS, 2),
            new Parameter<>(PSI_MIN, 0.0f),
            new Parameter<>(PSI_MAX, (float) PI),
            new Parameter<>(HEIGHT_AXIS, 2),
            new Parameter<>(HEIGHT_MIN, 0.0f),
            new Parameter<>(HEIGHT_MAX, 1.0f),
            new Parameter<>(ADJUSTING, false),
            new Parameter<>(TRANSFORM_VECTORS, true),
            new Parameter<>(META_NUMBER_OF_DIMENSIONS, 3)
        };
    }

    private void updateCoords(Parameters p)
    {
        if (p.get(MAP_TYPE).equals(MAP_SPHERICAL)) {
            int ir = p.get(RADIUS_AXIS);
            int iphi = p.get(PHI_AXIS);
            int ipsi = p.get(PSI_AXIS);
            if (ir >= dims.length)
                ir = CONSTANT_VALUE;
            if (iphi >= dims.length)
                iphi = CONSTANT_VALUE;
            if (ipsi >= dims.length)
                ipsi = CONSTANT_VALUE;
            float dr = 0;
            double dphi = 0, dpsi = 0;
            double drdi, drdj, drdk, dcphidi, dcphidj, dcphidk, dsphidi, dsphidj, dsphidk, dcpsidi, dcpsidj, dcpsidk, dspsidi, dspsidj, dspsidk;
            drdi = drdj = drdk
                    = dcphidi = dcphidj = dcphidk
                    = dsphidi = dsphidj = dsphidk
                    = dcpsidi = dcpsidj = dcpsidk
                    = dspsidi = dspsidj = dspsidk = 0;
            float r0 = p.get(RADIUS_MIN);
            if (ir == CONSTANT_VALUE) {
                dr = 0;
                drdi = drdj = drdk = 0;
            } else
                dr = (p.get(RADIUS_MAX) - r0) / (dims[ir] - 1);
            double phi0 = (float)p.get(PHI_MIN);
            if (iphi == CONSTANT_VALUE) {
                dphi = 0;
                dcphidi = dcphidj = dcphidk = dsphidi = dsphidj = dsphidk = 0;
            } else
                dphi = (p.get(PHI_MAX) - phi0) / (dims[iphi] - 1);
            double psi0 = (float)p.get(PSI_MIN);
            if (ipsi == CONSTANT_VALUE) {
                dpsi = 0;
                dcpsidi = dcpsidj = dcpsidk = dspsidi = dspsidj = dspsidk = 0;
            } else
                dpsi = (p.get(PSI_MAX) - psi0) / (dims[ipsi] - 1);
            double phi = phi0;
            double psi = psi0;
            double cphi = cos(phi);
            double sphi = sin(phi);
            double cpsi = cos(psi);
            double spsi = sin(psi);
            float r = r0;
            switch (dims.length) {
                case 3:
                    for (int i = 0, l = 0; i < dims[2]; i++) {
                        if (ir == 2) {
                            r = r0 + i * dr;
                            drdi = dr;
                            drdj = drdk = 0;
                        }
                        if (iphi == 2) {
                            phi = phi0 + i * dphi;
                            cphi = cos(phi);
                            sphi = sin(phi);
                            dcphidi = -dphi * sphi;
                            dcphidj = dcphidk = 0;
                            dsphidi = dphi * cphi;
                            dsphidj = dsphidk = 0;
                        }
                        if (ipsi == 2) {
                            psi = psi0 + i * dpsi;
                            cpsi = cos(psi);
                            spsi = sin(psi);
                            dcpsidi = -dpsi * spsi;
                            dcphidj = dcpsidk = 0;
                            dspsidi = dpsi * cpsi;
                            dspsidj = dspsidk = 0;
                        }
                        for (int j = 0; j < dims[1]; j++) {
                            if (ir == 1) {
                                r = r0 + j * dr;
                                drdj = dr;
                                drdi = drdk = 0;
                            }
                            if (iphi == 1) {
                                phi = phi0 + j * dphi;
                                cphi = cos(phi);
                                sphi = sin(phi);
                                dcphidj = -dphi * sphi;
                                dcphidi = dcphidk = 0;
                                dsphidj = dphi * cphi;
                                dsphidi = dsphidk = 0;
                            }
                            if (ipsi == 1) {
                                psi = psi0 + j * dpsi;
                                cpsi = cos(psi);
                                spsi = sin(psi);
                                dcpsidj = -dpsi * spsi;
                                dcpsidi = dcpsidk = 0;
                                dspsidj = dpsi * cpsi;
                                dspsidi = dspsidk = 0;
                            }

                            for (int k = 0; k < dims[0]; k++) {
                                if (ir == 0) {
                                    r = r0 + k * dr;
                                    drdk = dr;
                                    drdi = drdj = 0;
                                }
                                if (iphi == 0) {
                                    phi = phi0 + k * dphi;
                                    cphi = cos(phi);
                                    sphi = sin(phi);
                                    dcphidk = -dphi * sphi;
                                    dcphidi = dcphidj = 0;
                                    dsphidk = dphi * cphi;
                                    dsphidi = dsphidj = 0;
                                }
                                if (ipsi == 0) {
                                    psi = psi0 + k * dpsi;
                                    cpsi = cos(psi);
                                    spsi = sin(psi);
                                    dcpsidk = -dpsi * spsi;
                                    dcpsidi = dcpsidi = 0;
                                    dspsidk = dpsi * cpsi;
                                    dspsidi = dspsidi = 0;
                                }
                                coords[l] = (float) (r * cphi * cpsi);
                                coords[l + 1] = (float) (r * cphi * spsi);
                                coords[l + 2] = (float) (r * sphi);
                                if (transformVectors) {
                                    jacobian0[l] = (float) (drdk * cphi * cpsi + r * dcphidk * cpsi + r * cphi * dcpsidk);
                                    jacobian0[l + 1] = (float) (drdk * cphi * spsi + r * dcphidk * spsi + r * cphi * dspsidk);
                                    jacobian0[l + 2] = (float) (drdk * sphi + r * dsphidk);
                                    jacobian1[l] = (float) (drdj * cphi * cpsi + r * dcphidj * cpsi + r * cphi * dcpsidj);
                                    jacobian1[l + 1] = (float) (drdj * cphi * spsi + r * dcphidj * spsi + r * cphi * dspsidj);
                                    jacobian1[l + 2] = (float) (drdj * sphi + r * dsphidj);
                                    jacobian2[l] = (float) (drdi * cphi * cpsi + r * dcphidi * cpsi + r * cphi * dcpsidi);
                                    jacobian2[l + 1] = (float) (drdi * cphi * spsi + r * dcphidi * spsi + r * cphi * dspsidi);
                                    jacobian2[l + 2] = (float) (drdi * sphi + r * dsphidi);
                                }
                                l += 3;
                            }
                        }
                    }
                    break;
                case 2:
                    for (int j = 0, l = 0; j < dims[1]; j++) {
                        if (ir == 1) {
                            r = r0 + j * dr;
                            drdj = dr;
                            drdk = 0;
                        }
                        if (iphi == 1) {
                            phi = phi0 + j * dphi;
                            cphi = cos(phi);
                            sphi = sin(phi);
                            dcphidj = -dphi * sphi;
                            dsphidj = dphi * cphi;
                            dcphidk = dsphidk = 0;
                        }
                        if (ipsi == 1) {
                            psi = psi0 + j * dpsi;
                            cpsi = cos(psi);
                            spsi = sin(psi);
                            dcpsidj = -dpsi * spsi;
                            dspsidj = dpsi * cpsi;
                            dcpsidk = dspsidk = 0;
                        }

                        for (int k = 0; k < dims[0]; k++) {
                            if (ir == 0) {
                                r = r0 + k * dr;
                                drdk = dr;
                                drdj = 0;
                            }
                            if (iphi == 0) {
                                phi = phi0 + k * dphi;
                                cphi = cos(phi);
                                sphi = sin(phi);
                                dcphidk = -dphi * sphi;
                                dsphidk = dphi * cphi;
                                dcphidj = dsphidj = 0;
                            }
                            if (ipsi == 0) {
                                psi = psi0 + k * dpsi;
                                cpsi = cos(psi);
                                spsi = sin(psi);
                                dcpsidk = -dpsi * spsi;
                                dspsidk = dpsi * cpsi;
                                dcpsidj = dspsidj = 0;
                            }
                            coords[l] = (float) (r * cphi * cpsi);
                            coords[l + 1] = (float) (r * cphi * spsi);
                            coords[l + 2] = (float) (r * sphi);
                            if (transformVectors) {
                                jacobian0[l] = (float) (drdk * cphi * cpsi + r * dcphidk * cpsi + r * cphi * dcpsidk);
                                jacobian0[l + 1] = (float) (drdk * cphi * spsi + r * dcphidk * spsi + r * cphi * dspsidk);
                                jacobian0[l + 2] = (float) (drdk * sphi + r * dsphidk);
                                jacobian1[l] = (float) (drdj * cphi * cpsi + r * dcphidj * cpsi + r * cphi * dcpsidj);
                                jacobian1[l + 1] = (float) (drdj * cphi * spsi + r * dcphidj * spsi + r * cphi * dspsidj);
                                jacobian1[l + 2] = (float) (drdj * sphi + r * dsphidj);
                            }
                            l += 3;
                        }
                    }
                    break;
                case 1:
                    for (int k = 0, l = 0; k < dims[0]; k++) {
                        if (ir == 0) {
                            r = r0 + k * dr;
                            drdk = dr;
                            drdj = 0;
                        }
                        if (iphi == 0) {
                            phi = phi0 + k * dphi;
                            cphi = cos(phi);
                            sphi = sin(phi);
                            dcphidk = -dphi * sphi;
                            dsphidk = dphi * cphi;
                        }
                        if (ipsi == 0) {
                            psi = psi0 + k * dpsi;
                            cpsi = cos(psi);
                            spsi = sin(psi);
                            dcpsidk = -dpsi * spsi;
                            dspsidk = dpsi * cpsi;
                        }
                        coords[l] = (float) (r * cphi * cpsi);
                        coords[l + 1] = (float) (r * cphi * spsi);
                        coords[l + 2] = (float) (r * sphi);
                        if (transformVectors) {
                            jacobian0[l] = (float) (drdk * cphi * cpsi + r * dcphidk * cpsi + r * cphi * dcpsidk);
                            jacobian0[l + 1] = (float) (drdk * cphi * spsi + r * dcphidk * spsi + r * cphi * dspsidk);
                            jacobian0[l + 2] = (float) (drdk * sphi + r * dsphidk);
                        }
                        l += 3;
                    }
                    break;
            }
        } else {
            int ir = p.get(RADIUS_AXIS);
            int iphi = p.get(PHI_AXIS);
            int iz = p.get(HEIGHT_AXIS);
            if (ir >= dims.length)
                ir = CONSTANT_VALUE;
            if (iphi >= dims.length)
                iphi = CONSTANT_VALUE;
            if (iz >= dims.length)
                iz = CONSTANT_VALUE;
            float dr = 0, dz = 0;
            double dphi = 0;
            double drdi, drdj, drdk, dcphidi, dcphidj, dcphidk, dsphidi, dsphidj, dsphidk, dzdi, dzdj, dzdk;
            drdi = drdj = drdk
                    = dcphidi = dcphidj = dcphidk
                    = dsphidi = dsphidj = dsphidk
                    = dzdi = dzdj = dzdk = 0;
            float r0 = p.get(RADIUS_MIN);
            if (ir != CONSTANT_VALUE)
                dr = (p.get(RADIUS_MAX) - r0) / (dims[ir] - 1);
            double phi0 = (float)p.get(PHI_MIN);
            if (iphi != CONSTANT_VALUE)
                dphi = (p.get(PHI_MAX) - phi0) / (dims[iphi] - 1);
            float z0 = p.get(HEIGHT_MIN);
            if (iz != CONSTANT_VALUE)
                dz = (p.get(HEIGHT_MAX) - z0) / (dims[iz] - 1);
            double phi = phi0;
            double cphi = cos(phi);
            double sphi = sin(phi);
            float r = r0;
            float z = z0;
            switch (dims.length) {
                case 3:
                    for (int i = 0, l = 0; i < dims[2]; i++) {
                        if (ir == 2) {
                            r = r0 + i * dr;
                            drdi = dr;
                            drdj = drdk = 0;
                        }
                        if (iphi == 2) {
                            phi = phi0 + i * dphi;
                            cphi = cos(phi);
                            sphi = sin(phi);
                            dcphidi = -dphi * sphi;
                            dcphidj = dcphidk = 0;
                            dsphidi = dphi * cphi;
                            dsphidj = dsphidk = 0;
                        }
                        if (iz == 2) {
                            z = z0 + i * dz;
                            dzdi = dz;
                            dzdj = dzdk = 0;
                        }
                        for (int j = 0; j < dims[1]; j++) {
                            if (ir == 1) {
                                r = r0 + j * dr;
                                drdj = dr;
                                drdi = drdk = 0;
                            }
                            if (iphi == 1) {
                                phi = phi0 + j * dphi;
                                cphi = cos(phi);
                                sphi = sin(phi);
                                dcphidj = -dphi * sphi;
                                dcphidi = dcphidk = 0;
                                dsphidj = dphi * cphi;
                                dsphidi = dsphidk = 0;
                            }
                            if (iz == 1) {
                                z = z0 + j * dz;
                                dzdj = dz;
                                dzdi = dzdk = 0;
                            }
                            for (int k = 0; k < dims[0]; k++) {
                                if (ir == 0) {
                                    r = r0 + k * dr;
                                    drdk = dr;
                                    drdi = drdj = 0;
                                }
                                if (iphi == 0) {
                                    phi = phi0 + k * dphi;
                                    cphi = cos(phi);
                                    sphi = sin(phi);
                                    dcphidk = -dphi * sphi;
                                    dcphidi = dcphidj = 0;
                                    dsphidk = dphi * cphi;
                                    dsphidi = dsphidj = 0;
                                }
                                if (iz == 0) {
                                    z = z0 + k * dz;
                                    dzdk = dr;
                                    dzdi = dzdj = 0;
                                }
                                coords[l] = (float) (r * cphi);
                                coords[l + 1] = (float) (r * sphi);
                                coords[l + 2] = z;
                                if (transformVectors) {
                                    jacobian0[l] = (float) (drdk * cphi + r * dcphidk);
                                    jacobian0[l + 1] = (float) (drdk * sphi + r * dsphidk);
                                    jacobian0[l + 2] = (float) (dzdk);
                                    jacobian1[l] = (float) (drdj * cphi + r * dcphidj);
                                    jacobian1[l + 1] = (float) (drdj * sphi + r * dsphidj);
                                    jacobian1[l + 2] = (float) (dzdj);
                                    jacobian2[l] = (float) (drdi * cphi + r * dcphidi);
                                    jacobian2[l + 1] = (float) (drdi * sphi + r * dsphidi);
                                    jacobian2[l + 2] = (float) (dzdi);
                                }
                                l += 3;
                            }
                        }
                    }
                    break;
                case 2:
                    for (int j = 0, l = 0; j < dims[1]; j++) {
                        if (ir == 1) {
                            r = r0 + j * dr;
                            drdj = dr;
                            drdk = 0;
                        }
                        if (iphi == 1) {
                            phi = phi0 + j * dphi;
                            cphi = cos(phi);
                            sphi = sin(phi);
                            dcphidj = -dphi * sphi;
                            dsphidj = dphi * cphi;
                            dcphidk = dsphidk = 0;
                        }
                        if (iz == 1) {
                            z = z0 + j * dz;
                            dzdj = dz;
                            dzdk = 0;
                        }
                        for (int k = 0; k < dims[0]; k++) {
                            if (ir == 0) {
                                r = r0 + k * dr;
                                drdk = dr;
                                drdj = 0;
                            }
                            if (iphi == 0) {
                                phi = phi0 + k * dphi;
                                cphi = cos(phi);
                                sphi = sin(phi);
                                dcphidk = -dphi * sphi;
                                dsphidk = dphi * cphi;
                                dcphidj = dsphidj = 0;
                            }
                            if (iz == 0) {
                                z = z0 + k * dz;
                                dzdk = dr;
                                dzdi = dzdj = 0;
                            }
                            coords[l] = (float) (r * cphi);
                            coords[l + 1] = (float) (r * sphi);
                            coords[l + 2] = z;
                            if (transformVectors) {
                                jacobian0[l] = (float) (drdk * cphi + r * dcphidk);
                                jacobian0[l + 1] = (float) (drdk * sphi + r * dsphidk);
                                jacobian0[l + 2] = (float) (dzdk);
                                jacobian1[l] = (float) (drdj * cphi + r * dcphidj);
                                jacobian1[l + 1] = (float) (drdj * sphi + r * dsphidj);
                                jacobian1[l + 2] = (float) (dzdj);
                            }
                            l += 3;
                        }
                    }
                    break;
                case 1:
                    for (int k = 0, l = 0; k < dims[0]; k++) {
                        if (ir == 0) {
                            r = r0 + k * dr;
                            drdk = dr;
                        }
                        if (iphi == 0) {
                            phi = phi0 + k * dphi;
                            cphi = cos(phi);
                            sphi = sin(phi);
                            dcphidk = -dphi * sphi;
                            dsphidk = dphi * cphi;
                        }
                        if (iz == 0) {
                            z = z0 + k * dz;
                            dzdk = dz;
                        }
                        coords[l] = (float) (r * cphi);
                        coords[l + 1] = (float) (r * sphi);
                        coords[l + 2] = z;
                        if (transformVectors) {
                            jacobian0[l] = (float) (drdk * cphi + r * dcphidk);
                            jacobian0[l + 1] = (float) (drdk * sphi + r * dsphidk);
                            jacobian0[l + 2] = (float) (dzdk);
                        }
                        l += 3;
                    }
                    break;
            }
        }
        outRegularField.setCurrentCoords(new FloatLargeArray(coords));
        
        if (transformVectors) {
            float[][] invAffine = inField.getInvAffine();
            int nDim = inField.getDimNum();
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).getVectorLength() == nDim ) {
                    float[] outVdata;
                    DataArray inDA = inField.getComponent(i);
                    outVdata = (float[])outRegularField.getComponent("rad_" + inDA.getName()).getRawArray().getData();
                    float[] outV = new float[3];
                    for (int j = 0; j < outRegularField.getNNodes(); j++) {
                        float[] inV = inDA.getFloatElement(j);
                        for (int k = 0; k < 3; k++)
                            outV[k] = 0;
                        float[] u = {0, 0, 0};
                        for (int k = 0; k < inV.length; k++)
                            for (int l = 0; l < inV.length; l++)
                                u[k] += invAffine[k][l] * inV[l];
                        for (int k = 0; k < 3; k++)
                            outV[k] = jacobian0[3 * j + k] * u[0];
                        if (inV.length > 1)
                            for (int k = 0; k < 3; k++)
                                outV[k] += jacobian1[3 * j + k] * u[1];
                        if (inV.length > 2)
                            for (int k = 0; k < 3; k++)
                                outV[k] += jacobian2[3 * j + k] * u[2];
                        System.arraycopy(outV, 0, outVdata, 3 * j, 3);
                    }
                    outRegularField.getComponent("rad_" + inDA.getName()).recomputeStatistics();
            }
        }
    }

    public void updateField()
    {
        dims = inField.getDims();
        outRegularField = new RegularField(dims);
        outRegularField.setAffine(inField.getAffine());
        outRegularField.setPreferredExtents(inField.getPreferredExtents(), inField.getPreferredPhysicalExtents());
        outField = outRegularField;
   
        int nNodes = 1;
        for (int i = 0; i < dims.length; i++)
            nNodes *= dims[i];
        coords = new float[3 * nNodes];
        for (int i = 0; i < inField.getNComponents(); i++)
            outRegularField.addComponent(inField.getComponent(i).cloneShallow());
        if (transformVectors) {
            jacobian0 = new float[3 * (int) inField.getNNodes()];
            if (inField.getDims().length > 1)
                jacobian1 = new float[3 * (int) inField.getNNodes()];
            if (inField.getDims().length > 2)
                jacobian2 = new float[3 * (int) inField.getNNodes()];
    //        outRegularField.addComponent(DataArray.create(jacobian0, 3, "d_di"));
    //        if (dims.length > 1)
    //            outRegularField.addComponent(DataArray.create(jacobian1, 3, "d_dj"));
    //        if (dims.length > 2)
    //            outRegularField.addComponent(DataArray.create(jacobian2, 3, "d_dk"));
            for (int i = 0; i < inField.getNComponents(); i++)
                if (inField.getComponent(i).getVectorLength() == inField.getDimNum()) {
                    float[] outV = new float[3 * (int) inField.getNNodes()];
                    outRegularField.addComponent(DataArray.create(outV, 3, "rad_" + inField.getComponent(i).getName()));
                }
        }
    }

    private void validateParamsAndSetSmart(boolean resetParameters)
    {
        parameters.setParameterActive(false);

        int dimNum = inField.getDimNum();
        parameters.set(META_NUMBER_OF_DIMENSIONS, dimNum);

        if (parameters.get(RADIUS_AXIS) != CONSTANT_VALUE) parameters.set(RADIUS_AXIS, Math.min(dimNum - 1, parameters.get(RADIUS_AXIS)));
        if (parameters.get(PHI_AXIS) != CONSTANT_VALUE) parameters.set(PHI_AXIS, Math.min(dimNum - 1, parameters.get(PHI_AXIS)));
        if (parameters.get(PSI_AXIS) != CONSTANT_VALUE) parameters.set(PSI_AXIS, Math.min(dimNum - 1, parameters.get(PSI_AXIS)));
        if (parameters.get(HEIGHT_AXIS) != CONSTANT_VALUE) parameters.set(HEIGHT_AXIS, Math.min(dimNum - 1, parameters.get(HEIGHT_AXIS)));

        //TODO: this should be changed anyway (user values should be remembered and auto values should be set to smart)
        if (resetParameters) {
            //smart version: for n-dim field at least n different axes used as variable
            int[] dimUsed = new int[3];

            if (parameters.get(RADIUS_AXIS) != CONSTANT_VALUE) dimUsed[parameters.get(RADIUS_AXIS)] = 1;
            if (parameters.get(PHI_AXIS) != CONSTANT_VALUE) dimUsed[parameters.get(PHI_AXIS)] = 1;
            if (parameters.get(PSI_AXIS) != CONSTANT_VALUE) dimUsed[parameters.get(PSI_AXIS)] = 1;

            if (dimUsed[0] + dimUsed[1] + dimUsed[2] < dimNum) {
                int dim = 0;
                parameters.set(RADIUS_AXIS, dimNum == 3 ? dim++ : CONSTANT_VALUE);
                parameters.set(PHI_AXIS, dimNum >= 2 ? dim++ : CONSTANT_VALUE);
                parameters.set(PSI_AXIS, dim);
                parameters.set(HEIGHT_AXIS, dim);
            }

            if (parameters.get(RADIUS_AXIS) == CONSTANT_VALUE) parameters.set(RADIUS_MIN, 1.0f);
        }

        parameters.setParameterActive(true);
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy, resetFully);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") != null) {
            //1. get new field
            RegularField newInField = ((VNRegularField) getInputFirstValue("inField")).getField();
            //1a. set "different Field" flag
            boolean isDifferentField = !isFromVNA() && (inField == null || inField.getDimNum() != newInField.getDimNum());
            inField = newInField;

            //2. validate params             
            Parameters p;
            synchronized (parameters) {
                validateParamsAndSetSmart(isDifferentField);
            //2b. clone param (local read-only copy)
                p = parameters.getReadOnlyClone();
            }
            //3. update gui (GUI doesn't change parameters !!!!!!!!!!!!! - assuming correct set of parameters)
            notifyGUIs(p, isFromVNA() || isDifferentField, false);

            //4. run computation and propagate
            transformVectors = p.get(TRANSFORM_VECTORS);
            updateField();
            updateCoords(p);
            if (regularFieldGeometry != null)
                regularFieldGeometry.updateCoords();

            setOutputValue("outField", new VNRegularField(outRegularField));
            prepareOutputGeometry();
            show();
        }
    }
}
