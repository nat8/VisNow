//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.filters.Boundaries;

import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNField;
import pl.edu.icm.visnow.lib.types.VNIrregularField;

/**
 * @author Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling
 */
public class Boundaries extends OutFieldVisualizationModule
{

    /**
     * Creates a new instance of SurfaceSmoother
     */
    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;

    public Boundaries()
    {
        setPanel(ui);
    }

    @Override
    public void onActive()
    {
        if (getInputFirstValue("inField") == null || ((VNField) getInputFirstValue("inField")).getField() == null)
            return;
        IrregularField inField = ((VNIrregularField) getInputFirstValue("inField")).getField();
        long nNodes = inField.getNNodes();
        IntLargeArray nodeIndices = new IntLargeArray(nNodes, false);
        for (long i = 0; i < nNodes; i++)
            nodeIndices.setInt(i, -1);
        for (CellSet cellSet : inField.getCellSets()) {
            for (int i = 0; i < cellSet.getBoundaryCellArrays().length; i++) {
                CellArray ca = cellSet.getBoundaryCellArray(CellType.getType(i));
                if (ca == null || ca.getType() == CellType.POINT)
                    continue;
                if (nNodes > Integer.MAX_VALUE) {

                } else {
                    int[] nodes = ca.getNodes();
                    for (int j = 0; j < nodes.length; j++)
                        nodeIndices.setInt(nodes[j], 1);
                }
            }
        }
        int nBoundaryNodes = 0;
        for (long i = 0; i < nNodes; i++)
            if (nodeIndices.getInt(i) == 1) {
                nodeIndices.setInt(i, nBoundaryNodes);
                if (nBoundaryNodes == Integer.MAX_VALUE) {
                    // exit - too many boundary nodes
                }
                nBoundaryNodes += 1;
            }
        outIrregularField = new IrregularField(nBoundaryNodes);
        FloatLargeArray coords = inField.getCurrentCoords();
        float[] bCoords = new float[3 * nBoundaryNodes];
        int k = 0;
        for (long i = 0; i < nNodes; i++)
            if (nodeIndices.getInt(i) >= 0) {
                for (int j = 0; j < 3; j++)
                    bCoords[3 * k + j] = coords.getFloat(3 * i + j);
                k += 1;
            }

        for (DataArray da : inField.getComponents()) {
            int vlen = da.getVectorLength();
            k = 0;            
            if(da.getType() != DataArrayType.FIELD_DATA_COMPLEX) {
                LargeArray inData = da.getRawArray();
                LargeArray outData = null;
                switch(da.getType()) {
                    case FIELD_DATA_BYTE:
                        outData = new UnsignedByteLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_SHORT:
                        outData = new ShortLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_INT:
                        outData = new IntLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_FLOAT:
                        outData = new FloatLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_DOUBLE:
                        outData = new DoubleLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_LOGIC:
                        outData = new LogicLargeArray(nBoundaryNodes * vlen);
                        break;
                    case FIELD_DATA_STRING:
                        outData = new StringLargeArray(nBoundaryNodes * vlen);
                        break;
                }
                if(outData != null) {
                    for (long i = 0; i < nNodes; i++) {
                        if (nodeIndices.getInt(i) >= 0) {
                            for (int j = 0; j < vlen; j++)
                                outData.set(vlen * k + j, inData.get(vlen * i + j));
                            k += 1;
                        }
                    }
                    outIrregularField.addComponent(DataArray.create(outData, vlen, da.getName() + "_b"));                
                }
            } else {
                ComplexFloatLargeArray inCData = (ComplexFloatLargeArray)da.getRawArray();
                ComplexFloatLargeArray outCData = new ComplexFloatLargeArray(nBoundaryNodes * vlen);                    
                for (long i = 0; i < nNodes; i++) {
                    if (nodeIndices.getInt(i) >= 0) {
                        for (int j = 0; j < vlen; j++)
                            outCData.setComplexFloat(vlen * k + j, inCData.getComplexFloat(vlen * i + j));
                        k += 1;
                    }
                }
                outIrregularField.addComponent(DataArray.create(outCData, vlen, da.getName() + "_b"));
            }
        }
            

        outIrregularField.setCurrentCoords(new FloatLargeArray(bCoords));
        for (CellSet cellSet : inField.getCellSets()) {
            CellSet outCellSet = new CellSet(cellSet.getName() + "_boundary");
            for (int i = 0; i < cellSet.getBoundaryCellArrays().length; i++) {
                CellArray ca = cellSet.getBoundaryCellArray(CellType.getType(i));
                if (ca == null || ca.getType() == CellType.POINT)
                    continue;
                int[] nodes = ca.getNodes();
                int[] bNodes = new int[nodes.length];
                for (int j = 0; j < bNodes.length; j++)
                    bNodes[j] = nodeIndices.getInt(nodes[j]);
                CellArray bCa = new CellArray(ca.getType(), bNodes, ca.getOrientations(), null);
                outCellSet.setCellArray(bCa);
                outCellSet.setBoundaryCellArray(bCa);
            }
            outIrregularField.addCellSet(outCellSet);
        }
        setOutputValue("outField", new VNIrregularField(outIrregularField));
        outField = outIrregularField;
        prepareOutputGeometry();
        show();

    }
}
