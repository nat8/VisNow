/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
package pl.edu.icm.visnow.lib.basic.readers.ReadCSV;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.ArrayList;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.RegularField;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class CsvFieldReader
{

//    public static RegularField readCsvFloatField1D(String filePath, String fieldDelimiter, boolean headersLine)
//    {
//        return readCsvFloatField1D(filePath, fieldDelimiter, headersLine, 0, 0);
//    }
//
    public static RegularField readCsvFloatField1D(String filePath, String fieldDelimiter, boolean headersLine, int linesToSkip, int linesToSkipAtTheEnd)
    {
        return readCsvField1D(filePath, fieldDelimiter, headersLine, linesToSkip, linesToSkipAtTheEnd, false, false);
    }

    public static RegularField readCsvField1D(File file, String fieldDelimiter, boolean headersLine)
    {
        return readCsvField1D(file.getAbsolutePath(), fieldDelimiter, headersLine, 0, 0, true, false);
    }
//
//    public static RegularField readCsvField1D(String filePath, String fieldDelimiter, boolean headersLine, boolean doublePrecision)
//    {
//        return readCsvField1D(filePath, fieldDelimiter, headersLine, 0, 0, true, doublePrecision);
//    }
//    
//    public static RegularField readCsvField1D(File file, String fieldDelimiter, boolean headersLine, int linesToSkip, int linesToSkipAtTheEnd, boolean allowStrings, boolean doublePrecision)
//    {
//        return readCsvField1D(file.getAbsolutePath(), fieldDelimiter,  headersLine, linesToSkip, linesToSkipAtTheEnd, allowStrings, doublePrecision);
//    }
    /**
     * 
     * @param filepath not null filePath
     * @return 
     */
    public static RegularField readCsvField1D(String filepath, String fieldDelimiter, boolean headersLine, int linesToSkip, int linesToSkipAtTheEnd, boolean allowStrings, boolean doublePrecision)
    {
        if(filepath == null)
            return null;
        
        RegularField field = null;
        String modifiedFieldDelimiter = new String(fieldDelimiter);

        String line = null;
        int[] dims = new int[1];
        FileReader in;
        ArrayList<String> lines = new ArrayList<String>();

        try {
            in = new FileReader(filepath);
            LineNumberReader lr = new LineNumberReader(in);
            try {
                if (linesToSkip > 0) {
                    while (linesToSkip-- != 0) {
                        lr.readLine();
                    }
                }
                while ((line = lr.readLine()) != null) {
                    if (!line.isEmpty())
                        lines.add(line);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }

        int nLines = lines.size();
        if (headersLine) {
            nLines--;
        }
        nLines -= linesToSkipAtTheEnd;

        dims[0] = nLines;
        field = new RegularField(dims);

        line = lines.get(0);
        boolean quotesPresent = (line.startsWith("\"") && line.endsWith("\""));
        if (quotesPresent) {
            line = line.substring(1, line.length() - 1);
            modifiedFieldDelimiter = "\"" + fieldDelimiter + "\"";
        }
        String[] tmp = line.split(modifiedFieldDelimiter);
        int nFields = tmp.length;
        String[] headers = new String[nFields];
        ArrayList data = new ArrayList();
        ArrayList<Integer> dataVeclens = new ArrayList();

        if (headersLine) {
            //treat first line as headers
            for (int i = 0; i < nFields; i++) {
                headers[i] = tmp[i];
            }
            lines.remove(0);
        } else {
            for (int i = 0; i < nFields; i++) {
                headers[i] = "field" + i;
            }
        }

        double v;
        for (int l = 0; l < nLines; l++) {
            line = lines.get(l);
            if (l == 0) {
                quotesPresent = (line.startsWith("\"") && line.endsWith("\""));
                if (quotesPresent) {
                    modifiedFieldDelimiter = "\"" + fieldDelimiter + "\"";
                } else {
                    modifiedFieldDelimiter = "" + fieldDelimiter;
                }
            }
            if (quotesPresent) {
                line = line.substring(1, line.length() - 1);
            }

            tmp = line.split(modifiedFieldDelimiter);
            if(tmp.length != nFields) {
                //wrong number of fields
                if(quotesPresent) {
                    //means first or last field is empty - fix it
                    String[] ttmp = new String[tmp.length+1];
                    if(line.startsWith("\"")) {
                        ttmp[0] = "";
                        for (int i = 0; i < tmp.length; i++) {
                            ttmp[i+1] = tmp[i];                            
                        }                        
                    } else if(line.endsWith("\"")) {
                        for (int i = 0; i < tmp.length; i++) {
                            ttmp[i] = tmp[i];                            
                        }                        
                        ttmp[ttmp.length-1] = "";
                    }
                    tmp = ttmp;
                } else {
                    System.err.println("ERROR in line "+(headersLine?(l+1):l)+" - wrong number of fields");
                    return null;
                }
                
            }

            if (l == 0) {
                if (!allowStrings) {
                    for (int i = 0; i < nFields; i++) {
                        if(doublePrecision) {
                            double[] dData = new double[nLines];
                            data.add(dData);
                        } else {
                            float[] fData = new float[nLines];
                            data.add(fData);
                        }
                        dataVeclens.add(1);
                    }                    
                } else {
                    for (int i = 0; i < nFields; i++) {
                        if (tmp[i].startsWith("[") && tmp[i].endsWith("]")) {
                            //this is vector component
                            //TODO define in params vector delimiters and separator
                            tmp[i] = tmp[i].substring(1, tmp[i].length() - 1);
                            String[] tmp2 = tmp[i].split(fieldDelimiter);
                            int veclen = tmp2.length;
                            
                            if(doublePrecision) {
                                boolean alldoubles = false;
                                try {
                                    for (int j = 0; j < tmp2.length; j++) {
                                        Double.parseDouble(tmp2[j]);
                                    }
                                    alldoubles = true;
                                } catch (NumberFormatException ex) {
                                    alldoubles = false;
                                }

                                if (alldoubles) {
                                    double[] dData = new double[nLines * veclen];
                                    data.add(dData);                                
                                } else {
                                    String[] sData = new String[nLines * veclen];
                                    data.add(sData);
                                }
                            } else {
                                boolean allfloats = false;
                                try {
                                    for (int j = 0; j < tmp2.length; j++) {
                                        Float.parseFloat(tmp2[j]);
                                    }
                                    allfloats = true;
                                } catch (NumberFormatException ex) {
                                    allfloats = false;
                                }

                                if (allfloats) {
                                    float[] fData = new float[nLines * veclen];
                                    data.add(fData);                                
                                } else {
                                    String[] sData = new String[nLines * veclen];
                                    data.add(sData);
                                }
                            }
                            dataVeclens.add(veclen);
                        } else {
                            //this is scalar component
                            try {
                                if(doublePrecision) {
                                    v = Double.parseDouble(tmp[i]);
                                    double[] dData = new double[nLines];
                                    data.add(dData);
                                } else {
                                    v = Float.parseFloat(tmp[i]);
                                    float[] fData = new float[nLines];
                                    data.add(fData);
                                }
                            } catch (NumberFormatException ex) {
                                String[] sData = new String[nLines];
                                data.add(sData);
                            }
                            dataVeclens.add(1);
                        }
                    }
                }
            }

            int vlen;
            for (int i = 0; i < nFields; i++) {
                if (data.get(i) instanceof String[]) {
                    String[] sData = ((String[]) data.get(i));
                    vlen = dataVeclens.get(i);
                    if (vlen == 1) {
                        sData[l] = tmp[i];
                    } else {
                        if (tmp[i].startsWith("[") && tmp[i].endsWith("]"))
                            tmp[i] = tmp[i].substring(1, tmp[i].length() - 1);
                        String[] tmp2 = tmp[i].split(fieldDelimiter);                        
                        for (int j = 0; j < tmp2.length; j++) {
                            sData[l*vlen + j] = tmp2[j];
                        }
                    }
                } else if (data.get(i) instanceof double[]) {
                    double[] dData = ((double[]) data.get(i));
                    vlen = dataVeclens.get(i);
                    if (vlen == 1) {
                        try {
                            v = Double.parseDouble(tmp[i]);
                        } catch (NumberFormatException ex) {
                            v = 0.0;
                        }
                        dData[l] = v;
                    } else {
                        if (tmp[i].startsWith("[") && tmp[i].endsWith("]"))
                            tmp[i] = tmp[i].substring(1, tmp[i].length() - 1);
                        String[] tmp2 = tmp[i].split(fieldDelimiter);
                        for (int j = 0; j < tmp2.length; j++) {
                            try {
                                v = Double.parseDouble(tmp2[j]);
                            } catch (NumberFormatException ex) {
                                v = 0.0;
                            }
                            dData[l*vlen+j] = v;
                        }
                    }
                } else {
                    float[] fData = ((float[]) data.get(i));
                    vlen = dataVeclens.get(i);
                    if (vlen == 1) {
                        try {
                            v = Float.parseFloat(tmp[i]);
                        } catch (NumberFormatException ex) {
                            v = 0.0f;
                        }
                        fData[l] = (float) v;
                    } else {
                        if (tmp[i].startsWith("[") && tmp[i].endsWith("]"))
                            tmp[i] = tmp[i].substring(1, tmp[i].length() - 1);
                        String[] tmp2 = tmp[i].split(fieldDelimiter);
                        for (int j = 0; j < tmp2.length; j++) {
                            try {
                                v = Float.parseFloat(tmp2[j]);
                            } catch (NumberFormatException ex) {
                                v = 0.0f;
                            }
                            fData[l*vlen+j] = (float) v;
                        }
                    }
                }
            }
        }

        for (int i = 0; i < nFields; i++) {
            field.addComponent(DataArray.create(data.get(i), dataVeclens.get(i), headers[i]));
        }
        return field;
    }

    public static RegularField readCsvQuotedFloatField1D(String filePath, String quoteString, char comma, boolean headersLine)
    {
        if (filePath == null) {
            System.out.println("[readCsvQuotedFloatField1D] filePath is null");
            return null;
        }

        RegularField field = null;

        String line = null;
        int[] dims = new int[1];
        FileReader in;
        ArrayList<String> lines = new ArrayList<String>();

        try {
            in = new FileReader(filePath);
            LineNumberReader lr = new LineNumberReader(in);
            try {
                while ((line = lr.readLine()) != null) {
                    lines.add(line);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                return null;
            }
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
            return null;
        }

        int nLines = lines.size();
        if (headersLine) {
            nLines--;
        }

        dims[0] = nLines;
        field = new RegularField(dims);

        String[] tmp = Decode(lines.get(0), quoteString);
        int nFields = tmp.length;
        String[] headers = new String[nFields];
        float[][] data = new float[nFields][nLines];

        if (headersLine) {
            //treat first line as headers
            for (int i = 0; i < nFields; i++) {
                headers[i] = tmp[i];
            }
        } else {
            for (int i = 0; i < nFields; i++) {
                headers[i] = "field" + i;
            }
        }

        float v;
        for (int l = 0; l < nLines; l++) {
            if (headersLine) {
                tmp = Decode(lines.get(l + 1), quoteString);
            } else {
                tmp = Decode(lines.get(l), quoteString);
            }

            for (int i = 0; i < nFields; i++) {
                try {
                    String dotSeparatedLine = tmp[i].replace(comma, '.');
                    v = Float.parseFloat(dotSeparatedLine);

                } catch (NumberFormatException ex) {
                    v = 0.0f;
                }
                data[i][l] = v;
            }
        }

        for (int i = 0; i < nFields; i++) {
            field.addComponent(DataArray.create(data[i], 1, headers[i]));
        }
        return field;
    }

    private static String[] Decode(String line, String quoteString)
    {
        String[] tmp = line.split(quoteString);

        ArrayList<String> ret = new ArrayList<String>();

        int i = 1;
        do {
            ret.add(tmp[i]);
            i += 2;
        } while (i < tmp.length);

        String[] retA = new String[ret.size()];
        ret.toArray(retA);

        return retA;
    }

    private CsvFieldReader()
    {
    }
}
