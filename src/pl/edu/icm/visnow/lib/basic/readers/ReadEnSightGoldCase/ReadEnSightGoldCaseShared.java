package pl.edu.icm.visnow.lib.basic.readers.ReadEnSightGoldCase;

import java.nio.ByteOrder;
import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author Norbert_2
 */
public class ReadEnSightGoldCaseShared {

    static final ParameterName<String> FILENAME = new ParameterName("file name");
    static final ParameterName<Boolean> MATERIALS_AS_SETS = new ParameterName("materials as sets");
    static final ParameterName<Boolean> CELL_TO_NODE = new ParameterName("Cell to node");
    static final ParameterName<Boolean> MERGE_CELL_SETS = new ParameterName("Merge cell sets");
    static final ParameterName<Boolean> DROP_CELL_DATA = new ParameterName("Drop cell data");
    static final ParameterName<Boolean> DROP_CONSTANT_DATA = new ParameterName("Drop constant data");
    static final ParameterName<Boolean> SHOW = new ParameterName("show");
    static final ParameterName<Integer> INPUT_SOURCE = new ParameterName("input source");
}
