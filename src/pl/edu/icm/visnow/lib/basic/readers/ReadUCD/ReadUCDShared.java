package pl.edu.icm.visnow.lib.basic.readers.ReadUCD;

import pl.edu.icm.visnow.engine.core.ParameterName;

/**
 *
 * @author Norbert_2
 */


public class ReadUCDShared {
    
    static final ParameterName<String> FILENAME = new ParameterName("file name");
    static final ParameterName<Boolean> MATERIAL_AS_SETS = new ParameterName("material as sets");
    static final ParameterName<Boolean> INDICES = new ParameterName("indices");
    static final ParameterName<Integer> INPUT_SOURCE = new ParameterName("input source");
    static final ParameterName<Boolean> CELL_TO_NODE = new ParameterName("Cell to node");
    static final ParameterName<Boolean> MERGE_CELL_SETS = new ParameterName("Merge cell sets");
    static final ParameterName<Boolean> DROP_CELL_DATA = new ParameterName("Drop cell data");
    
}

