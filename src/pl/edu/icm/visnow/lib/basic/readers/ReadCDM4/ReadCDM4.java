//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2014 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.readers.ReadCDM4;

import java.io.IOException;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.engine.core.InputEgg;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import org.apache.log4j.Logger;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.lib.types.VNRegularField;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.ImmutablePair;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.jscic.utils.UnitUtils;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import static pl.edu.icm.visnow.lib.basic.readers.ReadCDM4.ReadCDM4Shared.*;
import pl.edu.icm.visnow.system.main.VisNow;
import pl.edu.icm.visnow.system.utils.usermessage.Level;
import ucar.ma2.Array;
import ucar.nc2.Variable;
import ucar.nc2.NetcdfFile;
import ucar.ma2.DataType;
import ucar.ma2.IndexIterator;
import ucar.nc2.Dimension;
import ucar.units.Unit;

/**
 * Module providing an interface to NetCDF/HDF data files.
 * Or, more precisely, files compatible with the Common Data Model version 4
 * (abbreviated CDM4 here). The package is, more or less, an interface to
 * NetCDF-Java library, website
 * {@link http://www.unidata.ucar.edu/software/thredds/current/netcdf-java}.
 *
 * @author Szymon Jaranowski (s.jaranowski@icm.edu.pl), Warsaw University, ICM
 * @author Piotr Wendykier (p.wendykier@uksw.edu.pl)
 */
public class ReadCDM4 extends OutFieldVisualizationModule
{

    private static final Logger LOGGER = Logger.getLogger(ReadCDM4.class);

    public static InputEgg[] inputEggs = null;
    public static OutputEgg[] outputEggs = null;
    private GUI computeUI;

    public ReadCDM4()
    {

        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    /**
     * The method extracts the name of variable from the String containing the
     * variable name along with dimensions.
     * The principle of operation is based on the fact, that NetCDF variable names
     * can't consist of "(" character; see:
     * {@link http://www.unidata.ucar.edu/software/netcdf/docs/netcdf/CDL-Syntax.html}.
     * <p>
     * Citation (as of 20th May 2014), for the record:
     * <i>In CDL, just as for netCDF, the names of dimensions, variables and attributes
     * (and, in netCDF-4 files, groups, user-defined types, compound member names, and
     * enumeration symbols) consist of arbitrary sequences of alphanumeric characters,
     * underscore '_', period '.', plus '+', hyphen '-', or at sign '@', but beginning
     * with a letter or underscore.</i>
     *
     * @param variableName Name of variable (escaped or unescaped, doesn't matter),
     *                     with dimensions.
     *
     * @return The name of variable, without any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    private static String stripDimension(String variableName)
    {
        return variableName.split("\\(")[0];
    }

    private static int[] extractDims(List<Dimension> list, boolean firstDimensionIsTime)
    {
        List<Integer> dims_list = new ArrayList<>();

        for (int i = firstDimensionIsTime ? 1 : 0; i < list.size(); i++) {
            Dimension dim = list.get(i);
            int length = dim.getLength();
            if (length > 1) {
                dims_list.add(length);
            }
        }
        int len = dims_list.size();
        if (len < 1 || len > 3) {
            return null; //Only 1D, 2D and 3D regular fields are supported
        }

        int[] dims = new int[len];
        for (int i = 0; i < len; ++i) {
            dims[i] = dims_list.get(i);
        }

        /* Swap the dimensions to conform to VisNow way of storing data. */
        if (len > 1) {
            int tmp = dims[0];
            dims[0] = dims[len - 1];
            dims[len - 1] = tmp;
        }
        return dims;
    }

    static int[] getVariableShape(String filename, String variableName)
    {
        if (filename == null) {
            return null;
        }

        int[] shape;

        try {
            NetcdfFile dataFile;
            dataFile = NetcdfFile.open(filename, null);

            Variable variable = dataFile.findVariable(variableName);
            if (variable == null) {
                LOGGER.error("Could not find the variable \"" + variableName + "\".");
                VisNow.get().userMessageSend(null, "Error reading file", "Could not find the variable \"" + variableName + "\".", Level.ERROR);
                return null;
            }
            shape = variable.getShape();
            dataFile.close();
        } catch (IOException e) {
            LOGGER.error("Could not read file " + filename);
            VisNow.get().userMessageSend(null, "Error reading file", "File " + filename + " is not supported.", Level.ERROR);
            return null;
        }

        return shape;
    }

    /**
     * The method gives the names of variables stored in given file.
     * These names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output
     * array may look like this: <tt>MyGroup/MyDatasetVersion1\.2</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of escaped names, without
     *         any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNames(String filename)
    {
        String[] out = getVariablesNamesAndDimensions(filename);
        if (out == null) {
            return null;
        }

        for (int i = 0; i < out.length; ++i) {
            out[i] = stripDimension(out[i]);
        }

        return out;
    }

    /**
     * The method gives the names of variables of certain dimension (given shape),
     * stored in given file.
     * These names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output
     * array may look like this: <tt>MyGroup/MyDatasetVersion1\.2</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of escaped names, without
     *         any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNames(String filename, int[] shape)
    {
        String[] out = getVariablesNamesAndDimensions(filename, shape);
        if (out == null) {
            return null;
        }

        for (int i = 0; out.length >= i; ++i) {
            out[i] = stripDimension(out[i]);
        }

        return out;
    }

    /**
     * The method gives the unescaped names of variables stored in given file.
     * Unescaped names are stripped from escape characters (which are usually
     * needed, when identifying variable by name - used by most methods), hence
     * more human-readable. Example element of the output array may look like this:
     * <tt>MyGroup/MyDatasetVersion1.2</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of unescaped names, without
     *         any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNamesUnescaped(String filename)
    {
        String[] out = getVariablesNamesAndDimensionsUnescaped(filename);
        if (out == null) {
            return null;
        }

        for (int i = 0; out.length >= i; ++i) {
            out[i] = stripDimension(out[i]);
        }

        return out;
    }

    /**
     * The method gives the unescaped names of variables of given dimension (certain
     * shape), stored in given file.
     * Unescaped names are stripped from escape characters (which are usually
     * needed, when identifying variable by name - used by most methods), hence
     * more human-readable. Example element of the output array may look like this:
     * <tt>MyGroup/MyDatasetVersion1.2</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of unescaped names, without
     *         any dimension information.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNamesUnescaped(String filename, int[] shape)
    {
        String[] out = getVariablesNamesAndDimensionsUnescaped(filename, shape);
        if (out == null) {
            return null;
        }

        for (int i = 0; out.length >= i; ++i) {
            out[i] = stripDimension(out[i]);
        }

        return out;
    }

    /**
     * The method gives the names and dimensions of variables stored in given file.
     * The names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output array
     * may look like this: <tt>MyGroup/MyDatasetVersion1\.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of escaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNamesAndDimensions(String filename)
    {
        return getVariablesNamesAndDimensions(filename, null);
    }

    /**
     * The method gives the names and dimensions of variables of certain dimension
     * (given shape), stored in given file.
     * The names are escaped, ie. with escape character "\" before some of the
     * special characters, like "." (dot). Most methods assume variable names
     * given to them as arguments are escaped. Example element of the output array
     * may look like this: <tt>MyGroup/MyDatasetVersion1\.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of escaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNamesAndDimensions(String filename, int[] shape)
    {
        if (filename == null) {
            return null;
        }
        try {
            NetcdfFile dataFile;
            dataFile = NetcdfFile.open(filename, null);

            ArrayList<String> variablesNamesAndDimensions = new ArrayList<>();
            List<Variable> variablesList = dataFile.getVariables();

            if (shape != null) {
                for (Variable variable : variablesList) {
                    if (Arrays.equals(variable.getShape(), shape)) {
                        variablesNamesAndDimensions.add(variable.getNameAndDimensions());
                    }
                }
            } else { // FilterVariables
                for (Variable variable : variablesList) {
                    if (variable.getShape().length == 0) {
                        /* Meta-data, probably...? */
                        LOGGER.warn("Could not determine the shape of variable \"" + variable.getNameAndDimensions() + "\", seems an empty array.");
                        VisNow.get().userMessageSend(null, "Empty array.", "Could not determine the shape of variable \"" + variable.getNameAndDimensions() + "\", seems an empty array.", Level.WARNING);
                        continue;
                    }
                    variablesNamesAndDimensions.add(variable.getNameAndDimensions());
                }
            }
            dataFile.close();
            return variablesNamesAndDimensions.toArray(new String[variablesNamesAndDimensions.size()]);
        } catch (IOException e) {
            LOGGER.error("Could not read file " + filename);
            VisNow.get().userMessageSend(null, "Error reading file", "File " + filename + " is not supported.", Level.ERROR);
            return null;
        }
    }

    /**
     * The method gives unescaped names and dimensions of variables stored in given file.
     * Unescaped names are stripped from escape characters (which are usually
     * needed, when identifying variable by name - used by most methods), hence
     * more human-readable. Example element of the output array may look like this:
     * <tt>MyGroup/MyDatasetVersion1.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     *
     * @return The output array of strings consist of unescaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNamesAndDimensionsUnescaped(String filename)
    {
        String[] out = getVariablesNamesAndDimensions(filename);
        if (out == null) {
            return null;
        }

        for (int i = 0; i < out.length; ++i) {
            out[i] = NetcdfFile.makeNameUnescaped(out[i]);
        }

        return out;
    }

    /**
     * The method gives unescaped names and dimensions of variables of certain
     * dimension (given shape), stored in given file.
     * Unescaped names are stripped from escape characters (which are usually
     * needed, when identifying variable by name - used by most methods), hence
     * more human-readable. Example element of the output array may look like this:
     * <tt>MyGroup/MyDatasetVersion1.2(Nx=100, Ny=100, Nz=20)</tt>.
     *
     * @param filename Name of CDM4 file (with system path).
     * @param shape    Required dimension (shape) of arrays stored in variables,
     *                 eg. <tt>new int[]{100, 100, 20}</tt>.
     *
     * @return The output array of strings consist of unescaped names and dimensions
     *         of variables in file.
     *
     * @see getVariablesNames
     * @see getVariablesNamesUnescaped
     * @see getVariablesNamesAndDimensions
     * @see getVariablesNamesAndDimensionsUnescaped
     */
    static String[] getVariablesNamesAndDimensionsUnescaped(String filename, int[] shape)
    {
        String[] out = getVariablesNamesAndDimensions(filename, shape);
        if (out == null) {
            return null;
        }

        for (int i = 0; i < out.length; ++i) {
            out[i] = NetcdfFile.makeNameUnescaped(out[i]);
        }

        return out;
    }

    private static LargeArrayType ucarDataTypeToLargeArrayType(DataType in)
    {
        switch (in) {
            case BOOLEAN:
                return LargeArrayType.LOGIC;
            case BYTE:
            case SHORT:
            case OPAQUE:
            case ENUM2:
                return LargeArrayType.SHORT;
            case INT:
            case USHORT:
            case ENUM4:
                return LargeArrayType.INT;
            case LONG:
            case UINT:
                return LargeArrayType.LONG;
            case FLOAT:
                return LargeArrayType.FLOAT;
            case UBYTE:
            case CHAR:
            case ENUM1:
                return LargeArrayType.UNSIGNED_BYTE;
            default:
                return LargeArrayType.DOUBLE;
        }
    }

    private static ArrayList<LargeArray> ucarArrayToLargeArrays(Array ucarArray, int[] dims, boolean isTimeDependent, ProgressAgent progressAgent, float currentProgress, float progressStep)
    {
        LargeArrayType arrayType = ucarDataTypeToLargeArrayType(ucarArray.getDataType());
        long nelements = 1;
        int ntimesteps = 1;
        for (int i = 0; i < dims.length; i++) {
            nelements *= (long) dims[i];
        }
        if (isTimeDependent) {
            ntimesteps = (int) (ucarArray.getSize() / nelements);
        }
        ArrayList<LargeArray> list = new ArrayList<>(ntimesteps);
        IndexIterator iter = ucarArray.getIndexIterator();

        for (int t = 0; t < ntimesteps; t++) {
            LargeArray la = LargeArrayUtils.create(arrayType, nelements);
            switch (la.getType()) {
                case LOGIC:
                    for (long i = 0; i < nelements; i++) {
                        la.setBoolean(i, iter.getBooleanNext());
                    }
                    break;
                case UNSIGNED_BYTE:
                    for (long i = 0; i < nelements; i++) {
                        la.setByte(i, iter.getByteNext());
                    }
                    break;
                case SHORT:
                    for (long i = 0; i < nelements; i++) {
                        la.setShort(i, iter.getShortNext());
                    }
                    break;
                case INT:
                    for (long i = 0; i < nelements; i++) {
                        la.setInt(i, iter.getIntNext());
                    }
                    break;
                case LONG:
                    for (long i = 0; i < nelements; i++) {
                        la.setLong(i, iter.getLongNext());
                    }
                    break;
                case FLOAT:
                    for (long i = 0; i < nelements; i++) {
                        la.setFloat(i, iter.getFloatNext());
                    }
                    break;
                default:
                    for (long i = 0; i < nelements; i++) {
                        la.setDouble(i, iter.getDoubleNext());
                    }
            }
            if (progressAgent != null) {
                progressAgent.setProgress(currentProgress + ((t + 1) / (double) ntimesteps) * progressStep);
            }
            list.add(la);
        }
        return list;
    }

    /**
     * Reads data from a file and stores them in a Field.
     *
     * @param filename       Name of CDM4 file (with system path)
     * @param variablesNames list of variable names
     * @param progressAgent  progress monitor
     *
     * @return a new Field
     */
    public static Field createField(String filename, String[] variablesNames, boolean firstDimensionIsTime, ProgressAgent progressAgent)
    {
        if (filename == null) {
            return null;
        }
        try {
            /* Code copying from "createArray", "createDataArray" and "getVariableShape" methods;
             * not nice, but prevents re-opening of the file. */
            NetcdfFile dataFile;
            dataFile = NetcdfFile.open(filename, null);

            /* Only regular fields are supported at this time (according to NetCDF Java API ver. 4.3.20). */
            int[] dims = null;
            RegularField field = null;
            float progressStep = 0.9f / variablesNames.length;

            for (int i = 0; i < variablesNames.length; ++i) {

                Variable variable = dataFile.findVariable(variablesNames[i]);

                if (variable == null) {
                    LOGGER.error("Could not find variable \"" + variablesNames[i] + "\".");
                    VisNow.get().userMessageSend(null, "Error reading file", "Could not find variable \"" + variablesNames[i] + "\".", Level.ERROR);
                    return null;
                }

                List<Dimension> dimensions = variable.getDimensions();
                if (dims == null) {
                    dims = extractDims(dimensions, firstDimensionIsTime);
                    if (dims == null) {
                        LOGGER.error("Only 1D, 2D and 3D variables of length greater than 1 are supported.");
                        VisNow.get().userMessageSend(null, "Error reading file", "Only 1D, 2D and 3D variables of length greater than 1 are supported.", Level.ERROR);
                        return null;
                    }
                    field = new RegularField(dims);
                }

                Array ucarArray = variable.read();
                if (ucarArray == null) {
                    LOGGER.error("Could not read array \"" + variablesNames[i] + "\".");
                    VisNow.get().userMessageSend(null, "Error reading file", "Could not read array \"" + variablesNames[i] + "\".", Level.ERROR);
                    return null;
                }
                ArrayList<LargeArray> la_list = ucarArrayToLargeArrays(ucarArray, dims, firstDimensionIsTime, progressAgent, i * progressStep, progressStep);
                if (la_list == null || la_list.isEmpty()) {
                    LOGGER.error("Could not convert array \"" + variablesNames[i] + "\".");
                    VisNow.get().userMessageSend(null, "Error reading file", "Could not convert array \"" + variablesNames[i] + "\".", Level.ERROR);
                    return null;
                }
                Unit u;
                String unit = variable.getUnitsString();
                try {
                    u = UnitUtils.getUnit(unit);
                    unit = u.toString();
                } catch (Exception e) {
                    unit = "1";
                }
                if (firstDimensionIsTime) {
                    ArrayList<Float> timeSeries = new ArrayList<>(la_list.size());
                    for (int j = 0; j < la_list.size(); j++) {
                        timeSeries.add((float) j);
                    }
                    TimeData td = new TimeData(timeSeries, la_list, 0);
                    field.addComponent(DataArray.create(td, 1, NetcdfFile.makeNameUnescaped(variablesNames[i]), unit, null));
                } else {
                    field.addComponent(DataArray.create(la_list.get(0), 1, NetcdfFile.makeNameUnescaped(variablesNames[i]), unit, null));
                }
            }

            if (dataFile != null) {
                dataFile.close();
            }
            return field;
        } catch (IOException e) {
            LOGGER.error("Could not read file " + filename);
            VisNow.get().userMessageSend(null, "Error reading file", "File " + filename + " is not supported.", Level.ERROR);
            return null;
        }
    }

    /**
     * Reads data from a file and stores them in a Field.
     *
     * @param params        parameters
     * @param progressAgent progress monitor
     *
     * @return a new Field
     */
    public static Field createField(Parameters params, ProgressAgent progressAgent)
    {
        String filename = params.get(FILENAME);
        String[] variablesNames = params.get(VARIABLE_NAMES);
        Boolean firstDimensionIsTime = params.get(FIRST_DIMENSION_IS_TIME);

        return createField(filename, variablesNames, firstDimensionIsTime, progressAgent);
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return ReadCDM4Shared.getDefaultParameters();
    }

    @Override
    protected void notifySwingGUIs(final pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        ProgressAgent progressAgent = getProgressAgent(100);
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);

        progressAgent.setProgress(0.0f);

        if (p.get(FILENAME).isEmpty()) {
            outField = null;
            progressAgent.setProgress(1.0);
            show();
            setOutputValue("outCDM4Datasets", null);
            return;
        } else {
            outField = createField(p, progressAgent);
            if (outField != null) {
                setOutputValue("outCDM4Datasets", new VNRegularField((RegularField) outField));
            } else {
                setOutputValue("outCDM4Datasets", null);
            }
            progressAgent.setProgress(0.9);
            prepareOutputGeometry();
            progressAgent.setProgress(1.0);
            show();
        }
    }

    @Override
    public void onInitFinishedLocal()
    {
        if (isForceFlag()) {
            computeUI.activateOpenDialog();
        }
    }
}
