//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField;

import java.io.LineNumberReader;
import java.util.Scanner;
import javax.imageio.stream.ImageInputStream;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.StringDataArray;
import pl.edu.icm.jscic.utils.TileUtils;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.BooleanArrayIOSchema;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.DataElementIOSchema;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.DataFileSchema;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.FieldIOSchema;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.FileSectionSchema;
import pl.edu.icm.visnow.lib.basic.readers.ReadVisNowField.utils.IntArrayIOSchema;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */
public class FileSectionReader
{

    protected Field outField = null;
    protected RegularField regularField = null;
    protected FileSectionSchema sectionSchema = null;
    protected int fileType;
    protected float time = 0;
    protected LineNumberReader reader = null;
    protected ImageInputStream inStream = null;
    protected Scanner scanner = null;
    protected SectionModel model = null;
    protected int nComps;
    protected long nData;
    protected LogicLargeArray[] boolArrs;
    protected UnsignedByteLargeArray[] byteArrs;
    protected ShortLargeArray[] shortArrs;
    protected IntLargeArray[] intArrs;
    protected FloatLargeArray[] floatArrs;
    protected DoubleLargeArray[] dblArrs;
    protected StringLargeArray[] strArrs;
    protected int[][] tileBds;
    protected String filePath;
    protected boolean streamRead = false;
    protected boolean continuingRead = false;
    protected String decimalSeparator = null;
    protected String entrySeparator = null;

    public FileSectionReader(Field outField, FieldIOSchema schema,
                             LineNumberReader reader, ImageInputStream inStream, Scanner scanner, int fileType, String filePath)
    {
        this.outField = outField;
        if (outField instanceof RegularField)
            regularField = (RegularField) outField;
        this.reader = reader;
        this.inStream = inStream;
        this.scanner = scanner;
        this.fileType = fileType;
        this.filePath = filePath;
    }

    public void setSectionSchema(FileSectionSchema sectionSchema, float time, String decimalSeparator, String entrySeparator)
    {
        this.sectionSchema = sectionSchema;
        model = new SectionModel(sectionSchema, outField, time);
        nComps = model.nItems;
        nData = model.nData;
        boolArrs = model.boolArrs;
        byteArrs = model.byteArrs;
        shortArrs = model.shortArrs;
        intArrs = model.intArrs;
        floatArrs = model.floatArrs;
        dblArrs = model.dblArrs;
        strArrs = model.strArrs;
        tileBds = sectionSchema.getTile();
        this.time = time;
        if (decimalSeparator != null) {
            String tmp = decimalSeparator.replaceAll("\"", "");
            if (tmp.equals("."))
                this.decimalSeparator = null;
            else
                this.decimalSeparator = tmp;
        }
        this.entrySeparator = entrySeparator;
    }

    protected void outputError(String text, String fname, int lineNumber, Exception e)
    {
        System.err.println("ERROR: " + text + "; in function " + fname + " line " + lineNumber);
        //e.printStackTrace();
    }

    public void setFilePath(String filePath)
    {
        this.filePath = filePath;
    }

    public void setContinuingRead(boolean continuingRead)
    {
        this.continuingRead = continuingRead;
    }

    public void setTime(float time)
    {
        this.time = time;
    }

    public void setStreamRead(boolean streamRead)
    {
        this.streamRead = streamRead;
    }

    public int readSection()
    {
        switch (fileType) {
            case DataFileSchema.BIG_ENDIAN:
            case DataFileSchema.LITTLE_ENDIAN:
                ReadBinarySectionData.readSectionData(model, inStream, filePath);
                break;
            case DataFileSchema.ASCII:
                ReadASCIIContinuousSectionData.readSectionData(model, scanner, filePath, decimalSeparator, entrySeparator);
                break;
            case DataFileSchema.COLUMN:
                ReadASCIIColumnSectionData.readSectionData(model, reader, filePath, decimalSeparator, entrySeparator);
                break;
            case DataFileSchema.FIXED_COLUMN:
                ReadASCIIFixedColumnSectionData.readSectionData(model, reader, filePath, decimalSeparator);
        }
        if (sectionSchema.getComponents().isEmpty())
            return 0;
        
        if (tileBds != null && regularField != null) {
            for (int iComp = 0; iComp < model.nItems; iComp++) {
                int cmp = model.comps[iComp];
                int coord = model.coords[iComp];
                if (cmp < regularField.getNComponents()) {
                    DataArray ar = regularField.getComponent(cmp);
                    switch (ar.getType()) {
                        case FIELD_DATA_BYTE:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), byteArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_SHORT:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), shortArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_INT:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), intArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_FLOAT:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), floatArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        case FIELD_DATA_DOUBLE:
                            TileUtils.putTile(tileBds, regularField.getDims(),
                                         ar.produceData(time), dblArrs[iComp],
                                         ar.getVectorLength(), coord);
                            break;
                        default:
                            break;
                    }
                } else if (cmp == regularField.getNComponents()) {
                    TileUtils.putTile(tileBds, regularField.getDims(),
                                 regularField.produceCoords(time), floatArrs[iComp],
                                 3, coord);
                } else if (cmp == regularField.getNComponents() + 1) {
                    TileUtils.putTile(tileBds, regularField.getDims(),
                                 regularField.produceMask(time), byteArrs[iComp],
                                 1, coord);
                }
            }
        } else {
            for (int iComp = 0; iComp < model.nItems; iComp++) {
                DataElementIOSchema sch = model.schemas[iComp];
                DataContainer container = sch.getDataset();
                int vlen = sch.getVectorLength();
                int cmp = model.comps[iComp];
                int coord = model.coords[iComp];
                if (coord >= 0) {
                    if (sch instanceof IntArrayIOSchema) {
                        IntLargeArray iTarget = ((IntArrayIOSchema) sch).getIntArray();
                        IntLargeArray iSource = intArrs[iComp];
                        if (iSource != iTarget)
                            for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                iTarget.setInt(j, iSource.getInt(i));
                    } else if (sch instanceof BooleanArrayIOSchema) {
                        LogicLargeArray blSource = boolArrs[iComp];
                        LogicLargeArray blTarget = ((BooleanArrayIOSchema) sch).getBoolArray();
                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                            blTarget.setByte(j, blSource.getByte(i));
                    } else if (cmp < container.getNComponents()) {
                        DataArray ar = container.getComponent(cmp);
                        switch (ar.getType()) {
                            case FIELD_DATA_LOGIC:
                                LogicLargeArray blTarget = (LogicLargeArray) (ar.produceData(time));
                                UnsignedByteLargeArray blSource = byteArrs[iComp];
                                for (int i = 0, j = coord; i < nData; i++, j += vlen)
                                    blTarget.setByte(j, blSource.getByte(i));
                                break;
                            case FIELD_DATA_BYTE:
                                UnsignedByteLargeArray bTarget = ((UnsignedByteLargeArray) ar.produceData(time));
                                UnsignedByteLargeArray bSource = byteArrs[iComp];
                                if (bSource != bTarget) {
                                    if (bTarget.length() == bSource.length())
                                        LargeArrayUtils.arraycopy(bSource, 0, bTarget, 0, bSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            bTarget.setByte(j, bSource.getByte(i));
                                }
                                break;
                            case FIELD_DATA_SHORT:
                                ShortLargeArray sTarget = ((ShortLargeArray) ar.produceData(time));
                                ShortLargeArray sSource = shortArrs[iComp];
                                if (sSource != sTarget) {
                                    if (sTarget.length() == sSource.length())
                                        LargeArrayUtils.arraycopy(sSource, 0, sTarget, 0, sSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            sTarget.setShort(j, sSource.getShort(i));
                                }
                                break;
                            case FIELD_DATA_INT:
                                IntLargeArray iTarget = ((IntLargeArray) ar.produceData(time));
                                IntLargeArray iSource = intArrs[iComp];
                                if (iSource != iTarget) {
                                    if (iTarget.length() == iSource.length())
                                        LargeArrayUtils.arraycopy(iSource, 0, iTarget, 0, iSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            iTarget.setInt(j, iSource.getInt(i));
                                }
                                break;
                            case FIELD_DATA_FLOAT:
                                FloatLargeArray fTarget = ((FloatLargeArray) ar.produceData(time));
                                FloatLargeArray fSource = floatArrs[iComp];
                                if (fSource != fTarget) {
                                    if (fTarget.length() == fSource.length())
                                        LargeArrayUtils.arraycopy(fSource, 0, fTarget, 0, fSource.length());
                                    else
                                    for (long i = coord, j = 0; j < fSource.length(); i += vlen, j++)
                                        fTarget.setFloat(i, fSource.getFloat(j));
                                }
                                break;
                            case FIELD_DATA_DOUBLE:
                                DoubleLargeArray dTarget = ((DoubleLargeArray) ar.produceData(time));
                                DoubleLargeArray dSource = dblArrs[iComp];
                                if (dSource != dTarget) {
                                    if (dTarget.length() == dSource.length())
                                        LargeArrayUtils.arraycopy(dSource, 0, dTarget, 0, dSource.length());
                                    else
                                    for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                        dTarget.setDouble(j, dSource.getDouble(i));
                                }
                                break;
                            case FIELD_DATA_STRING:
                                StringLargeArray stTarget = ((StringDataArray) ar).produceData(time);
                                StringLargeArray stSource = strArrs[iComp];
                                if (stSource != stTarget) {
                                    if (stTarget.length() == stSource.length())
                                        LargeArrayUtils.arraycopy(stSource, 0, stTarget, 0, stSource.length());
                                    else
                                        for (long i = 0, j = coord; i < nData; i++, j += vlen)
                                            stTarget.set(j, stSource.get(i));
                                }
                                break;
                            default:
                                break;
                        }
                    } else if (cmp == container.getNComponents()) {
                        FloatLargeArray fTarget = outField.produceCoords(time);
                        FloatLargeArray fSource = floatArrs[iComp];
                        if (fTarget != fSource)
                        {
                            if (fTarget.length() == fSource.length())
                                LargeArrayUtils.arraycopy(fSource, 0, fTarget, 0, fSource.length());
                            else
                            for (long i = coord, j = 0; j < fSource.length(); i += vlen, j++)
                                fTarget.setFloat(i, fSource.getFloat(j));
                        }
                    } else if (cmp == container.getNComponents() + 1) {
                        LogicLargeArray mask = outField.produceMask(time);
                        for (long i = 0; i < boolArrs[iComp].length(); i++) 
                            mask.setByte(i, boolArrs[iComp].getByte(i));
                    }
                }
                //            else if (cmp == outField.getNData() + 1)
                //            {
                //               System.arraycopy(boolArrs[iComp], 0, outField.produceMask(time), 0, boolArrs[iComp].length);
                //            } else if (cmp < outField.getNData() && outField.getData(cmp).getType() == DataArray.FIELD_DATA_BOOLEAN)
                //            {
                //               boolean[] blTarget = ((BooleanDataArray)outField.getData(cmp)).produceBoolData(time);
                //               System.arraycopy(boolArrs[iComp], 0, blTarget, 0, boolArrs[iComp].length);
                //            }

            }
        }
        return 0;
    }
}
