//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.testdata.TestPoints;

import static org.apache.commons.math3.util.FastMath.*;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.visnow.engine.core.OutputEgg;
import pl.edu.icm.visnow.engine.core.Parameter;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.engine.core.Parameters;
import static pl.edu.icm.visnow.lib.basic.testdata.TestPoints.TestPointsShared.POINT_COUNT;
import pl.edu.icm.visnow.lib.templates.visualization.modules.OutFieldVisualizationModule;
import pl.edu.icm.visnow.lib.types.VNIrregularField;
import pl.edu.icm.visnow.lib.utils.SwingInstancer;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class TestPoints extends OutFieldVisualizationModule
{

    protected GUI computeUI = null;
    protected boolean fromUI = false;
    public static OutputEgg[] outputEggs = null;

    /**
     * Creates a new instance of TestGeometryObject
     */
    public TestPoints()
    {
        parameters.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                startAction();
            }
        });
        SwingInstancer.swingRunAndWait(new Runnable()
        {
            @Override
            public void run()
            {
                computeUI = new GUI();
                ui.addComputeGUI(computeUI);
                setPanel(ui);
                computeUI.setParameters(parameters);
            }
        });
    }

    @Override
    protected Parameter[] createDefaultParameters()
    {
        return new Parameter[]{
            new Parameter<>(POINT_COUNT, 50),};
    }

    @Override
    public boolean isGenerator()
    {
        return true;
    }

    public void createTestPoints(Parameters p)
    {
        int nPoints = p.get(POINT_COUNT);
        outField = new IrregularField(nPoints);
        outIrregularField = (IrregularField) outField;
        FloatLargeArray coords = new FloatLargeArray((long) nPoints * 3, false);
        IntLargeArray data = new IntLargeArray(nPoints, false);
        FloatLargeArray vData = new FloatLargeArray((long) nPoints * 3, false);
        String[] texts = new String[nPoints];
        int[] cells = new int[nPoints];
        byte[] orient = new byte[nPoints];
        for (int i = 0; i < nPoints; i++) {
            data.setInt(i, i + 1);
            texts[i] = "point " + i;
            cells[i] = i;
            orient[i] = 1;
        }
        for (long i = 0; i < nPoints; i++) {
            for (long j = 0; j < 3; j++)
                coords.setFloat(3 * i + j, (float) random() - .5f);
            vData.setFloat(3 * i, -coords.getFloat(3 * i + 1) + (float) random() / 5 - .1f);
            vData.setFloat(3 * i + 1, -coords.getFloat(3 * i) + (float) random() / 5 - .1f);
            vData.setFloat(3 * i + 2, -coords.getFloat(3 * i + 2) + (float) random() / 5 - .1f);
        }
        CellArray ca = new CellArray(CellType.POINT, cells, orient, null);
        CellSet cs = new CellSet();
        cs.setCellArray(ca);
        outIrregularField.addCellSet(cs);
        outIrregularField.setCurrentCoords(coords);
        outIrregularField.addComponent(DataArray.create(data, 1, "points"));
        outIrregularField.addComponent(DataArray.create(texts, 1, "texts"));
        outIrregularField.addComponent(DataArray.create(vData, 3, "vectors"));
    }

    @Override
    protected void notifySwingGUIs(pl.edu.icm.visnow.engine.core.ParameterProxy clonedParameterProxy, boolean resetFully, boolean setRunButtonPending)
    {
        computeUI.updateGUI(clonedParameterProxy);
    }

    @Override
    public void onActive()
    {
        Parameters p;
        synchronized (parameters) {
            p = parameters.getReadOnlyClone();
        }
        notifyGUIs(p, false, false);
        
        createTestPoints(p);
        prepareOutputGeometry();
        show();
        setOutputValue("outField", new VNIrregularField(outIrregularField));
    }
}
