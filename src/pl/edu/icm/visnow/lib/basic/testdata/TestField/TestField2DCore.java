//<editor-fold defaultstate="collapsed" desc=" License ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the 
 University of Warsaw, Interdisciplinary Centre for Mathematical and 
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland. 

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.basic.testdata.TestField;

import java.util.ArrayList;
import java.util.List;
import static org.apache.commons.math3.util.FastMath.PI;
import static org.apache.commons.math3.util.FastMath.sqrt;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;
import pl.edu.icm.jscic.TimeData;
import pl.edu.icm.visnow.engine.core.ProgressAgent;
import static pl.edu.icm.visnow.lib.basic.testdata.TestField.TestFieldShared.*;

/**
 * 2D data components computational core.
 * <p>
 * @author Marcin Szpak, University of Warsaw, ICM
 */
public class TestField2DCore
{
    private final ProgressAgent progressAgent;

    private FloatLargeArray dataGaussians, dataGaussians1, dataGaussiansSwirls, 
                            dataGaussiansGradient, dataTime0, dataTime1, dataTime2, dataTime3;
    private FloatLargeArray[] vectorDataTime = new FloatLargeArray[121];
    private UnsignedByteLargeArray dataByteGaussians;
    private ShortLargeArray dataShortGaussians;
    private IntLargeArray dataIntGaussians;
    private ComplexFloatLargeArray dataComplexGaussians;
    private FloatLargeArray fImData, fReData;
    private StringLargeArray dataStringGaussians;
    private LogicLargeArray dataLogicGaussians;

    private boolean gaussiansQ;
    private boolean gaussians1Q;
    private boolean gaussiansSwirls;
    private boolean gaussiansGradientQ;
    private boolean byteGaussiansQ;
    private boolean shortGaussiansQ;
    private boolean intGaussiansQ;
    private boolean complexGaussiansQ;
    private boolean stringGaussiansQ;
    private boolean logicGaussiansQ;
    private boolean timeFieldQ;
    private boolean vectorTimeFieldQ;

    private TestField2DCore(ProgressAgent progressAgent)
    {
        this.progressAgent = progressAgent;
    }

    private class Compute implements Runnable
    {
        double[][] randoms;
        int[] dims;
        int nThreads;
        int iThread;
        int n;
        double res;

        public Compute(int nThreads, int[] dims, int iThread, double[][] randoms)
        {
            this.nThreads = nThreads;
            this.iThread = iThread;
            this.dims = dims;
            n = dims[0] * dims[1];
            res = Math.sqrt(dims[0] * dims[1]);
            this.randoms = randoms;
        }

        @Override
        public void run()
        {
            for (long j = iThread; j < dims[1]; j += nThreads) {
                progressAgent.increase();
                for (long i = 0, l = dims[0] * j; i < dims[0]; i++, l++) {
                    double u = (2. * i - dims[0]) / res;
                    double v = (2. * j - dims[1]) / res;
                    double s = 0, t = 0, w = 0, sg[] = new double[]{0, 0}, sr[] = new double[]{0, 0};
                    for (int k = 0; k < randoms.length; k++) {
                        double r = randoms[k][4] * Math.exp(-(u - randoms[k][0]) * (u - randoms[k][0]) / randoms[k][2] -
                                (v - randoms[k][1]) * (v - randoms[k][1]) / randoms[k][3]);
                        s += r;
                        sg[0] += -2 * r * (u - randoms[k][0]) / randoms[k][2];
                        sg[1] += -2 * r * (v - randoms[k][1]) / randoms[k][3];
                        sr[0] += r * (.1 * (u - randoms[k][0]) - (v - randoms[k][1])) / randoms[k][3];
                        sr[1] += r * (.1 * (v - randoms[k][1]) + (u - randoms[k][0])) / randoms[k][2];
                        if (k > 1)
                            t += r;
                        if (k > 3)
                            w += r;
                    }
                    if (gaussiansQ || byteGaussiansQ) {
                        dataGaussians.setFloat(l, (float) s);
                    }
                    if (gaussians1Q) {
                        dataGaussians1.setFloat(l, (float) t);
                    }
                    if (gaussiansSwirls) {
                        dataGaussiansSwirls.setFloat(2 * l, (float) sr[0]);
                        dataGaussiansSwirls.setFloat(2 * l + 1, (float) sr[1]);
                    }
                    if (gaussiansGradientQ) {
                        dataGaussiansGradient.setFloat(2 * l, (float) sg[0]);
                        dataGaussiansGradient.setFloat(2 * l + 1, (float) sg[1]);
                    }
                    //datax.setFloat(l, 5 * (float) (sin(5 * u) + cos(5 * v) + 2));
                    if (shortGaussiansQ) {
                        dataShortGaussians.setShort(l, (short) s);
                    }
                    if (intGaussiansQ) {
                        dataIntGaussians.setInt(l, (int) s);
                    }
                    if (complexGaussiansQ) {
                        fReData.setFloat(l, (float) s);
                        fImData.setFloat(l, (float) t);
                    }
                    if (stringGaussiansQ) {
                        dataStringGaussians.set(l, "v=" + (float) s);
                    }
                    if (logicGaussiansQ) {
                        dataLogicGaussians.setByte(l, s < 1.0 ? (byte) 1 : (byte) 0);
                    }
                    if (timeFieldQ) {
                        double r0 = .1234567;
                        double r000 = sqrt((u - r0) * (u - r0) + (v - r0) * (v - r0));
                        double r010 = sqrt((u - r0) * (u - r0) + (v + r0) * (v + r0));
                        double r100 = sqrt((u + r0) * (u + r0) + (v - r0) * (v - r0));
                        double r110 = sqrt((u + r0) * (u + r0) + (v + r0) * (v + r0));
                        dataTime0.setFloat(l, (float) (1 / (.5 + u * u + v * v)));
                        dataTime1.setFloat(l, (float) (1 / (.5 + r000)));
                        dataTime2.setFloat(l, (float) (1 / (.5 + r000) - 1 / (.5 + r010)));
                        dataTime3.setFloat(l, (float) (1 / (.5 + r000) - 1 / (.5 + r010) - 1 / (.5 + r100) + 1 / (.5 + r110)));
                    }
                }
            }
        }
    }

    static List<DataArray> createDataArrays(int nThreads, int[] dims, int[] components, double[][] randoms, ProgressAgent progressAgent)
    {
        return new TestField2DCore(progressAgent).createDataArrays(nThreads, dims, components, randoms);
    }

    private List<DataArray> createDataArrays(int nThreads, int[] dims, int[] components, double[][] randoms)
    {
        int n = dims[0] * dims[1];
        // 10/150
        progressAgent.increase((int) dims[1] / 10);

        for (int i = 0; i < components.length; i++) {
            String componentName = FIELD_NAMES_2D[components[i]];
            switch (componentName) {
                case GAUSSIANS:
                    gaussiansQ = true;
                    break;
                case GAUSSIANS1:
                    gaussians1Q = true;
                    break;
                case GAUSSIANS_SWIRLS:
                    gaussiansSwirls = true;
                    break;
                case GAUSSIANS_GRADIENT:
                    gaussiansGradientQ = true;
                    break;
                case BYTE_GAUSSIANS:
                    byteGaussiansQ = true;
                    break;
                case SHORT_GAUSSIANS:
                    shortGaussiansQ = true;
                    break;
                case INT_GAUSSIANS:
                    intGaussiansQ = true;
                    break;
                case COMPLEX_GAUSSIANS:
                    complexGaussiansQ = true;
                    break;
                case STRING_GAUSSIANS:
                    stringGaussiansQ = true;
                    break;
                case LOGIC_GAUSSIANS:
                    logicGaussiansQ = true;
                    break;
                case TIME_FIELD:
                    timeFieldQ = true;
                    break;
                case TIME_VECTOR_FIELD:
                    vectorTimeFieldQ = true;
                    break;
                default:
                    throw new IllegalStateException("Incorrect field component: " + componentName);
            }
        }

        if (gaussiansQ) dataGaussians = new FloatLargeArray(n, false);
        if (gaussians1Q) dataGaussians1 = new FloatLargeArray(n, false);
        if (gaussiansSwirls) dataGaussiansSwirls = new FloatLargeArray(2 * n, false);
        if (gaussiansGradientQ) dataGaussiansGradient = new FloatLargeArray(2 * n, false);
        if (byteGaussiansQ) {
            dataByteGaussians = new UnsignedByteLargeArray(n, false);
            if (dataGaussians == null) dataGaussians = new FloatLargeArray(n, false);
        }
        if (shortGaussiansQ) dataShortGaussians = new ShortLargeArray(n, false);
        if (intGaussiansQ) dataIntGaussians = new IntLargeArray(n, false);
        if (complexGaussiansQ) {
            dataComplexGaussians = new ComplexFloatLargeArray(n);
            fReData = dataComplexGaussians.getRealArray();
            fImData = dataComplexGaussians.getImaginaryArray();
        }
        if (stringGaussiansQ) dataStringGaussians = new StringLargeArray(n, DataArray.MAX_STRING_LENGTH, false);
        if (logicGaussiansQ) dataLogicGaussians = new LogicLargeArray(n);
        if (timeFieldQ) {
            dataTime0 = new FloatLargeArray(n, false);
            dataTime1 = new FloatLargeArray(n, false);
            dataTime2 = new FloatLargeArray(n, false);
            dataTime3 = new FloatLargeArray(n, false);
        }
        if (vectorTimeFieldQ)
            for (int i = 0; i < vectorDataTime.length; i++) 
                vectorDataTime[i] = new FloatLargeArray(2 * n, false);

        // 20/150
        progressAgent.increase((int) dims[1] / 10);


        Thread[] workThreads = new Thread[nThreads];
        for (int i = 0; i < workThreads.length; i++) {
            workThreads[i] = new Thread(new Compute(nThreads, dims, i, randoms));
            workThreads[i].start();
        }
        for (Thread workThread : workThreads)
            try {
                workThread.join();
            } catch (InterruptedException e) {
            }

        List<DataArray> dataArrays = new ArrayList<>();

        if (gaussiansQ) dataArrays.add(DataArray.create(dataGaussians, 1, GAUSSIANS));

        if (gaussians1Q) dataArrays.add(DataArray.create(dataGaussians1, 1, GAUSSIANS1));
        if (gaussiansSwirls) dataArrays.add(DataArray.create(dataGaussiansSwirls, 2, GAUSSIANS_SWIRLS));
        if (gaussiansGradientQ) dataArrays.add(DataArray.create(dataGaussiansGradient, 2, GAUSSIANS_GRADIENT));

        if (byteGaussiansQ) {
            DataArray bdta = DataArray.create(dataGaussians, 1, "gaussians");

            float min = (float) bdta.getPreferredMinValue();
            float max = (float) bdta.getPreferredMaxValue();
            float tmp = 255 / (max - min);
            for (long i = 0; i < dataGaussians.length(); i++)
                dataByteGaussians.setByte(i, (byte) (0xff & (int)(tmp * (dataGaussians.get(i) - min))));
            bdta = DataArray.create(dataByteGaussians, 1, BYTE_GAUSSIANS);
            bdta.setPreferredRanges(0, 255, min, max);
            dataArrays.add(bdta);
        }
        if (shortGaussiansQ) dataArrays.add(DataArray.create(dataShortGaussians, 1, SHORT_GAUSSIANS));
        if (intGaussiansQ) dataArrays.add(DataArray.create(dataIntGaussians, 1, INT_GAUSSIANS));
        if (complexGaussiansQ) dataArrays.add(DataArray.create(dataComplexGaussians, 1, COMPLEX_GAUSSIANS));
        if (stringGaussiansQ) dataArrays.add(DataArray.create(dataStringGaussians, 1, STRING_GAUSSIANS));
        if (logicGaussiansQ) {
            DataArray lda = DataArray.create(dataLogicGaussians, 1, LOGIC_GAUSSIANS); 
            lda.setPreferredRanges(0, 1, 0, 1);
            dataArrays.add(lda);
        }
        if (timeFieldQ) {
            DataArray da = DataArray.create(dataTime3, 1, TIME_FIELD);
            da.addRawArray(dataTime2, 1);
            da.addRawArray(dataTime1, 2);
            da.addRawArray(dataTime0, 3);
            dataArrays.add(da);
        }
        if (vectorTimeFieldQ) {
            int nTimesteps = 300;
            ArrayList<Float> timeSeries = new ArrayList<>();
            ArrayList<LargeArray> dataSeries = new ArrayList<>();
            for (int tStep = 0; tStep < nTimesteps; tStep++) {
                timeSeries.add(new Float(tStep));
                float x0 = .5f * dims[0] + .25f * dims[0] * (float)Math.cos(tStep / 10.);
                float y0 = .5f * dims[1] + dims[0] * (.6f * tStep / (float)nTimesteps  + .25f * (float)Math.sin(tStep / 10.) - .3f);
                float[] vect = new float[2 * n];
                for (int i = 0, l = 0; i < dims[1]; i++)
                    for (int j = 0; j < dims[0]; j++, l += 2) {
                            float u = j - x0;
                            float v = i - y0;
                            float w = .05f / ((float)Math.sqrt(u * u + v * v) + 13);
                            vect[l    ] =  w * (v - u) ;
                            vect[l + 1] = -w * (v + u);
                        }
                dataSeries.add(new FloatLargeArray(vect));
            }
            TimeData vf = new TimeData(timeSeries, dataSeries, 0);
            dataArrays.add(DataArray.create(vf, 2, "nonstationary_vector_field"));
        }
        
        return dataArrays;
    }
    
    public static float[] createRadialCoords(int resolution, int[] dims)
    {
        float[] crds = new float[3 * dims[0] * dims[1]];
        for (int i = 0, l = 0; i < dims[1]; i++) {
            float s = (float)Math.sin(PI * i / (4 * resolution));
            float c = (float)Math.cos(PI * i / (4 * resolution));
            for (int j = 0; j < dims[0]; j++, l += 3) {
                float r = (j * .7f) / (resolution - 1.f) + .3f;
                crds[l]     = c * r;
                crds[l + 1] = s * r;
                crds[l + 2] = 0;
            }
        }
        return crds;
    }
    public static float[] createSphericalCoords(int resolution, int[] dims)
    {
        float[] crds = new float[3 * dims[0] * dims[1]];
        for (int i = 0, l = 0; i < dims[1]; i ++) {
            float s0 = (float)Math.sin(PI * i / (2 * resolution) - PI / 4);
            float c0 = (float)Math.cos(PI * i / (2 * resolution) - PI / 4);
            for (int j = 0; j < dims[0]; j++, l += 3) {
                float s = (float)Math.sin(PI * j / (2 * resolution));
                float c = (float)Math.cos(PI * j / (2 * resolution));
                    crds[l]     = c0 * c;
                    crds[l + 1] = c0 * s;
                    crds[l + 2] = s0 ;
                }
            }
        return crds;
    }

}
