//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">


package pl.edu.icm.visnow.lib.utils.pointProbe;

import java.util.Arrays;
import pl.edu.icm.visnow.lib.utils.probeInterfaces.Probe;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.visnow.geometries.objects.generics.*;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.cells.SimplexPosition;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.visnow.engine.core.ParameterChangeListener;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph;
import static pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyph.GlyphType.*;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphGUI;
import pl.edu.icm.visnow.geometries.interactiveGlyphs.InteractiveGlyphParams;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.lib.utils.interpolation.FieldPosition;
import static pl.edu.icm.visnow.lib.utils.interpolation.SubsetGeometryComponents.INDEX_COORDS;
/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */
public class GeometricPointProbe extends Probe 
{

    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
    }

    protected Field inField = null;
    protected RegularField regularInField = null;
    protected IrregularField irregularInField = null;
    protected int trueNSpace = 3;
    protected int[] dims;
    protected IrregularField pointField = null;
    
    protected InteractiveGlyph       glyph       = new InteractiveGlyph(POINT);
    protected InteractiveGlyphParams glyphParams = glyph.getParams();
    protected InteractiveGlyphGUI    glyphGUI    = glyph.getComputeUI();
    protected float[]                center      = glyphParams.getCenter(); 
    
    protected DataMappingParams mapParams;
    
    
    public GeometricPointProbe()
    {
        glyph.addParameterChangelistener(new ParameterChangeListener()
        {
            @Override
            public void parameterChanged(String name)
            {
                updateProbe();
                show();
            }
        });
        glyphParams.setFireWhenAdjusting(true);
        glyph.setName("probeGlyph");
    }
    
    public void probePosition()
    {
        float[] ind = null; 
        pointField = new IrregularField(1);
        CellSet pointCells = new CellSet("point");
        pointCells.addCells(new CellArray(CellType.POINT, new int[]{0}, new byte[]{(byte)1}, null));
        pointField.addCellSet(pointCells);
        center      = Arrays.copyOf(glyphParams.getCenter(), 3); 
        pointField.setCurrentCoords(new FloatLargeArray(center));
        if (inField instanceof RegularField) {
            switch (regularInField.getDimNum()) {
            case 3:
               ind = regularInField.getFloatIndices(center[0], center[1], center[2]); 
               break;
            case 2:
               ind = regularInField.getFloatIndices(center[0], center[1]); 
               break;
            case 1:
               ind = new float[] {center[0]};
               break;
            }
            pointField.removeComponents();
            pointField.addComponent(DataArray.create(ind, dims.length, INDEX_COORDS));
        }
        else {
            SimplexPosition pos = inField.getFieldCoords(center);
            if (pos == null)
                return;
            FieldPosition[] nodes = new FieldPosition[] {new FieldPosition(pos)};
            pointField.removeComponents();
            for (DataArray component : inField.getComponents()) 
                pointField.addComponent(FieldPosition.interpolate(nodes, component));
        }
    }
    
    protected final void updateProbe()
    {
        probePosition();
        fireStateChanged(false);
    }
    
    @Override
    public void setInData(Field inField, DataMappingParams mapParams)
    {
        this.mapParams = mapParams;
        this.inField = inField;
        if (inField instanceof RegularField) {
            irregularInField = null;
            regularInField = (RegularField)inField;
            dims = regularInField.getDims();
        }
        else {
            irregularInField = (IrregularField)inField;
            regularInField = null;
        }
        trueNSpace = inField.getTrueNSpace();
        
        glyph.setField(inField);
        if (trueNSpace == 2) {
            glyph.setType(PLANAR_LINE);
            glyph.setField(inField);
            glyph.getGlyph().setName("plane_point");
        }
        else {
            glyph.setType(LINE);
            glyph.setField(inField);
            glyph.getGlyph().setName("point");
        }
        if (pointField != null && mapParams != null)
            mapParams.setInData(pointField, (DataContainer)null);
    }

    @Override
    public IrregularField getSliceField() {
        return pointField;
    }

    public void setMapParams(DataMappingParams mapParams)
    {
        this.mapParams = mapParams;
    }

    
    @Override
    public float[] getPlaneCenter()
    {
        return center;
    }
    
    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return glyph;
    }
    
    public InteractiveGlyph getGlyph() {
        return glyph;
    }

    @Override
    public InteractiveGlyphGUI getGlyphGUI()
    {
        return glyphGUI;
    }
    
    @Override
    public void hide()
    {
        glyph.hide();
    }

    @Override
    public void show()
    {
         glyph.show();
    }
}
