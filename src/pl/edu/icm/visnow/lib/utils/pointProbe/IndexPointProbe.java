//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.pointProbe;

import pl.edu.icm.visnow.lib.utils.probeInterfaces.Probe;
import pl.edu.icm.visnow.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceParams;
import javax.swing.JPanel;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.geometries.objects.CroppedRegularFieldOutline;
import pl.edu.icm.visnow.geometries.objects.generics.OpenBranchGroup;
import pl.edu.icm.visnow.geometries.parameters.DataMappingParams;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEvent;
import pl.edu.icm.visnow.geometries.viewer3d.eventslisteners.render.RenderEventListener;
import pl.edu.icm.visnow.lib.gui.FieldBasedUI.IndexSliceUI.IndexSliceGUI;
import pl.edu.icm.visnow.lib.utils.field.IndexPointSlice;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */


public class IndexPointProbe extends Probe
{
    
    protected RegularField inField = null;
    protected IndexSliceParams params = new IndexSliceParams();
    protected IndexSliceGUI gui = new IndexSliceGUI(IndexSliceParams.Type.POINT);
    protected int[] inDims;
    protected IrregularField slice;
    protected CroppedRegularFieldOutline  glyph = new CroppedRegularFieldOutline();
    protected OpenBranchGroup parent = new OpenBranchGroup();
    protected boolean glyphVisible = true, lastGlyphVisible = true;

    protected RenderEventListener mapEventListener = new RenderEventListener()
    {
        @Override
        public void renderExtentChanged(RenderEvent e)
        {
            updateCurrentProbeGeometry();
        }
    };
    
    public IndexPointProbe()
    {
        params.setType(IndexSliceParams.Type.POINT);
        gui.setParams(params);
        params.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                glyphVisible = params.isGlyphVisible();
                glyph.updateBoxCoords(params.getPosition(), params.getFixed(), params.getLow(), params.getUp());
                System.out.printf("%8.3f %8.3f %8.3f %n",
                        glyph.getCenter()[0], glyph.getCenter()[1], glyph.getCenter()[2]);
                slice = (IrregularField)IndexPointSlice.slice(inField, params);
                fireStateChanged(false);
            }
        });
        glyph.setName("probeGroup");
        parent.setName("probeParentGroup");
    }
    
    @Override
    public final void setInData(Field field, DataMappingParams mapParams)
    {
        if (!(field instanceof RegularField))
            return;
        inField = (RegularField)field;
        glyph.setField(inField);
        inDims = inField.getDims();
        params.setActive(false);
        params.setFieldSchema(inField.getSchema());
        params.setActive(true);
        updateCurrentProbeGeometry();
    }
    
    @Override
    public void setDataMappingParams(DataMappingParams dataMappingParams)
    {
    }
   
    @Override
    public OpenBranchGroup getGlyphGeometry()
    {
        return parent;
    }
    
    public void updateCurrentProbeGeometry()
    {
        glyph.setPosition(params.getPosition(), params.getFixed(), params.getLow(), params.getUp());
        show();
    }
    
    public float[] getPlaneCenter()
    {
        if (slice == null)
            return new float[] {0,0,0};
        return slice.getCurrentCoords().getFloatData();
    }

    public void hide()
    {
        if (glyph != null)
            glyph.detach();
    }

    public void show()
    {
        if (glyph.getParent() == null)
            parent.addChild(glyph);
    }

    @Override
    public JPanel getGlyphGUI()
    {
        return gui;
    }

    @Override
    public IrregularField getSliceField()
    {
        return slice;
    }

    @Override
    public RegularField getRegularSliceField()
    {
        return null;
    }
}
