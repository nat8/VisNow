//</editor-fold>

package pl.edu.icm.visnow.lib.utils.interpolation;

/**
 *
 * @author know (Krzysztof S. Nowinski (know@icm.edu.pl)
 * Warsaw University, Interdisciplinary Centre
 * for Mathematical and Computational Modelling)
 */

public class SubsetGeometryComponents
{
    public static final String NODE_POSITIONS      = "node_positions";
    public static final String INDEX_COORDS        = "index_space_coords";
    public static final String NODE_INDICES        = "node_indices";
    public static final String LINE_SLICE_COORDS   = "line_slice_coords";
    public static final String PLANAR_SLICE_COORDS = "planar_slice_coords";
}
