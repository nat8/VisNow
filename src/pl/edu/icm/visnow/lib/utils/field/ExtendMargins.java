//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version. */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.field;

import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;
import pl.edu.icm.jlargearrays.DoubleLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LargeArrayType;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.LargeArrayUtils;
import pl.edu.icm.visnow.lib.utils.PaddingType;

/**
 *
 * @author Krzysztof S. Nowinski
 * @author Piotr Wendykier
 * <p>
 * <p>
 * University of Warsaw, ICM
 */
public class ExtendMargins
{

    /**
     * Extends float array <code>data</code> (regarded as array of dimensions
     * <code>dims</code>) by <code>margins</code>. New array has dimensions
     * <code>extDims</code>,
     * <code>extDims[i] = margins[i][0] + dims[i] + margins[i][1]</code>.
     *
     * @param dims           - dimensions of original array
     * @param vlen           - length of vectors elements of original array
     * @param extDims        - dimensions of extended array
     * @param margins        - margin widths
     * @param data           - original data array of length <code>vlen * dims[0] * ...</code>
     * @param method         - if <code>PaddingType.ZERO</code>, margin is filled by
     *                       zeros, if <code>PaddingType.FIXED</code>, margin is filled by nearest
     *                       values of original array, if <code>PaddingType.PERIODIC</code>, margin is
     *                       filled by periodic extension, if <code>PaddingType.REFLECTED</code>,
     *                       margin is filled by reflected extension.
     * <p>
     * @param reflectVectors
     *                       <p>
     * @return extended data array of length <code>vlen * extDims[0] * ...</code>
     */
    public static LargeArray extendMargins(long[] dims, int vlen, long[] extDims, long[][] margins, LargeArray data, PaddingType method, boolean reflectVectors)
    {
        if (extDims.length < dims.length) {
            throw new IllegalArgumentException("extDims.length cannot be smaller than dims.length");
        }

        long nExt = 1;
        for (int i = 0; i < extDims.length; i++) {
            nExt *= extDims[i];
        }
        LargeArray extData = LargeArrayUtils.create(data.getType(), vlen * nExt);
        long offset = 0;

        switch (dims.length) {
            case 3:
                if (method == PaddingType.ZERO) {
                    for (long i = 0, m = 0; i < dims[2]; i++) {
                        for (long j = 0; j < dims[1]; j++) {
                            for (long k = 0, l = ((margins[2][0] + i) * extDims[1] + margins[1][0] + j) * extDims[0] + margins[0][0]; k < dims[0]; k++, l++, m++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l * vlen + n, data.get(m * vlen + n));
                                }
                            }
                        }
                    }
                } else if (method == PaddingType.FIXED) {
                    for (long i = 0, m = 0; i < dims[2]; i++) {
                        for (long j = 0; j < dims[1]; j++) {
                            for (long k = 0, l = ((margins[2][0] + i) * extDims[1] + margins[1][0] + j) * extDims[0] + margins[0][0]; k < dims[0]; k++, l++, m++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l * vlen + n, data.get(m * vlen + n));
                                }
                            }
                        }
                    }
                    long l = 0;
                    for (long i = 0; i < dims[2]; i++) {
                        for (long j = 0; j < dims[1]; j++) {
                            l = (((margins[2][0] + i) * extDims[1] + margins[1][0] + j) * extDims[0] + margins[0][0]) * vlen;
                            for (long k = 1; k <= margins[0][0]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l - k * vlen + n, extData.get(l + n));
                                }
                            }
                            l = (((margins[2][0] + i) * extDims[1] + margins[1][0] + j) * extDims[0] + margins[0][0] + dims[0] - 1) * vlen;
                            for (long k = 1; k <= margins[0][1]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l + k * vlen + n, extData.get(l + n));
                                }
                            }
                        }
                    }
                    long m = extDims[0];
                    for (long i = 0; i < dims[2]; i++) {
                        for (long j = 0; j < extDims[0]; j++) {
                            l = (((margins[2][0] + i) * extDims[1] + margins[1][0]) * extDims[0] + j) * vlen;
                            for (long k = 1; k <= margins[1][0]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l - k * m * vlen + n, extData.get(l + n));
                                }
                            }
                            l = (((margins[2][0] + i) * extDims[1] + margins[1][0] + dims[1] - 1) * extDims[0] + j) * vlen;
                            for (long k = 1; k <= margins[1][1]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l + k * m * vlen + n, extData.get(l + n));
                                }
                            }
                        }
                    }
                    m = extDims[1] * extDims[0];
                    for (long i = 0; i < extDims[1]; i++) {
                        for (long j = 0; j < extDims[0]; j++) {
                            l = ((margins[2][0] * extDims[1] + i) * extDims[0] + j) * vlen;
                            for (long k = 1; k <= margins[2][0]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l - k * m * vlen + n, extData.get(l + n));
                                }
                            }
                            l = (((margins[2][0] + dims[2] - 1) * extDims[1] + i) * extDims[0] + j) * vlen;
                            for (long k = 1; k <= margins[2][1]; k++) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(l + k * m * vlen + n, extData.get(l + n));
                                }
                            }
                        }
                    }
                } else if (method == PaddingType.PERIODIC) {
                    long sOff = margins[2][0];
                    long rOff = margins[1][0];
                    long cOff = margins[0][0];
                    for (long s = -sOff; s < extDims[2] - sOff; s++) {
                        long sOut = s + sOff;
                        long sIn = periodic(s, dims[2]);
                        for (long r = -rOff; r < extDims[1] - rOff; r++) {
                            long rOut = r + rOff;
                            long rIn = periodic(r, dims[1]);
                            for (long c = -cOff; c < extDims[0] - cOff; c++) {
                                long cOut = c + cOff;
                                long cIn = periodic(c, dims[0]);
                                long idx = (sIn * dims[1] * dims[0] + rIn * dims[0] + cIn) * vlen;
                                long idxExt = (sOut * extDims[1] * extDims[0] + rOut * extDims[0] + cOut) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxExt + n, data.get(idx + n));
                                }
                            }
                        }
                    }
                } else if (method == PaddingType.REFLECTED) {
                    long sOff = margins[2][0];
                    long rOff = margins[1][0];
                    long cOff = margins[0][0];
                    for (long s = -sOff; s < extDims[2] - sOff; s++) {
                        long sOut = s + sOff;
                        long sIn = reflected(s, dims[2]);
                        for (long r = -rOff; r < extDims[1] - rOff; r++) {
                            long rOut = r + rOff;
                            long rIn = reflected(r, dims[1]);
                            for (long c = -cOff; c < extDims[0] - cOff; c++) {
                                long cOut = c + cOff;
                                long cIn = reflected(c, dims[0]);
                                long idx = (sIn * dims[1] * dims[0] + rIn * dims[0] + cIn) * vlen;
                                long idxExt = (sOut * extDims[1] * extDims[0] + rOut * extDims[0] + cOut) * vlen;
                                if (reflectVectors == false || vlen == 1 || data.getType() == LargeArrayType.LOGIC || data.getType() == LargeArrayType.COMPLEX_FLOAT || data.getType() == LargeArrayType.COMPLEX_DOUBLE || data.getType() == LargeArrayType.STRING || data.getType() == LargeArrayType.OBJECT) {
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxExt + n, data.get(idx + n));
                                    }
                                } else {
                                    if(vlen >= 3) {
                                        if (sIn + sOff != sOut) {
                                            extData.setDouble(idxExt + 2, -data.getDouble(idx + 2));
                                        } else {
                                            extData.set(idxExt + 2, data.get(idx + 2));
                                        }
                                    }
                                    if(vlen >= 2) {
                                        if (rIn + rOff != rOut) {
                                            extData.setDouble(idxExt + 1, -data.getDouble(idx + 1));
                                        } else {
                                            extData.set(idxExt + 1, data.get(idx + 1));
                                        }
                                    }
                                    if (cIn + cOff != cOut) {
                                        extData.setDouble(idxExt, -data.getDouble(idx));
                                    } else {
                                        extData.set(idxExt, data.get(idx));
                                    }
                                }
                            }
                        }
                    }
                }
                break;
            case 2:
                if (extDims.length == 3) {
                    offset = margins[2][0] * extDims[1] * extDims[0] * vlen;
                }
                if (method == PaddingType.ZERO) {
                    for (long j = 0, m = 0; j < dims[1]; j++) {
                        for (long k = 0, l = (margins[1][0] + j) * extDims[0] + margins[0][0]; k < dims[0]; k++, l++, m++) {
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + l * vlen + n, data.get(m * vlen + n));
                            }
                        }
                    }
                } else if (method == PaddingType.FIXED) {
                    for (long j = 0, m = 0; j < dims[1]; j++) {
                        for (long k = 0, l = (margins[1][0] + j) * extDims[0] + margins[0][0]; k < dims[0]; k++, l++, m++) {
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + l * vlen + n, data.get(m * vlen + n));
                            }
                        }
                    }
                    long l = 0;
                    for (long j = 0; j < dims[1]; j++) {
                        l = ((margins[1][0] + j) * extDims[0] + margins[0][0]) * vlen;
                        for (long k = 1; k <= margins[0][0]; k++) {
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + l - k * vlen + n, extData.get(offset + l + n));
                            }
                        }
                        l = ((margins[1][0] + j) * extDims[0] + margins[0][0] + dims[0] - 1) * vlen;
                        for (long k = 1; k <= margins[0][1]; k++) {
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + l + k * vlen + n, extData.get(offset + l + n));
                            }
                        }
                    }
                    long m = extDims[0];
                    for (long j = 0; j < extDims[0]; j++) {
                        l = ((margins[1][0]) * extDims[0] + j) * vlen;
                        for (long k = 1; k <= margins[1][0]; k++) {
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + l - k * m * vlen + n, extData.get(offset + l + n));
                            }
                        }
                        l = ((margins[1][0] + dims[1] - 1) * extDims[0] + j) * vlen;
                        for (long k = 1; k <= margins[1][1]; k++) {
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + l + k * m * vlen + n, extData.get(offset + l + n));
                            }
                        }

                        if (extDims.length == 3) {
                            for (long s = 0; s < margins[2][0]; s++) {
                                long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                                for (long r = 0; r < extDims[1]; r++) {
                                    for (long c = 0; c < extDims[0]; c++) {
                                        long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            extData.set(idxOut + n, extData.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                            for (long s = margins[2][0] + 1; s < extDims[2]; s++) {
                                long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                                for (long r = 0; r < extDims[1]; r++) {
                                    for (long c = 0; c < extDims[0]; c++) {
                                        long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                        for (long n = 0; n < vlen; n++) {
                                            extData.set(idxOut + n, extData.get(idx + n));
                                        }
                                        idx += vlen;
                                    }
                                }
                            }
                        }

                    }
                } else if (method == PaddingType.PERIODIC) {
                    long rOff = margins[1][0];
                    long cOff = margins[0][0];
                    for (long r = -rOff; r < extDims[1] - rOff; r++) {
                        long rOut = r + rOff;
                        long rIn = periodic(r, dims[1]);
                        for (long c = -cOff; c < extDims[0] - cOff; c++) {
                            long cOut = c + cOff;
                            long cIn = periodic(c, dims[0]);
                            long idx = (rIn * dims[0] + cIn) * vlen;
                            long idxExt = (rOut * extDims[0] + cOut) * vlen;
                            for (long n = 0; n < vlen; n++) {
                                extData.set(offset + idxExt + n, data.get(idx + n));
                            }
                        }
                    }

                    if (extDims.length == 3) {
                        for (long s = 0; s < margins[2][0]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                        for (long s = margins[2][0] + 1; s < extDims[2]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                    }

                } else if (method == PaddingType.REFLECTED) {
                    long rOff = margins[1][0];
                    long cOff = margins[0][0];
                    for (long r = -rOff; r < extDims[1] - rOff; r++) {
                        long rOut = r + rOff;
                        long rIn = reflected(r, dims[1]);
                        for (long c = -cOff; c < extDims[0] - cOff; c++) {
                            long cOut = c + cOff;
                            long cIn = reflected(c, dims[0]);
                            long idx = (rIn * dims[0] + cIn) * vlen;
                            long idxExt = (rOut * extDims[0] + cOut) * vlen;
                            if (reflectVectors == false || vlen == 1 || data.getType() == LargeArrayType.LOGIC || data.getType() == LargeArrayType.COMPLEX_FLOAT || data.getType() == LargeArrayType.COMPLEX_DOUBLE || data.getType() == LargeArrayType.STRING || data.getType() == LargeArrayType.OBJECT) {
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(offset + idxExt + n, data.get(idx + n));
                                }
                            } else {
                                if( vlen >= 2) {
                                    if (rIn + rOff != rOut) {
                                        extData.setDouble(idxExt + 1, -data.getDouble(idx + 1));
                                    } else {
                                        extData.set(idxExt + 1, data.get(idx + 1));
                                    }
                                }
                                if (cIn + cOff != cOut) {
                                    extData.setDouble(idxExt, -data.getDouble(idx));
                                } else {
                                    extData.set(idxExt, data.get(idx));
                                }
                            }
                        }
                    }

                    if (extDims.length == 3) {
                        for (long s = 0; s < margins[2][0]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                        for (long s = margins[2][0] + 1; s < extDims[2]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                    }

                }
                break;
            case 1:
                if (extDims.length == 2) {
                    offset = margins[1][0] * extDims[0] * vlen;
                } else if (extDims.length == 3) {
                    offset = margins[2][0] * extDims[1] * extDims[0] * vlen + margins[1][0] * extDims[0] * vlen;
                }
                if (method == PaddingType.ZERO) {
                    for (long k = 0, m = 0, l = margins[0][0]; k < dims[0]; k++, l++, m++) {
                        for (long n = 0; n < vlen; n++) {
                            extData.set(offset + l * vlen + n, data.get(m * vlen + n));
                        }
                    }
                } else if (method == PaddingType.FIXED) {
                    for (long k = 0, m = 0, l = margins[0][0]; k < dims[0]; k++, l++, m++) {
                        for (long n = 0; n < vlen; n++) {
                            extData.set(offset + l * vlen + n, data.get(m * vlen + n));
                        }
                    }
                    long l = (margins[0][0]) * vlen;
                    for (long k = 1; k <= margins[0][0]; k++) {
                        for (long n = 0; n < vlen; n++) {
                            extData.set(offset + l - k * vlen + n, extData.get(offset + l + n));
                        }
                    }
                    l = (margins[0][0] + dims[0] - 1) * vlen;
                    for (long k = 1; k <= margins[0][1]; k++) {
                        for (long n = 0; n < vlen; n++) {
                            extData.set(offset + l + k * vlen + n, extData.get(offset + l + n));
                        }
                    }

                    if (extDims.length == 2) {
                        for (long r = 0; r < margins[1][0]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long r = margins[1][0] + 1; r < extDims[1]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                    } else if (extDims.length == 3) {
                        for (long r = 0; r < margins[1][0]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long r = margins[1][0] + 1; r < extDims[1]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long s = 0; s < margins[2][0]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }

                        for (long s = margins[2][0] + 1; s < extDims[2]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                    }

                } else if (method == PaddingType.PERIODIC) {
                    long cOff = margins[0][0];
                    for (long c = -cOff; c < extDims[0] - cOff; c++) {
                        long cOut = c + cOff;
                        long cIn = periodic(c, dims[0]);
                        long idx = cIn * vlen;
                        long idxExt = cOut * vlen;
                        for (long n = 0; n < vlen; n++) {
                            extData.set(offset + idxExt + n, data.get(idx + n));
                        }
                    }

                    if (extDims.length == 2) {
                        for (long r = 0; r < margins[1][0]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long r = margins[1][0] + 1; r < extDims[1]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                    } else if (extDims.length == 3) {
                        for (long r = 0; r < margins[1][0]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long r = margins[1][0] + 1; r < extDims[1]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long s = 0; s < margins[2][0]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                        for (long s = margins[2][0] + 1; s < extDims[2]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                    }

                } else if (method == PaddingType.REFLECTED) {
                    long cOff = margins[0][0];
                    for (long c = -cOff; c < extDims[0] - cOff; c++) {
                        long cOut = c + cOff;
                        long cIn = reflected(c, dims[0]);
                        long idx = cIn * vlen;
                        long idxExt = cOut * vlen;
                        for (long n = 0; n < vlen; n++) {
                            extData.set(offset + idxExt + n, data.get(idx + n));
                        }
                    }

                    if (extDims.length == 2) {
                        for (long r = 0; r < margins[1][0]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long r = margins[1][0] + 1; r < extDims[1]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                    } else if (extDims.length == 3) {
                        for (long r = 0; r < margins[1][0]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long r = margins[1][0] + 1; r < extDims[1]; r++) {
                            long idx = margins[1][0] * extDims[0] * vlen;
                            for (long c = 0; c < extDims[0]; c++) {
                                long idxOut = (r * extDims[0] + c) * vlen;
                                for (long n = 0; n < vlen; n++) {
                                    extData.set(idxOut + n, extData.get(idx + n));
                                }
                                idx += vlen;
                            }
                        }

                        for (long s = 0; s < margins[2][0]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                        for (long s = margins[2][0] + 1; s < extDims[2]; s++) {
                            long idx = margins[2][0] * extDims[1] * extDims[0] * vlen;
                            for (long r = 0; r < extDims[1]; r++) {
                                for (long c = 0; c < extDims[0]; c++) {
                                    long idxOut = (s * extDims[1] * extDims[0] + r * extDims[0] + c) * vlen;
                                    for (long n = 0; n < vlen; n++) {
                                        extData.set(idxOut + n, extData.get(idx + n));
                                    }
                                    idx += vlen;
                                }
                            }
                        }
                    }
                }
                break;
        }
        return extData;
    }

    /**
     * Extends float array <code>data</code> (regarded as array of dimensions
     * <code>dims</code>) by <code>margins</code>. New array has dimensions
     * <code>extDims</code>,
     * <code>extDims[i] = margins[i][0] + dims[i] + margins[i][1]</code>.
     *
     * @param dims           - dimensions of original array
     * @param vlen           - length of vectors elements of original array
     * @param extDims        - dimensions of extended array
     * @param margins        - margin widths
     * @param data           - original data array of length <code>vlen * dims[0] *
     *                       ...</code>
     * @param method         - if <code>PaddingType.ZERO</code>, margin is filled by
     *                       zeros, if <code>PaddingType.FIXED</code>, margin is filled by nearest
     *                       values of original array, if <code>PaddingType.PERIODIC</code>, margin is
     *                       filled by periodic extension, if <code>PaddingType.REFLECTED</code>,
     *                       margin is filled by reflected extension.
     * <p>
     * @param reflectVectors
     *                       <p>
     * @return extended data array of length <code>vlen * extDims[0] *
     *         ...</code>
     */
    public static float[] extendMargins(int[] dims, int vlen, int[] extDims, int[][] margins, float[] data, PaddingType method, boolean reflectVectors)
    {
        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++) {
            dimsl[i] = dims[i];

        }
        long[] extDimsl = new long[extDims.length];
        for (int i = 0; i < extDimsl.length; i++) {
            extDimsl[i] = extDims[i];
        }
        long[][] marginsl = new long[margins.length][margins[0].length];
        for (int i = 0; i < marginsl.length; i++) {
            for (int j = 0; j < marginsl[0].length; j++) {
                marginsl[i][j] = margins[i][j];
            }
        }
        return (float[])extendMargins(dimsl, vlen, extDimsl, marginsl, new FloatLargeArray(data), method, reflectVectors).getData();
    }

    public static byte[] extendMargins(int[] dims, int vlen, int[] extDims, int[][] margins, byte[] data, PaddingType method, boolean reflectVectors)
    {

        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++) {
            dimsl[i] = dims[i];

        }
        long[] extDimsl = new long[extDims.length];
        for (int i = 0; i < extDimsl.length; i++) {
            extDimsl[i] = extDims[i];
        }
        long[][] marginsl = new long[margins.length][margins[0].length];
        for (int i = 0; i < marginsl.length; i++) {
            for (int j = 0; j < marginsl[0].length; j++) {
                marginsl[i][j] = margins[i][j];
            }
        }
        return (byte[])extendMargins(dimsl, vlen, extDimsl, marginsl, new UnsignedByteLargeArray(data), method, reflectVectors).getData();
    }

  
    public static short[] extendMargins(int[] dims, int vlen, int[] extDims, int[][] margins, short[] data, PaddingType method, boolean reflectVectors)
    {
        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++) {
            dimsl[i] = dims[i];

        }
        long[] extDimsl = new long[extDims.length];
        for (int i = 0; i < extDimsl.length; i++) {
            extDimsl[i] = extDims[i];
        }
        long[][] marginsl = new long[margins.length][margins[0].length];
        for (int i = 0; i < marginsl.length; i++) {
            for (int j = 0; j < marginsl[0].length; j++) {
                marginsl[i][j] = margins[i][j];
            }
        }
        return (short[])extendMargins(dimsl, vlen, extDimsl, marginsl, new ShortLargeArray(data), method, reflectVectors).getData();
    }

    public static int[] extendMargins(int[] dims, int vlen, int[] extDims, int[][] margins, int[] data, PaddingType method, boolean reflectVectors)
    {
        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++) {
            dimsl[i] = dims[i];

        }
        long[] extDimsl = new long[extDims.length];
        for (int i = 0; i < extDimsl.length; i++) {
            extDimsl[i] = extDims[i];
        }
        long[][] marginsl = new long[margins.length][margins[0].length];
        for (int i = 0; i < marginsl.length; i++) {
            for (int j = 0; j < marginsl[0].length; j++) {
                marginsl[i][j] = margins[i][j];
            }
        }
        return (int[])extendMargins(dimsl, vlen, extDimsl, marginsl, new IntLargeArray(data), method, reflectVectors).getData();
    }

    public static double[] extendMargins(int[] dims, int vlen, int[] extDims, int[][] margins, double[] data, PaddingType method, boolean reflectVectors)
    {
        long[] dimsl = new long[dims.length];
        for (int i = 0; i < dimsl.length; i++) {
            dimsl[i] = dims[i];

        }
        long[] extDimsl = new long[extDims.length];
        for (int i = 0; i < extDimsl.length; i++) {
            extDimsl[i] = extDims[i];
        }
        long[][] marginsl = new long[margins.length][margins[0].length];
        for (int i = 0; i < marginsl.length; i++) {
            for (int j = 0; j < marginsl[0].length; j++) {
                marginsl[i][j] = margins[i][j];
            }
        }
        return (double[])extendMargins(dimsl, vlen, extDimsl, marginsl, new DoubleLargeArray(data), method, reflectVectors).getData();
    }

    public static void fillOut3DMargins(byte[] data, int[] dims, byte val)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + k] = val;
            }
        }
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + k] = val;
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[dims[0] * i1 + i0] = data[dims[0] * i1 + i0 + k] = val;
            }
        }
    }

    public static void fillOut3DMargins(byte[] data, int[] dims)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + 1];
                data[(i2 * dims[1] + i1) * dims[0] + k] = data[(i2 * dims[1] + i1) * dims[0] + k - 1];
            }
        }
        k = dims[0] * (dims[1] - 1);
        int l = dims[0];
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + l];
                data[i2 * dims[1] * dims[0] + i0 + k] = data[i2 * dims[1] * dims[0] + i0 + k - l];
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        l = dims[0] * dims[1];
        for (int i = 0; i < dims[1] * dims[0]; i++) {
            data[i] = data[i + l];
            data[i + k] = data[i + k + l];
        }
    }

    public static void fillOut3DMargins(short[] data, int[] dims, short val)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + k] = val;
            }
        }
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + k] = val;
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[dims[0] * i1 + i0] = data[dims[0] * i1 + i0 + k] = val;
            }
        }
    }

    public static void fillOut3DMargins(short[] data, int[] dims)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + 1];
                data[(i2 * dims[1] + i1) * dims[0] + k] = data[(i2 * dims[1] + i1) * dims[0] + k - 1];
            }
        }
        k = dims[0] * (dims[1] - 1);
        int l = dims[0];
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + l];
                data[i2 * dims[1] * dims[0] + i0 + k] = data[i2 * dims[1] * dims[0] + i0 + k - l];
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        l = dims[0] * dims[1];
        for (int i = 0; i < dims[1] * dims[0]; i++) {
            data[i] = data[i + l];
            data[i + k] = data[i + k + l];
        }
    }

    public static void fillOut3DMargins(int[] data, int[] dims, int val)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + k] = val;
            }
        }
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + k] = val;
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[dims[0] * i1 + i0] = data[dims[0] * i1 + i0 + k] = val;
            }
        }
    }

    public static void fillOut3DMargins(int[] data, int[] dims)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + 1];
                data[(i2 * dims[1] + i1) * dims[0] + k] = data[(i2 * dims[1] + i1) * dims[0] + k - 1];
            }
        }
        k = dims[0] * (dims[1] - 1);
        int l = dims[0];
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + l];
                data[i2 * dims[1] * dims[0] + i0 + k] = data[i2 * dims[1] * dims[0] + i0 + k - l];
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        l = dims[0] * dims[1];
        for (int i = 0; i < dims[1] * dims[0]; i++) {
            data[i] = data[i + l];
            data[i + k] = data[i + k + l];
        }
    }

    public static void fillOut3DMargins(float[] data, int[] dims, float val)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + k] = val;
            }
        }
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + k] = val;
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[dims[0] * i1 + i0] = data[dims[0] * i1 + i0 + k] = val;
            }
        }
    }

    public static void fillOut3DMargins(float[] data, int[] dims)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + 1];
                data[(i2 * dims[1] + i1) * dims[0] + k] = data[(i2 * dims[1] + i1) * dims[0] + k - 1];
            }
        }
        k = dims[0] * (dims[1] - 1);
        int l = dims[0];
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + l];
                data[i2 * dims[1] * dims[0] + i0 + k] = data[i2 * dims[1] * dims[0] + i0 + k - l];
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        l = dims[0] * dims[1];
        for (int i = 0; i < dims[1] * dims[0]; i++) {
            data[i] = data[i + l];
            data[i + k] = data[i + k + l];
        }
    }

    public static void fillOut3DMargins(double[] data, int[] dims, double val)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + k] = val;
            }
        }
        k = dims[0] * (dims[1] - 1);
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + k] = val;
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        for (int i1 = 0; i1 < dims[1]; i1++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[dims[0] * i1 + i0] = data[dims[0] * i1 + i0 + k] = val;
            }
        }
    }

    public static void fillOut3DMargins(double[] data, int[] dims)
    {
        int vlen = data.length / (dims[0] * dims[1] * dims[2]);
        if (data.length != vlen * dims[0] * dims[1] * dims[2]) {
            return;
        }
        int k = dims[0] - 1;
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i1 = 0; i1 < dims[1]; i1++) {
                data[(i2 * dims[1] + i1) * dims[0]] = data[(i2 * dims[1] + i1) * dims[0] + 1];
                data[(i2 * dims[1] + i1) * dims[0] + k] = data[(i2 * dims[1] + i1) * dims[0] + k - 1];
            }
        }
        k = dims[0] * (dims[1] - 1);
        int l = dims[0];
        for (int i2 = 0; i2 < dims[2]; i2++) {
            for (int i0 = 0; i0 < dims[0]; i0++) {
                data[i2 * dims[1] * dims[0] + i0] = data[i2 * dims[1] * dims[0] + i0 + l];
                data[i2 * dims[1] * dims[0] + i0 + k] = data[i2 * dims[1] * dims[0] + i0 + k - l];
            }
        }
        k = dims[0] * dims[1] * (dims[2] - 1);
        l = dims[0] * dims[1];
        for (int i = 0; i < dims[1] * dims[0]; i++) {
            data[i] = data[i + l];
            data[i + k] = data[i + k + l];
        }
    }

    private static int reflected(int i, int n)
    {
        int ip = mod(i, 2 * n);
        if (ip < n) {
            return ip;
        } else {
            return n - (ip % n) - 1;
        }
    }

    private static int mod(int i, int n)
    {
        return ((i % n) + n) % n;
    }

    private static int periodic(int i, int n)
    {
        int ip = mod(i, 2 * n);
        if (ip < n) {
            return ip;
        } else {
            return (ip % n);
        }
    }

    private static int fixed(int i, int n)
    {
        if (i >= 0 && i < n) {
            return i;
        } else if (i < 0) {
            return 0;
        } else {
            return n - 1;
        }
    }

    private static long reflected(long i, long n)
    {
        long ip = mod(i, 2 * n);
        if (ip < n) {
            return ip;
        } else {
            return n - (ip % n) - 1;
        }
    }

    private static long mod(long i, long n)
    {
        return ((i % n) + n) % n;
    }

    private static long periodic(long i, long n)
    {
        long ip = mod(i, 2 * n);
        if (ip < n) {
            return ip;
        } else {
            return (ip % n);
        }
    }

    private static long fixed(long i, long n)
    {
        if (i >= 0 && i < n) {
            return i;
        } else if (i < 0) {
            return 0;
        } else {
            return n - 1;
        }
    }

    private ExtendMargins()
    {
    }
}
