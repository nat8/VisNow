//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.io;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.util.Locale;
import java.util.Vector;
import org.apache.commons.lang3.StringUtils;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.LargeArray;
import pl.edu.icm.jlargearrays.LogicLargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;
import pl.edu.icm.jscic.DataContainer;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.dataarrays.DataArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;
import static pl.edu.icm.jscic.dataarrays.DataArrayType.*;
import static pl.edu.icm.visnow.lib.utils.io.SerializedFieldWriter.LOGGER;

/**
 *
 * @author Krzysztof S. Nowinski, University of Warsaw ICM
 */


public class WriteContainer
{
    public static final void writeHeader(DataContainer container, PrintWriter headerWriter)
    {
        try {
            int maxCmpNameLen = 0;
            int maxUnitLen = 0;
            int maxVectorDesLen = 0;
            for (DataArray da: container.getComponents()) 
                if (da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_STRING) {
                    if (da.getName().length() > maxCmpNameLen)
                        maxCmpNameLen = da.getName().length();
                    if (da.getUnit() != null && !da.getUnit().isEmpty() && !da.getUnit().equals("1") &&
                        da.getUnit().length() > maxUnitLen)
                        maxUnitLen = da.getUnit().length();
                    int vDesLen = 0;
                    if (da.getVectorLength() > 1) {
                        if (da.getMatrixDims()[0] != da.getVectorLength()) {
                            vDesLen = 13;
                            if (da.isSymmetric())
                                vDesLen += 4;
                            else
                                vDesLen += 2;
                        } else
                           vDesLen = 10;
                    }
                    if (vDesLen > maxVectorDesLen)
                        maxVectorDesLen = vDesLen;
            }
            maxUnitLen += 1;
            for (int i = 0; i < container.getNComponents(); i++) {
                DataArray da = container.getComponent(i);
                if (!(da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_STRING))
                    continue;
                headerWriter.printf(Locale.US, "component %" + maxCmpNameLen + "s %7s,", 
                                    da.getName().replace(' ', '_').replace('.', '_'), (da.getType().toString()));
                if (da.getVectorLength() > 1) {
                    if (da.getMatrixDims()[0] != da.getVectorLength()) {
                        headerWriter.print(" array " + da.getMatrixDims()[0]); 
                        if (da.isSymmetric())
                            headerWriter.printf(" %" + (maxVectorDesLen - 9) + "s,", "sym");
                        else
                            headerWriter.printf(" " + da.getMatrixDims()[1] + " %" + (maxVectorDesLen - 7) + "s,", "");
                    } else 
                        headerWriter.printf("%" + (maxVectorDesLen - 1)  + "s,", "vector " + da.getVectorLength());
                }
                else 
                    headerWriter.printf("%" + (maxVectorDesLen + 1) + "s", " ");
                if (maxUnitLen > 0) { // nontrivial units present
                    if (da.getUnit() != null && !da.getUnit().isEmpty() && !da.getUnit().equals("1"))
                        headerWriter.printf(" unit %" + maxUnitLen + "s, ", da.getUnit());
                    else 
                        headerWriter.printf("%" + (maxUnitLen + 7) + "s ", " ");
                }
                headerWriter.printf("min %10.5f, max %10.5f, phys_min %10.5f, phys_max %10.5f",      
                                     da.getPreferredMinValue(),     da.getPreferredMaxValue(),
                                     da.getPreferredPhysMinValue(), da.getPreferredPhysMaxValue());
                if (da.getUserData() != null) {
                    headerWriter.print(", user:");
                    String[] udata = da.getUserData();
                    for (int j = 0; j < udata.length; j++) {
                        if (j > 0)
                            headerWriter.print(";");
                        headerWriter.print("\"" + udata[j] + "\"");
                    }
                }
                headerWriter.println();
            }
        } catch (Exception e) {
        }
    }
    
    public static final boolean writeBinary(DataContainer container, String prefix,
                                            PrintWriter headerWriter, MemoryMappedFileWriter largeContentOutput)
    {
        try {
            if (prefix != null && !prefix.isEmpty())
                prefix = prefix + ":";
            float[] timeSteps = container.getTimesteps();
            if (timeSteps.length == 1) {
                if (container instanceof Field) {
                    Field fld = (Field)container;
                    if (fld.getCurrentMask() != null) {
                        headerWriter.println("mask");
                        LogicLargeArray mask = fld.getCurrentMask();
                        largeContentOutput.writeLogicLargeArray(mask, 0, mask.length());
                    }
                    if (fld.getCurrentCoords() != null) {
                        headerWriter.println("coords");
                        FloatLargeArray coords = fld.getCurrentCoords();
                        largeContentOutput.writeFloatLargeArray(coords, 0, coords.length());
                    }
                }
                for (int i = 0; i < container.getNComponents(); i++) {
                    DataArray da = container.getComponent(i);
                    if (!da.isNumeric())
                        continue;
                    headerWriter.println(prefix + da.getName().replace(' ', '_').replace('.', '_'));
                    long length = da.getNElements() * da.getVectorLength();
                    if (da.getType() == DataArrayType.FIELD_DATA_COMPLEX) {
                        largeContentOutput.writeLargeArray(((ComplexFloatLargeArray)da.getRawArray()).getRealArray(), 0, length);
                        largeContentOutput.writeLargeArray(((ComplexFloatLargeArray)da.getRawArray()).getImaginaryArray(), 0, length);
                    }
                    else
                        largeContentOutput.writeLargeArray(da.getRawArray(), 0, length);
                }
            } else {
                for (int step = 0; step < timeSteps.length; step++) {
                    float t = timeSteps[step];
                    headerWriter.println("timestep " + t);
                    if (container instanceof Field) {
                        Field fld = (Field)container;
                        if (fld.isMaskTimestep(t)) {
                            headerWriter.println("mask");
                            LogicLargeArray mask = fld.getMask(t);
                            largeContentOutput.writeLogicLargeArray(mask, 0, mask.length());
                        }
                        if (fld.isCoordTimestep(t)) {
                            headerWriter.println("coords");
                            FloatLargeArray coords = fld.getCoords(t);
                            largeContentOutput.writeFloatLargeArray(coords, 0, coords.length());
                        }
                    }
                    for (int i = 0; i < container.getNComponents(); i++) {
                        DataArray da = container.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        headerWriter.println(da.getName().replace(' ', '_').replace('.', '_'));
                        long length = da.getNElements() * da.getVectorLength();
                        if (da.getType() == FIELD_DATA_COMPLEX) {
                            largeContentOutput.writeLargeArray(((ComplexFloatLargeArray)da.getRawArray(t)).getRealArray(), 0, length);
                            largeContentOutput.writeLargeArray(((ComplexFloatLargeArray)da.getRawArray(t)).getImaginaryArray(), 0, length);
                        }
                        else
                            largeContentOutput.writeLargeArray(da.getRawArray(t), 0, length);
                    }
                    headerWriter.println("end");
                }
            }
        } catch (IOException e) {
            if (largeContentOutput != null) 
                try {
                    largeContentOutput.close();
                } catch (IOException ex) {
                }
            LOGGER.error("Error writing field", e);
            return false;
        }
        return true;
    }
    
    public static final boolean writeBinary(DataContainer container, String prefix,
                                            PrintWriter headerWriter, File dataFile)
    {
        MemoryMappedFileWriter largeContentOutput = null;
        try {
            headerWriter.println("file \"" + dataFile.getName() + "\" binary");
            largeContentOutput = new MemoryMappedFileWriter(new RandomAccessFile(dataFile, "rw"));
            boolean status = writeBinary(container, prefix, headerWriter, largeContentOutput);
            largeContentOutput.close();
            return status;
        } catch (IOException e) {
            LOGGER.error("Error writing field", e);
            return false;
        }
    }
    
    public static final boolean writeBinary(DataContainer container, 
                                            PrintWriter headerWriter, File dataFile)
    { 
        return writeBinary(container, "", headerWriter, dataFile);
    }
    
    public static final boolean writeASCII(DataContainer container, String prefix, 
                                           PrintWriter headerWriter,
                                           PrintWriter contentWriter)
    {
        try {
            if (prefix != null && !prefix.isEmpty())
                prefix = prefix + ":";
            else
                prefix = "";
            float[] timeSteps = container.getTimesteps();
            Vector<LargeArray> dataArrs = new Vector<>();
            Vector<DataArrayType> dataTypes = new Vector<>();
            Vector<Integer> dataVlens = new Vector<>();
            if (timeSteps.length == 1) {
                headerWriter.println("skip 1");
                if (container instanceof Field) {
                    Field fld = (Field)container;
                    if (fld.getCurrentMask() != null) {
                        headerWriter.println("mask");
                        contentWriter.printf(Locale.US, "mask");
                        dataArrs.add(fld.getCurrentMask());
                        dataTypes.add(FIELD_DATA_LOGIC);
                        dataVlens.add(1);
                    }
                    if (fld.getCurrentCoords() != null) {
                        headerWriter.print("coords, ");
                        contentWriter.printf(Locale.US, "%" + (10 * 3 - 2) + "s  ", "coordinates");
                        dataArrs.add(fld.getCurrentCoords());
                        dataTypes.add(FIELD_DATA_FLOAT);
                        dataVlens.add(3);
                    }
                }
                int iOut = 0;
                for (int i = 0; i < container.getNComponents(); i++) {
                    DataArray da = container.getComponent(i);
                    if (!(da.isNumeric() || da.getType() == DataArrayType.FIELD_DATA_STRING))
                        continue;
                    dataArrs.add(da.getRawArray());
                    dataTypes.add(da.getType());
                    dataVlens.add(da.getVectorLength());
                    if (iOut > 0)
                        headerWriter.print(", ");
                    headerWriter.print(prefix + da.getName().replace(' ', '_').replace('.', '_'));
                    contentWriter.print("" + da.getName().replace(' ', '_').replace('.', '_') + "\t");
                    iOut += 1;
                }
                contentWriter.println();
                headerWriter.println();
                int nData = dataArrs.size();
                for (int k = 0; k < container.getNElements(); k++) {
                    for (int l = 0; l < nData; l++) {
                        int vl = dataVlens.get(l);
                        switch (dataTypes.get(l)) {
                        case FIELD_DATA_LOGIC:
                            for (int i = 0; i < vl; i++)
                                contentWriter.print(dataArrs.get(l).getBoolean(k * vl + i) ? "1\t" : "0\t");
                            break;
                        case FIELD_DATA_BYTE:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%3d\t", dataArrs.get(l).getUnsignedByte(k * vl + i) & 0xff);
                            break;
                        case FIELD_DATA_SHORT:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%4d\t", dataArrs.get(l).getShort(k * vl + i));
                            break;
                        case FIELD_DATA_INT:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%6d\t", dataArrs.get(l).getInt(k * vl + i));
                            break;
                        case FIELD_DATA_FLOAT:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%9.4f\t", dataArrs.get(l).getFloat(k * vl + i));
                            break;
                        case FIELD_DATA_DOUBLE:
                            for (int i = 0; i < vl; i++)
                                contentWriter.printf(Locale.US, "%13.6f\t", dataArrs.get(l).getDouble(k * vl + i));
                            break;
                        case FIELD_DATA_STRING:
                            for (int i = 0; i < vl; i++) {
                                String str = ((StringLargeArray)dataArrs.get(l)).get(k * vl + i);
                                if (StringUtils.containsWhitespace(str))
                                    contentWriter.print("\"" + str + "\"");
                                else
                                    contentWriter.print(str);
                            }
                            break;
                        case FIELD_DATA_COMPLEX:
                            float[] v = ((ComplexFloatLargeArray)dataArrs.get(l)).getComplexData(null, k * vl, (k + 1) * vl, 1);
                            for (int i = 0; i < vl; i++) {
                                contentWriter.printf(Locale.US, "%9.4f\t", v[2 * i]);
                                contentWriter.printf(Locale.US, "%9.4f\t", v[2 * i + 1]);
                            }
                            break;
                        default:
                            if (contentWriter != null) 
                                contentWriter.close();
                            LOGGER.error("Error writing field: invalid field type");
                            return false;
                        }
                    }
                    contentWriter.println();
                }
            } else {
                for (int step = 0; step < timeSteps.length; step++) {
                    dataArrs.clear();
                    dataVlens.clear();
                    dataTypes.clear();
                    float t = timeSteps[step];
                    headerWriter.println("timestep " + t);
                    headerWriter.println("skip 1");
                    if (container instanceof Field) {
                        Field fld = (Field)container;
                        if (fld.getMask() != null && fld.getMask().isTimestep(t)) {
                            headerWriter.println("mask");
                            contentWriter.printf(Locale.US, "mask");
                            dataArrs.add(fld.getMask(t));
                            dataTypes.add(FIELD_DATA_LOGIC);
                            dataVlens.add(1);
                        }
                        if (fld.getCoords() != null && fld.getCoords().isTimestep(t)) {
                            headerWriter.print("coords, ");
                            contentWriter.printf(Locale.US, "%" + (10 * 3 - 2) + "s  ", "coordinates");
                            dataArrs.add(fld.getCoords(t));
                            dataTypes.add(FIELD_DATA_FLOAT);
                            dataVlens.add(3);
                        }
                    }

                    int iOut = 0;
                    for (int i = 0; i < container.getNComponents(); i++) {
                        DataArray da = container.getComponent(i);
                        if (!da.isNumeric() || !da.isTimestep(t))
                            continue;
                        dataArrs.add(da.getRawArray(t));
                        dataTypes.add(da.getType());
                        dataVlens.add(da.getVectorLength());
                        if (iOut > 0)
                            headerWriter.print(", ");
                        headerWriter.print(da.getName().replace(' ', '_').replace('.', '_'));
                        contentWriter.print("" + da.getName().replace(' ', '_').replace('.', '_') + "\t");
                        iOut += 1;
                    }
                    contentWriter.println();
                    headerWriter.println();
                    int nData = dataArrs.size();
                    for (int k = 0; k < container.getNElements(); k++) {
                        for (int l = 0; l < nData; l++) {
                            int vl = dataVlens.get(l);
                            switch (dataTypes.get(l)) {
                            case FIELD_DATA_LOGIC:
                                for (int i = 0; i < vl; i++)
                                    contentWriter.print(dataArrs.get(l).getBoolean(k * vl + i) ? "1\t" : "0\t");
                                break;
                            case FIELD_DATA_BYTE:
                                for (int i = 0; i < vl; i++)
                                    contentWriter.printf(Locale.US, "%3d\t", dataArrs.get(l).getUnsignedByte(k * vl + i) & 0xff);
                                break;
                            case FIELD_DATA_SHORT:
                                for (int i = 0; i < vl; i++)
                                    contentWriter.printf(Locale.US, "%4d\t", dataArrs.get(l).getShort(k * vl + i));
                                break;
                            case FIELD_DATA_INT:
                                for (int i = 0; i < vl; i++)
                                    contentWriter.printf(Locale.US, "%6d\t", dataArrs.get(l).getInt(k * vl + i));
                                break;
                            case FIELD_DATA_FLOAT:
                                for (int i = 0; i < vl; i++)
                                    contentWriter.printf(Locale.US, "%9.4f\t", dataArrs.get(l).getFloat(k * vl + i));
                                break;
                            case FIELD_DATA_DOUBLE:
                                for (int i = 0; i < vl; i++)
                                    contentWriter.printf(Locale.US, "%13.6f\t", dataArrs.get(l).getDouble(k * vl + i));
                                break;
                            case FIELD_DATA_COMPLEX:
                                float[] v = ((ComplexFloatLargeArray)dataArrs.get(l)).getComplexData(null, k * vl, (k + 1) * vl, 1);
                                for (int i = 0; i < vl; i++) {
                                    contentWriter.printf(Locale.US, "%9.4f\t", v[2 * i]);
                                    contentWriter.printf(Locale.US, "%9.4f\t", v[2 * i + 1]);
                                }
                                break;
                            default:
                                if (contentWriter != null) 
                                    contentWriter.close();
                                LOGGER.error("Error writing field: invalid field type");
                                return false;
                            }
                        }
                        contentWriter.println();
                    }
                    headerWriter.println("end");
                    contentWriter.println();
                }
            }
        } catch (Exception e) {
            if (contentWriter != null) {
                contentWriter.close();
            }
            LOGGER.error("Error writing field", e);
            return false;
        }
        return true;
    }
    
    
    public static final boolean writeASCII(DataContainer container, 
                                           PrintWriter headerWriter, File dataFile)
    {
        try {
            headerWriter.println("file \"" + dataFile.getName() + "\" ascii col");
            PrintWriter contentWriter = new PrintWriter(new FileOutputStream(dataFile));
            boolean status = writeASCII(container, "", headerWriter, contentWriter);
            contentWriter.close();
            return status;
        } catch (Exception e) {
            return false;
        }
    }
}
