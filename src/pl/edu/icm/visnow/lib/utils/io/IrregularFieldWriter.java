//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.io;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.RandomAccessFile;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.util.Locale;
import java.util.logging.Level;
import org.apache.log4j.Logger;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jscic.CellArray;
import pl.edu.icm.jscic.CellSet;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.cells.Cell;
import pl.edu.icm.jscic.cells.CellType;
import pl.edu.icm.jlargearrays.UnsignedByteLargeArray;

/**
 * @author Krzysztof Nowinski (know@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class IrregularFieldWriter extends FieldWriter
{

    private final IrregularField irregularField;
    private static final Logger LOGGER = Logger.getLogger(IrregularFieldWriter.class);
    private int iCol = 0;
    private final boolean asciiFormat;

    /**
     * Creates a new instance of IrregularFieldWriter.
     *
     * @param irregularField field
     * @param headerFile     header file
     * @param dataFile       data file
     * @param asciiFormat    if true, then an ASCII file format is used
     * @param overwrite      if true, then existing header and data files will be overwritten
     *
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public IrregularFieldWriter(IrregularField irregularField, File headerFile, File dataFile, boolean asciiFormat, boolean overwrite) throws FileSystemException
    {
        super(irregularField, headerFile, dataFile, overwrite);
        this.irregularField = irregularField;
        this.asciiFormat = asciiFormat;
    }

    private String clean(String s)
    {
        return s.replaceAll("\\s", "_").replaceAll(",", "").
                 replaceAll("\\.", "").replaceAll("=", "").
                 replaceAll(":", "");
    }
    
    private void writeCellSetBinary(CellSet cellSet, PrintWriter headerWriter, MemoryMappedFileWriter largeContentOutput) throws Exception
    {
        String setName = clean(cellSet.getName());
        CellType[] cellTypes = Cell.getProperCellTypes();
        for (CellType cellType : cellTypes) {
            if (cellSet.getCellArray(cellType) != null) {
                CellArray cellArray = cellSet.getCellArray(cellType);
                headerWriter.println(setName + ":" + cellType.getPluralName() + ":nodes");
                IntLargeArray nodes = new IntLargeArray(cellArray.getNodes());
                largeContentOutput.writeIntLargeArray(nodes, 0, nodes.length());
                if (cellArray.getDataIndices() != null) {
                    headerWriter.println(setName + ":" + cellType.getPluralName() + ":indices");
                    nodes = new IntLargeArray(cellArray.getDataIndices());
                    largeContentOutput.writeIntLargeArray(nodes, 0, nodes.length());
                }
                if (cellArray.getOrientations() != null) {
                    UnsignedByteLargeArray orientations = new UnsignedByteLargeArray(cellArray.getOrientations());
                    headerWriter.println(setName + ":" + cellType.getPluralName() + ":orientations");
                    largeContentOutput.writeUnsignedByteLargeArray(orientations, 0, orientations.length());
                }
            }
        }
        if (cellSet.getNComponents() > 0)
            WriteContainer.writeBinary(cellSet, setName, headerWriter, largeContentOutput);
    }

    private void writeCellSetASCII(CellSet cellSet, PrintWriter headerWriter, PrintWriter contentWriter) throws Exception
    {
        String setName = clean(cellSet.getName());
        headerWriter.println("skip 1");
        contentWriter.println(setName);
        CellType[] cellTypes = Cell.getProperCellTypes();
        for (CellType cellType : cellTypes) 
            if (cellSet.getCellArray(cellType) != null) {
                headerWriter.println("skip 2");
                CellArray cellArray = cellSet.getCellArray(cellType);
                contentWriter.println(cellType.getPluralName());
                CellArray ca = cellSet.getCellArray(cellType);
                int[] nodes = ca.getNodes();
                int nn = cellType.getNVertices();
                headerWriter.print(setName + ":" + cellType.getPluralName() + ":nodes, ");
                contentWriter.printf("%" + (11 * nn) + "s  ", "nodes     ");
                if (cellArray.getDataIndices() != null) {
                    headerWriter.print(setName + ":" + cellType.getPluralName() + ":indices, ");
                    contentWriter.print("   indices");
                }
                if (cellArray.getOrientations() != null) {
                    headerWriter.print(setName + ":" + cellType.getPluralName() + ":orientations, ");
                    contentWriter.print(" orientations");
                }
                contentWriter.println();
                headerWriter.println();
                for (int i = 0; i < ca.getNCells(); i++) {
                    for (int j = 0; j < nn; j++)
                        contentWriter.printf("%10d ", nodes[nn * i + j]);
                    if (cellArray.getDataIndices() != null)
                        contentWriter.printf("%10d ", cellArray.getDataIndices()[i]);
                    if (cellArray.getOrientations() != null)
                        contentWriter.print(cellArray.getOrientations()[i] == 1 ? "         1" : "         0");
                    contentWriter.println();
                }
            }
        if (cellSet.getNComponents() > 0)
            WriteContainer.writeASCII(cellSet, setName, headerWriter, contentWriter);
    } 

    @Override
    public boolean writeField()
    {
        if (irregularField == null)
            return false;
        boolean ascii = irregularField.getNNodes() < 500 || asciiFormat;
        PrintWriter contentWriter = null;
        PrintWriter fileDescriptionWriter = null;
        MemoryMappedFileWriter largeContentOutput = null;
        ByteArrayOutputStream fileDescriptionBuffer = new  ByteArrayOutputStream();
        try {
            headerWriter = new PrintWriter(new FileOutputStream(headerFile));
            fileDescriptionWriter = new PrintWriter(fileDescriptionBuffer);
            headerWriter.println("#VisNow irregular field");
            if (irregularField.getName() != null)
                headerWriter.print("field \"" + irregularField.getName() + "\"");
            headerWriter.print(", nnodes = " + irregularField.getNNodes());
            if (irregularField.hasMask())
                headerWriter.print(", mask");
            if (irregularField.getUserData() != null) {
                headerWriter.print(", user:");
                String[] udata = irregularField.getUserData();
                for (int j = 0; j < udata.length; j++) {
                    if (j > 0)
                        headerWriter.print(";");
                    headerWriter.print("\"" + udata[j] + "\"");
                }
            }
            headerWriter.println();
            float[][] userExtents = irregularField.getPreferredPhysicalExtents();
            if (inField.getCoordsUnits() != null && inField.getCoordsUnits().length == 3) {
                String[] cu = inField.getCoordsUnits();
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e %s%n", userExtents[0][0], userExtents[1][0], cu[0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e %s%n", userExtents[0][1], userExtents[1][1], cu[1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e %s%n", userExtents[0][2], userExtents[1][2], cu[2]);
            } else {
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e%n", userExtents[0][0], userExtents[1][0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e%n", userExtents[0][1], userExtents[1][1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e%n", userExtents[0][2], userExtents[1][2]);
            }
            WriteContainer.writeHeader(inField, headerWriter);
            if (ascii) {
                fileDescriptionWriter.println("file \"" + dataFile.getName() + "\" ascii col");
                contentWriter = new PrintWriter(new FileOutputStream(dataFile));
                if (!WriteContainer.writeASCII(inField, "", fileDescriptionWriter, contentWriter)) {
                    if (headerWriter != null) 
                        headerWriter.close();
                    fileDescriptionWriter.close();
                    contentWriter.close();
                    return false;
                }
            } else {
                fileDescriptionWriter.println("file \"" + dataFile.getName() + "\" binary");
                largeContentOutput = new MemoryMappedFileWriter(new RandomAccessFile(dataFile, "rw"));
                if (!WriteContainer.writeBinary(inField, null, fileDescriptionWriter, largeContentOutput)) {
                    if (headerWriter != null) 
                        headerWriter.close();
                    fileDescriptionWriter.close();
                    largeContentOutput.close();
                    return false;
                }
            }
            
            for (CellSet cellSet : irregularField.getCellSets()) {
                int nData = 0;
                if (cellSet.getNComponents() > 0)
                    nData = (int)cellSet.getComponent(0).getNElements();
                headerWriter.print("CellSet " + cellSet.getName().replaceAll("\\s", "_").replaceAll(",", "").replaceAll("\\.", "").replaceAll("=", "").replaceAll(":", ""));
                if (nData > 0)
                    headerWriter.println(", nData " + nData);
                else
                    headerWriter.println();
                String filler = "             ";
                CellType[] cellTypes = Cell.getProperCellTypes();
                for (CellType cellType : cellTypes) {
                    if (cellSet.getCellArray(cellType) != null) {
                        CellArray cellArray = cellSet.getCellArray(cellType);
                        if (cellArray.getNCells() > 1)
                            headerWriter.printf("%s%s%7d%n", cellType.getPluralName(),
                                                filler.substring(0, 12 - cellType.getPluralName().length()),
                                                cellArray.getNCells());
                        else
                            headerWriter.printf("%s%s%7d%n", cellType.getName(),
                                                filler.substring(0, 12 - cellType.getName().length()),
                                                cellArray.getNCells());
                    }
                }
                if (nData != 0) 
                    WriteContainer.writeHeader(cellSet, headerWriter);
                    if (ascii) 
                        writeCellSetASCII(cellSet, fileDescriptionWriter, contentWriter);
                    else 
                        writeCellSetBinary(cellSet, fileDescriptionWriter, largeContentOutput);
            }
            fileDescriptionWriter.close();
            ByteArrayInputStream is = new ByteArrayInputStream(fileDescriptionBuffer.toByteArray());
            BufferedReader fileDescriptionReader = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = fileDescriptionReader.readLine()) != null)
                headerWriter.println(line);
            fileDescriptionReader.close();
            headerWriter.close();
            if (contentWriter != null)
                contentWriter.close();
            if (largeContentOutput != null)
                largeContentOutput.close();
            return true;
        }   catch (Exception ex) {
            headerWriter.close();
            if (fileDescriptionWriter != null)
                fileDescriptionWriter.close();
            if (contentWriter != null)
                contentWriter.close();
            java.util.logging.Logger.getLogger(IrregularFieldWriter.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
}
