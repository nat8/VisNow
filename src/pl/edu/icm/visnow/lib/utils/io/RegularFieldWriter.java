//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>

package pl.edu.icm.visnow.lib.utils.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.FileSystemException;
import java.util.Locale;
import org.apache.log4j.Logger;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.jscic.dataarrays.DataArray;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class RegularFieldWriter extends FieldWriter
{

    private final RegularField regularField;
    private static final Logger LOGGER = Logger.getLogger(RegularFieldWriter.class);
    private final boolean asciiFormat;

    /**
     * Creates a new instance of RegularFieldWriter.
     *
     * @param regularField input field
     * @param headerFile   header file
     * @param dataFile     data file
     * @param asciiFormat  if true, then an ASCII file format is used
     * @param overwrite    if true, then existing header and data files will be overwritten
     *
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public RegularFieldWriter(RegularField regularField, File headerFile, File dataFile, boolean asciiFormat, boolean overwrite) throws FileSystemException
    {
        super(regularField, headerFile, dataFile, overwrite);
        this.regularField = regularField;
        this.asciiFormat = asciiFormat;
    }


    @Override
    public boolean writeField()
    {
        if (regularField == null)
            return false;
        try {
            headerWriter = new PrintWriter(new FileOutputStream(headerFile));
            headerWriter.println("#VisNow regular field");
            if (regularField.getName() != null && !regularField.getName().trim().isEmpty())
                headerWriter.print("field \"" + regularField.getName() + "\",");
            headerWriter.print(" dims ");
            for (int i = 0; i < regularField.getDims().length; i++)
                headerWriter.print(" " + regularField.getDims()[i]);
            if (regularField.hasMask())
                headerWriter.print(", mask");
            if (regularField.getCurrentCoords() != null)
                headerWriter.print(", coords");
            if (regularField.getUserData() != null) {
                headerWriter.print(", user:");
                String[] udata = regularField.getUserData();
                for (int j = 0; j < udata.length; j++) {
                    if (j > 0)
                        headerWriter.print(";");
                    headerWriter.print("\"" + udata[j] + "\"");
                }
            }
            headerWriter.println();
            if (regularField.getCurrentCoords() == null) {
                float[][] af = regularField.getAffine();
                headerWriter.printf(Locale.US, "origin %10.4e %10.4e %10.4e %n", af[3][0], af[3][1], af[3][2]);
                for (int i = 0; i < 3; i++)
                    headerWriter.printf(Locale.US, "    v%d %10.4e %10.4e %10.4e %n", i, af[i][0], af[i][1], af[i][2]);
            }
            float[][] userExtents = regularField.getPreferredPhysicalExtents();
            if (inField.getCoordsUnits() != null && inField.getCoordsUnits().length == 3) {
                String[] cu = inField.getCoordsUnits();
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e %s%n", userExtents[0][0], userExtents[1][0], cu[0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e %s%n", userExtents[0][1], userExtents[1][1], cu[1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e %s%n", userExtents[0][2], userExtents[1][2], cu[2]);
            } else {
                headerWriter.printf(Locale.US, "user extent x %10.4e %10.4e%n", userExtents[0][0], userExtents[1][0]);
                headerWriter.printf(Locale.US, "user extent y %10.4e %10.4e%n", userExtents[0][1], userExtents[1][1]);
                headerWriter.printf(Locale.US, "user extent z %10.4e %10.4e%n", userExtents[0][2], userExtents[1][2]);
            }
            WriteContainer.writeHeader(inField, headerWriter);
            boolean status;
            if (asciiFormat)
                status = WriteContainer.writeASCII(inField, headerWriter, dataFile);
            else 
                status = WriteContainer.writeBinary(inField, headerWriter, dataFile);
            headerWriter.close();
            return status;
        } catch (FileNotFoundException e) {
            if (headerWriter != null) {
                headerWriter.close();
            }
            LOGGER.error("Error writing field", e);
        }
        return false;
    }
}
