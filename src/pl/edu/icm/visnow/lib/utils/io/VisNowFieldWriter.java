//<editor-fold defaultstate="collapsed" desc=" COPYRIGHT AND LICENSE ">
/* VisNow
 Copyright (C) 2006-2013 University of Warsaw, ICM

 This file is part of GNU Classpath.

 GNU Classpath is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2, or (at your option)
 any later version.

 GNU Classpath is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with GNU Classpath; see the file COPYING.  If not, write to the
 University of Warsaw, Interdisciplinary Centre for Mathematical and
 Computational Modelling, Pawinskiego 5a, 02-106 Warsaw, Poland.

 Linking this library statically or dynamically with other modules is
 making a combined work based on this library.  Thus, the terms and
 conditions of the GNU General Public License cover the whole
 combination.

 As a special exception, the copyright holders of this library give you
 permission to link this library with independent modules to produce an
 executable, regardless of the license terms of these independent
 modules, and to copy and distribute the resulting executable under
 terms of your choice, provided that you also meet, for each linked
 independent module, the terms and conditions of the license of that
 module.  An independent module is a module which is not derived from
 or based on this library.  If you modify this library, you may extend
 this exception to your version of the library, but you are not
 obligated to do so.  If you do not wish to do so, delete this
 exception statement from your version.
 */
//</editor-fold>
package pl.edu.icm.visnow.lib.utils.io;

import java.io.File;
import java.nio.file.FileSystemException;
import javax.swing.JOptionPane;
import org.apache.commons.io.FilenameUtils;
import pl.edu.icm.jscic.Field;
import pl.edu.icm.jscic.IrregularField;
import pl.edu.icm.jscic.RegularField;
import pl.edu.icm.visnow.lib.basic.writers.FieldWriter.FieldWriterFileFormat;

/**
 * @author Bartosz Borucki (babor@icm.edu.pl) University of Warsaw,
 * Interdisciplinary Centre for Mathematical and Computational Modelling
 */
public class VisNowFieldWriter
{

    /**
     * Writes field to a file.
     *
     * @param field     field
     * @param path      file path
     * @param format    file format
     * @param overwrite if true, then existing header and data files will be overwritten
     *
     * @return true if the operation was successful, false otherwise
     *
     * @throws FileAlreadyExistsException if overwrite == false and output files already exist.
     * @throws FileSystemException        if cannot write to output files.
     */
    public static boolean writeField(Field field, String path, FieldWriterFileFormat format, boolean overwrite) throws FileSystemException
    {
        String baseName = FilenameUtils.removeExtension(path);
        File headerFile = format != FieldWriterFileFormat.SERIALIZED ? new File(baseName + "." + format.getHeaderFileExtension()) : null;
        File dataFile = new File(baseName + "." + format.getDataFileExtension());

        if (format == FieldWriterFileFormat.SERIALIZED) {
            return new SerializedFieldWriter(field, dataFile, overwrite).writeField();
        } else {
            if (field instanceof RegularField)
                return new RegularFieldWriter((RegularField) field, headerFile, dataFile, format == FieldWriterFileFormat.VNF_ASCII, overwrite).writeField();
            else if (field instanceof IrregularField)
                return new IrregularFieldWriter((IrregularField) field, headerFile, dataFile, format == FieldWriterFileFormat.VNF_ASCII, overwrite).writeField();
        }
        return false;
    }

    /**
     * Tests whether the application can modify the file denoted by path. A confirm dialog box will appear if the file exists.
     *
     * @param path   file path
     * @param format file format
     *
     * @return true if the application can modify the file denoted by path and the user confirmed the existing file can be overwritten, false otherwise.
     */
    public static boolean canWriteToOutputFile(String path, FieldWriterFileFormat format)
    {
        String baseName = FilenameUtils.removeExtension(path);
        String outHeaderFileName = format != FieldWriterFileFormat.SERIALIZED ? baseName + "." + format.getHeaderFileExtension() : null;
        String outDataFileName = baseName + "." + format.getDataFileExtension();
        boolean result = true;

        if (outHeaderFileName != null) {
            File outHeaderFile = new File(outHeaderFileName);
            if (!outHeaderFile.getParentFile().canWrite()) {
                JOptionPane.showMessageDialog(null, "Cannot write file (check write permissions)");
                return false;
            }
            if (outHeaderFile.exists()) {
                Object ans = JOptionPane.showConfirmDialog(null, "File " + outHeaderFile.getName() + " exists.\nOwerwrite?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
                result = ans instanceof Integer && (Integer) ans == JOptionPane.YES_OPTION;
            }
        }
        File outDataFile = new File(outDataFileName);
        if (!outDataFile.getParentFile().canWrite()) {
            JOptionPane.showMessageDialog(null, "Cannot write file (check write permissions)");
            return false;
        }
        if (outDataFile.exists()) {
            Object ans;
            if (format != FieldWriterFileFormat.SERIALIZED) {
                ans = JOptionPane.showConfirmDialog(null, "Data file " + outDataFile.getName() + " exists in the selected target directory\nand may be referenced by other VNF files.\nThis operation will overwrite this data file.\nContinue?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            } else {
                ans = JOptionPane.showConfirmDialog(null, "File " + outDataFile.getName() + " exists.\nOwerwrite?", "", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
            }
            result = result && (ans instanceof Integer && (Integer) ans == JOptionPane.YES_OPTION);
        }
        return result;
    }
}
