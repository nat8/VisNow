package pl.edu.icm.visnow.ModuleCreator;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Krzysztof S. Nowinski (University of Warsaw, ICM)
 */
public class GeneralPanel extends javax.swing.JPanel
{

    ModuleCreatorFrame parent = null;

    private File projectDir, packageDir, packageDirectory;
    private String packageName, moduleName, displayedName;

    /**
     * Creates new form GeneralPanel
     */
    public GeneralPanel()
    {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents()
    {
        GridBagConstraints gridBagConstraints;

        projectDirectoryChooser = new JFileChooser();
        packageDirectoryChooser = new JFileChooser();
        jLabel2 = new JLabel();
        nameField = new JTextField();
        projectButton = new JButton();
        projectField = new JTextField();
        packageButton = new JButton();
        packageField = new JTextField();
        jLabel1 = new JLabel();
        shortDescriptionField = new JTextField();
        jLabel3 = new JLabel();
        jScrollPane2 = new JScrollPane();
        descriptionArea = new JTextArea();
        doneButton = new JButton();
        jLabel4 = new JLabel();
        displayedNameField = new JTextField();

        setLayout(new GridBagLayout());

        jLabel2.setText("module class name");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        add(jLabel2, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        add(nameField, gridBagConstraints);

        projectButton.setText("project directory");
        projectButton.setHorizontalAlignment(SwingConstants.LEFT);
        projectButton.setHorizontalTextPosition(SwingConstants.LEFT);
        projectButton.setIconTextGap(0);
        projectButton.setMargin(new Insets(2, 0, 2, 0));
        projectButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                projectButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        add(projectButton, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(projectField, gridBagConstraints);

        packageButton.setText("module directory");
        packageButton.setHorizontalAlignment(SwingConstants.LEFT);
        packageButton.setHorizontalTextPosition(SwingConstants.LEFT);
        packageButton.setMargin(new Insets(2, 0, 2, 0));
        packageButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                packageButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        add(packageButton, gridBagConstraints);

        packageField.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                packageFieldActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(packageField, gridBagConstraints);

        jLabel1.setText("short description");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(jLabel1, gridBagConstraints);

        shortDescriptionField.setText("shortDescriptionField");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(shortDescriptionField, gridBagConstraints);

        jLabel3.setText("description");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 5;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(9, 0, 0, 0);
        add(jLabel3, gridBagConstraints);

        descriptionArea.setColumns(20);
        descriptionArea.setRows(5);
        jScrollPane2.setViewportView(descriptionArea);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 6;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.weighty = 0.2;
        add(jScrollPane2, gridBagConstraints);

        doneButton.setText("done");
        doneButton.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent evt)
            {
                doneButtonActionPerformed(evt);
            }
        });
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 7;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        add(doneButton, gridBagConstraints);

        jLabel4.setText("module name as displayed  ");
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        add(jLabel4, gridBagConstraints);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.insets = new Insets(0, 0, 2, 0);
        add(displayedNameField, gridBagConstraints);
    }// </editor-fold>//GEN-END:initComponents

    private void projectButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_projectButtonActionPerformed
    {//GEN-HEADEREND:event_projectButtonActionPerformed
        projectDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = projectDirectoryChooser.showDialog(this, "Select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                projectDir = projectDirectoryChooser.getSelectedFile();
                projectField.setText(projectDir.getCanonicalPath());
                packageDirectoryChooser.setCurrentDirectory(projectDir);
            } catch (IOException ex) {
                Logger.getLogger(ModuleCreatorFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_projectButtonActionPerformed

    private void packageButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_packageButtonActionPerformed
    {//GEN-HEADEREND:event_packageButtonActionPerformed
        packageDirectoryChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = packageDirectoryChooser.showDialog(this, "Select");
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            try {
                packageDir = packageDirectoryChooser.getSelectedFile();
                String packageDirPath = packageDir.getCanonicalPath();
                packageName = packageDirPath.substring(projectDir.getCanonicalPath().length() + 1);
                moduleName = nameField.getText();
                String packagePath = packageDirPath + File.separator + moduleName;
                packageName = packageName.replace(File.separator, ".") + "." + moduleName;
                packageField.setText(packageName);
                packageDirectory = new File(packagePath);
                packageDirectory.mkdir();
            } catch (IOException ex) {
                Logger.getLogger(ModuleCreatorFrame.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }//GEN-LAST:event_packageButtonActionPerformed

    private void packageFieldActionPerformed(ActionEvent evt)//GEN-FIRST:event_packageFieldActionPerformed
    {//GEN-HEADEREND:event_packageFieldActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_packageFieldActionPerformed

    private void doneButtonActionPerformed(ActionEvent evt)//GEN-FIRST:event_doneButtonActionPerformed
    {//GEN-HEADEREND:event_doneButtonActionPerformed
        if (parent != null) {
            parent.setPackageName(packageName);
            parent.setPackageDirectory(packageDirectory);
            parent.setModuleName(moduleName);
            parent.setDisplayedName(displayedNameField.getText());
            parent.setShortDescription(shortDescriptionField.getText());
        }
    }//GEN-LAST:event_doneButtonActionPerformed

    public void setParent(ModuleCreatorFrame parent)
    {
        this.parent = parent;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private JTextArea descriptionArea;
    private JTextField displayedNameField;
    private JButton doneButton;
    private JLabel jLabel1;
    private JLabel jLabel2;
    private JLabel jLabel3;
    private JLabel jLabel4;
    private JScrollPane jScrollPane2;
    private JTextField nameField;
    private JButton packageButton;
    private JFileChooser packageDirectoryChooser;
    private JTextField packageField;
    private JButton projectButton;
    private JFileChooser projectDirectoryChooser;
    private JTextField projectField;
    private JTextField shortDescriptionField;
    // End of variables declaration//GEN-END:variables
}
