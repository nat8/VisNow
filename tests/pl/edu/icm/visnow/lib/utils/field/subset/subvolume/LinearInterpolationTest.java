package pl.edu.icm.visnow.lib.utils.field.subset.subvolume;

import java.util.Vector;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import pl.edu.icm.jlargearrays.ComplexFloatLargeArray;
import pl.edu.icm.jlargearrays.FloatLargeArray;
import pl.edu.icm.jlargearrays.IntLargeArray;
import pl.edu.icm.jlargearrays.ShortLargeArray;
import pl.edu.icm.jlargearrays.StringLargeArray;
import pl.edu.icm.jscic.dataarrays.DataArrayType;

import static org.junit.Assert.*;
import pl.edu.icm.jscic.TimeData;

/**
 *
 * @author know
 */


public class LinearInterpolationTest
{
    
    public LinearInterpolationTest()
    {
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
    }
    
    @After
    public void tearDown()
    {
    }

    /**
     * Test of interpolateToNewNodesSet method, of class LinearInterpolation.
     */
    @Test
    public void testInterpolateToNewNodesSet()
    {
        System.out.println("interpolateToNewNodesSet");
        int nNodes = 3;
        Vector<TimeData> inVals = new Vector<>();
        short[] cs = {0, 1, 2};
        int[] ci   = {0, 1, 2};
        float[] cf = {0, 1, 2, 1, 2, 3};
        float[] cref = {0, 1, 2};
        float[] cimf = {2, 1, 0};
        String[] cst = {"a", "b", "c"};
        TimeData tds = new TimeData(DataArrayType.FIELD_DATA_SHORT);
        tds.setValue(new ShortLargeArray(cs), 0);
        TimeData tdi = new TimeData(DataArrayType.FIELD_DATA_INT);
        tdi.setValue(new IntLargeArray(ci), 0);
        TimeData tdf = new TimeData(DataArrayType.FIELD_DATA_FLOAT);
        tdf.setValue(new FloatLargeArray(cf), 0);
        TimeData tdstr = new TimeData(DataArrayType.FIELD_DATA_STRING);
        tdstr.setValue(new StringLargeArray(cst), 0);
        TimeData tdc = new TimeData(DataArrayType.FIELD_DATA_COMPLEX);
        tdc.setValue(new ComplexFloatLargeArray(cref, cimf), 0);
        inVals.add(tds);
        inVals.add(tdi);
        inVals.add(tdf);
        inVals.add(tdc);
        inVals.add(tdstr);
        int[] vLens = {1, 1, 2, 1, 1};
        NewNode[] newNodes = {new NewNode(0, 1, .4f), new NewNode(0, 2, .6f),  new NewNode(1, 2, .5f)};
        boolean singleTimeMoment = false;
        Vector<TimeData> expResult = null;
        Vector<TimeData> result = LinearInterpolation.interpolateToNewNodesSet(nNodes, inVals, vLens, newNodes, singleTimeMoment);
//        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
    }
    
    public static void main(String[] args)
    {
        new LinearInterpolationTest().testInterpolateToNewNodesSet();
    }
    
}
